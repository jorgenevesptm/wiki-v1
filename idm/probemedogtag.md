<!-- TITLE: Problème dogtag -->
<!-- SUBTITLE: IdM s'arrête peu après le démarrage -->

# Les services d'IdM s'arrêtent après le démarrage

Suite à un problème de certificats, le service dogtag ne démarre pas et par conséquent toute la chaine de services s'arrête.
Workaround: relancer les services avec la commande:
ipactl start --ignore-service-failure