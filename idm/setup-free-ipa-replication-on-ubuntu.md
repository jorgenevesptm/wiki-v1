<!-- TITLE: Setup FreeIPA Replication On Ubuntu & CA Renewal Master -->
<!-- SUBTITLE: Procedure -->

# Presentation
In this document, we will cover complete steps to configure FreeIPA replication on Ubuntu 18.04. When you have FreeIPA replica setup, FreeIPA Clients can continue to authenticate even if a server is down.
 
 An example of replicating from the master to replica server within following names:
 The master server : srv-idm-form-01.telem.local
 The replica server : srv-identity-mgt-form-01.telem.local
 
**It is critical that each following steps be competed successfully. In case of any issue, refer to the TROUBLESHOOTING session below to get it fixed prior going to the next step.**

# Step 1: Configure DNS local hosts file on both servers
 Add the IPA master & replica IP addresses into their local /etc/hosts on **both servers**
```text
$ vi /etc/hosts
```

10.144.2.107   srv-idm-form-01.telem.local                  srv-idm-form-01
10.144.2.118   srv-identity-mgt-form-01.telem.local    srv-identity-mgt-form-01


# Step 2: Install FreeIPA Client on Replica Server
```text
$ apt-get install freeipa-client -y
$ ipa-client-install --hostname=`hostname -f`  --mkhomedir  --server=srv-idm-form-01.telem.local --domain telem.local --realm TELEM.LOCAL
```

Proceed with fixed values and no DNS discovery? [no]: **yes**
Continue to configure the system with these values? [no]: **yes**
User authorized to enroll computers: **admin**

Client configuration complete.
The ipa-client-install command was successful

# Step 3: Enable mkhomedir
By default, sssd service will not create a home directory for the user on the first login, we need to enable this feature by modifying PAM configuration file.

```text
$ sudo bash -c "cat > /usr/share/pam-configs/mkhomedir" <<EOF
Name: activate mkhomedir
Default: yes
Priority: 900
Session-Type: Additional
Session:
required pam_mkhomedir.so umask=0022 skel=/etc/skel
EOF
```
then run:
```text
$ sudo pam-auth-update
```

When the PAM Configuration pop-up window is displayed, **ensure all options are enabled**, then select **OK** and press **Enter**

# Step 4: Install FreeIPA Server on Replica
```text
$ apt-get install freeipa-server  -y
```
Select **OK** and press **Enter** when the pop-up windows "**Configuring krb5-admin-server**" and "**Configuring opendnssec-common**" are displayed.

Test by requesting for a Kerberos ticket on the replica:
```text
$ kinit admin
Password for admin@TELEM.LOCAL:

$ klist
Ticket cache: KEYRING:persistent:0:0
Default principal: admin@TELEM.LOCAL

Valid starting       Expires              Service principal
10/02/2020 15:29:32  10/03/2020 15:29:27  krbtgt/TELEM.LOCAL@TELEM.LOCAL
```
  
# Step 5: Add Replica server to the ipaservers group on master server
**Login to Master Server** and add replica server to the **ipaservers** group:
```text
$ ipa hostgroup-add-member ipaservers --hosts srv-identity-mgt-form-01.telem.local
```
  
# Step 6: Run ipa-replica-install on the Replica server
To this point, you only have to run  ipa-replica-install command on the replica server to sync FreeIPA Server configurations and get the server ready for clients to connect to.
```text
$ ipa-replica-install --setup-ca 
```

# Step 7: Checkpoint
```text
$ ipactl status
Directory Service: RUNNING
krb5kdc Service: RUNNING
kadmin Service: RUNNING
httpd Service: RUNNING
ipa-custodia Service: RUNNING
pki-tomcatd Service: RUNNING
ipa-otpd Service: RUNNING
```
If any is not RUNNING, refer to the **TROUBLESHOOTING** session to get it fixed prior going to the next step!


# Step 8: Promote a replica to a CA Renewal Master
if you plan to take the master CA server offline or decommission it, promote a replica to take the its place as the master CA.
Determine which server is the current renewal master:

Run on the **Replica Server**
```text
$ ipa config-show | grep "CA renewal master"
IPA CA renewal master: srv-idm-form-01.telem.local
```
To set a different server to handle certificate renewal:
```text
$ ipa config-mod --ca-renewal-master-server srv-identity-mgt-form-01.telem.local

.................................................................................................................
................................................................................................................
IPA masters: srv-identity-mgt-form-01.telem.local, srv-idm-form-01.telem.local
IPA CA servers: srv-identity-mgt-form-01.telem.local, srv-idm-form-01.telem.local
IPA CA renewal master: srv-identity-mgt-form-01.telem.local
IPA master capable of PKINIT: srv-identity-mgt-form-01.telem.local, srv-idm-form-01.telem.local
```

Make sure the new CA renewal master is the one from the replica:
```text
$ ipa config-show | grep "CA renewal master"
IPA CA renewal master: srv-identity-mgt-form-01.telem.local
```

# Step 9: Delete the old master server from replication topology
Run on the **Replica Server**
```text
$ ipa server-del srv-idm-form-01.telem.local
Removing srv-idm-form-01.telem.local from replication topology, please wait...
ipa: WARNING: Failed to cleanup srv-idm-form-01.telem.local DNS entries: DNS is not configured
ipa: WARNING: You may need to manually remove them from the tree
------------------------------------------------
Deleted IPA server "srv-idm-form-01.telem.local"
```
# Step 10: Remove the old master from IPASERVERS group
Run on the **Replica Server**
```text
$ ipa hostgroup-remove-member ipaservers --hosts srv-idm-form-01.telem.local
Host-group: ipaservers
Description: IPA server hosts
Member hosts: srv-identity-mgt-form-01.telem.local
Number of members removed 1
```

# Step 11: Checkpoints
Run on the **Replica Server**
```text
$ ipactl status
Directory Service: RUNNING
krb5kdc Service: RUNNING
kadmin Service: RUNNING
httpd Service: RUNNING
ipa-custodia Service: RUNNING
pki-tomcatd Service: RUNNING
ipa-otpd Service: RUNNING
```
>TIP: If any is not RUNNING, try to run ** ipactl restart**. If the issue is not fixed, refer to the TROUBLESHOOTING session to get it fixed prior going to the next step!

```text
$ ipa config-show
  Maximum username length: 32
  Home directory base: /home
  Default shell: /bin/sh
  Default users group: ipausers
  Default e-mail domain: telem.local
  Search time limit: 2
  Search size limit: 100
  User search fields: uid,givenname,sn,telephonenumber,ou,title
  Group search fields: cn,description
  Enable migration mode: FALSE
  Certificate Subject base: O=TELEM.LOCAL
  Password Expiration Notification (days): 4
  Password plugin features: AllowNThash, KDC:Disable Last Success
  SELinux user map order: guest_u:s0$xguest_u:s0$user_u:s0$staff_u:s0-s0:c0.c1023$unconfined_u:s0-s0:c0.c1023
  Default SELinux user: unconfined_u:s0-s0:c0.c1023
  Default PAC types: MS-PAC, nfs:NONE
  IPA masters: srv-identity-mgt-form-01.telem.local
  IPA CA servers: srv-identity-mgt-form-01.telem.local
  IPA CA renewal master: srv-identity-mgt-form-01.telem.local
  IPA master capable of PKINIT: srv-identity-mgt-form-01.telem.local
```

# Step 12: Update the IPA Renewall Master Config
Run on the **Replica Server**
Set the value of **ca.crl.MasterCRL.enableCRLCache** and **ca.crl.MasterCRL.enableCRLUpdates** in **/etc/pki/pki-tomcat/ca/CS.cfg** to **true** :

First stop CA service:

```text
$ systemctl stop pki-tomcatd@pki-tomcat
```
vi **/etc/pki/pki-tomcat/ca/CS.cfg** and change value to **true**
ca.crl.MasterCRL.enableCRLCache=**true**
ca.crl.MasterCRL.enableCRLUpdates=**true**

Start CA service
```text
$ systemctl start pki-tomcatd.service
$ ipactl status
```
# Step 13: Configure Apache to handle CRL requests
Configure Apache to handle CRL requests in **/etc/apache2/conf-enabled/ipa-pki-proxy.conf** by commenting out the RewriteRule on the last line as below:

#RewriteRule ^/ipa/crl/MasterCRL.bin https://<hostname>/ca/ee/ca/getCRL?op=getCRL&crlIssuingPoint=MasterCRL [L,R=301,NC]

Restart Apache:
```text
$ systemctl restart apache2
```

# Step 14: Set dnarange on the replica server
```text
$ ipa user-show admin | egrep -i 'uid|gid'
  UID: 1123000000
  GID: 1123000000
```
So now we know that the first ID is ‘1123000000’ and usually the installer allocated around 20.000 IDs of range. With that knowledge, you can set the dnarange on the replica:

```text
$ ipa-replica-manage dnarange-set srv-identity-mgt-form-01.telem.local 1123000000-1123020000

$ ipa-replica-manage dnarange-show
srv-identity-mgt-form-01.telem.local: 1123000005-1123020000
```

# Step 15: Ensure the local IPA certificate databases is enable to update with certificates from the server
```text
$ ipa-certupdate
.............................
Systemwide CA database updated.
Systemwide CA database updated.
The ipa-certupdate command was successful
```

# Step 16: Creating a test user
```text
$ ipa user-add  --first=Test  --last=user  testuser001
-------------------
Added user "tuser1"
-------------------
  User login: testuser001
  First name: Test
	Last name: User
  Full name: Test User
  Display name: Test User
  Initials: tu
  Home directory: /home/testuser001
  GECOS: Test User
  Login shell: /bin/sh
  Principal name: testuser001@TELEM.LOCAL
  Principal alias: testuser001@TELEM.LOCAL
  Email address: testuser001@telem.local
  UID: 1123000004
  GID: 1123000004
  Password: False
  Member of groups: ipausers
```

# Step 18: Stop the IPA services on the old master
Run on the **old Master Server**
```text
$ ipactl stop
```

# Step 19: Connect to the FreeIPA WEB UI
https://srv-identity-mgt-form-01.telem.local


# Troubleshooting
* **Uninstall freeipa-client**
In case of failure, identify the issue, uninstall the IPA client and re-install it!
$ ipa-client-install –uninstall

* **Uninstall freeipa-server**
In case of failure, identify the issue, uninstall the IPA server and re-install it!
$ ipa-server-install –uninstall

* **Unable to start the krb5-kdc.service**
$ kdb5_util create -s –r TELEM.LOCAL
$ systemctl restart krb5-kdc.service

* **Unable to start the kadmin service**
$ touch /etc/krb5kdc/kadm5.acl
$ ipactl restart

* **Remove a replica server**
$ ipa-replica-manage del \<hostname> --force

* **Remove a member from the ipaservers hostgroup**
$ ipa hostgroup-remove-member ipaservers --hosts \<hostname>

* **Delete a server**
$ ipa server-del \<hostname>

* **Unable login access to WEB UI. You need to change the permission as following**
$ chmod +x /var/lib/krb5kdc

* **Issue with IPA 4301: CertificateOperationError**
Copy file from **srv-linux-mgt-01:/home/ansible/ipa/dogtag.py** to **\<ipa_server>:/usr/lib/python2.7/dist-packages/ipapython**
Once the **dogtag.py** file is replaced, restart IPA services: **ipactl restart**

* **fontawesome issue**
$ cd \<ipa-replica-server>: /usr/share/fonts/truetype
$ cp -r font-awesome/ ./fontawesome/
$ ipactl restart
$ logoff/logon your IPA WEB UI session

* **Good resources about FreeIPA & Certificate sites**
https://frasertweedale.github.io/blog-redhat/tags/certificates.html
https://www.freeipa.org/page/Howto/CA_Certificate_Renewal

### **Certificates - The following certificates will be generated on each replica:**
**Subsystem certificate**: this certificate is used for internal CA communications and there can be one per replica
**Audit certificate**: the audit certificate is used to sign the audit log, having a certificate per replica will insure the auditors can verify which replica generated each auditable event, improving the auditability of the CA.
**Server certificate**: this is already per server today, no changes
**RA Agent Certificate**: A new agent user will be created on each replica, added to the RAs group, and a new certificate per replica will be created
**OCSP certificate**: some more research needs to be done, but it appears that multiple OCSP certificates can coexist. This certificate is optional (necessary only when the CRL/OCSP role is transferred) so this cert will be generated only when needed and not copied over from the initial CA

### **Certificates/keys that still need to be transferred to replicas:**
**CA signing key**: in order to be the same CA all replicas need a copy of the CA key
**KRA certificates**: The storage certificate must be shared between KRA/Vault servers so that all servers can encrypt/decrypt the storage. The transport key certificate could be generate per replica but that would make clients more complex as they would need to know which one to use for each replica making load-balancing and fallback clients much harder.

### **Certificates commands**
List certificates: **certutil -L -d /etc/pki/pki-tomcat/alias**
Export certificate: **pk12util -d /etc/pki/pki-tomcat/alias -n** 'certificate_name' **-o file.p12**  (NSS password is stored in /etc/pki/pki-tomcat/alias/pwdfile.txt, set PKCS12 passwd to the same one)
Import certificate: **pk12util -d /etc/pki/pki-tomcat/alias -i file.p12**

### **Useful Config files**
	/usr/share/ipa/html/ca.crt
	/usr/local/share/ca-certificates/ipa-ca.crt
	/etc/ssl/certs/ca-certificates.crt
	/etc/ipa/ca.crt
	/etc/ldap/ldap.conf
	/var/lib/ipa/certs/httpd.crt
	/var/lib/krb5kdc/kdc.crt
	/var/lib/pki/pki-tomcat/conf/server.xml 






