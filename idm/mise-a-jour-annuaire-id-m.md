<!-- TITLE: Mise A Jour Annuaire IdM -->
<!-- SUBTITLE: A quick summary of Mise A Jour Annuaire IdM -->

# Mise à jour de l'annuaire IdM suite à un refresh des données de QA depuis la Prod
***Note:*** Remplacer #mdp_directory_manager# par le mdp sur le keepass (Domain telem.local - SAE - IdM - srv-idm-qa-01)

1- Vider l'annuaire IdM:
ldapsearch -D "cn=Directory Manager" -H ldaps://srv-idm-qa-01.telem.local -x -w  **#mdp_directory_manager#** -b cn=users,cn=accounts,dc=telem,dc=local -s one dn 100 |grep dn: |grep -Ev 'uid=mir_user,cn=users,cn=accounts,dc=telem,dc=local|uid=systel,cn=users,cn=accounts,dc=telem,dc=local|uid=admin,cn=users,cn=accounts,dc=telem,dc=local|uid=admin-c2c,cn=users,cn=accounts,dc=telem,dc=local|uid=ro_user,cn=users,cn=accounts,dc=telem,dc=local|uid=rw_user,cn=users,cn=accounts,dc=telem,dc=local|uid=disegno,cn=users,cn=accounts,dc=telem,dc=local|uid=pwmuser,cn=users,cn=accounts,dc=telem,dc=local|uid=testuser,cn=users,cn=accounts,dc=telem,dc=local|uid=99999,cn=users,cn=accounts,dc=telem,dc=local' |cut -b 5- | ldapdelete -x -D "cn=Directory Manager" -H ldaps://srv-idm-qa-01.telem.local -w **#mdp_directory_manager#**

(en cas de problème, s'adresser à Jean-Yves Corre ou José de Almeida)
2 - Lancer l'application OractleToLDAP.exe qui se trouve dans le P:\ECADIS\ldap\

2.1 - Attention à vérifier que le PC ECA a la possiblité de joindre l'IP de l'IDM (10.144.2.105)
2.2 - Vérifier les paramètres de la configuration dans le fichier .conf
2.2.1 - TnsName.ORA => Nom de la base Oracle
2.2.2 - Accept_empty_email = 1 (1 = on accepte tous. 0 = on accepte uniquement les gens qui ont un email)

3 - Reset du mot de passe de tous les utilisateurs (118):
ldapsearch -D "cn=Directory Manager" -H ldaps://srv-idm-qa-01.telem.local -x -w **#mdp_directory_manager#** -b cn=users,cn=accounts,dc=telem,dc=local -s one dn 100 |grep dn: |grep -Ev 'uid=mir_user,cn=users,cn=accounts,dc=telem,dc=local|uid=systel,cn=users,cn=accounts,dc=telem,dc=local|uid=admin,cn=users,cn=accounts,dc=telem,dc=local|uid=admin-c2c,cn=users,cn=accounts,dc=telem,dc=local|uid=ro_user,cn=users,cn=accounts,dc=telem,dc=local|uid=rw_user,cn=users,cn=accounts,dc=telem,dc=local|uid=disegno,cn=users,cn=accounts,dc=telem,dc=local|uid=rw_user,cn=users,cn=accounts,dc=telem,dc=local|uid=pwmuser,cn=users,cn=accounts,dc=telem,dc=local|uid=testuser,cn=users,cn=accounts,dc=telem,dc=local|uid=99999,cn=users,cn=accounts,dc=telem,dc=local' |cut -b 5- | xargs -I '{}' ldappasswd -x -D "cn=Directory Manager" -H ldaps://srv-idm-qa-01.telem.local -w **#mdp_directory_manager#** -s 118 '{}'


# Mise à jour de l'annuaire IDM de FORM depuis la Prod

1 - Vider l'annaire IDM
ldapsearch -D "cn=Directory Manager" -H ldaps://srv-idm-form-01.telem.local -x -w **#mdp_directory_manager#**  -b cn=users,cn=accounts,dc=telem,dc=local -s one dn 100 |grep dn: |grep -Ev 'uid=mir_user,cn=users,cn=accounts,dc=telem,dc=local|uid=systel,cn=users,cn=accounts,dc=telem,dc=local|uid=admin,cn=users,cn=accounts,dc=telem,dc=local|uid=admin-c2c,cn=users,cn=accounts,dc=telem,dc=local|uid=ro_user,cn=users,cn=accounts,dc=telem,dc=local|uid=rw_user,cn=users,cn=accounts,dc=telem,dc=local|uid=pwmuser,cn=users,cn=accounts,dc=telem,dc=local|uid=testuser,cn=users,cn=accounts,dc=telem,dc=local|uid=99999,cn=users,cn=accounts,dc=telem,dc=local' |cut -b 5- | ldapdelete -x -D "cn=Directory Manager" -H ldaps://srv-idm-form-01.telem.local -w **#mdp_directory_manager#** 

2 - Lancer l'application OractleToLDAP.exe qui se trouve dans le P:\ECADIS\ldap\

2.0 - Modifier l'IP dans le fichier de config (10.144.2.176)
2.1 - Attention à vérifier que le PC ECA a la possiblité de joindre l'IP de l'IDM (10.144.2.176)
2.2 - Vérifier les paramètres de la configuration dans le fichier .conf
2.2.1 - TnsName.ORA => Nom de la base Oracle
2.2.2 - Accept_empty_email = 1 (1 = on accepte tous. 0 = on accepte uniquement les gens qui ont un email)
