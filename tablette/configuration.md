<!-- TITLE: Configuration -->
<!-- SUBTITLE: Préparation tablette -->

## Procédure v117.4
Si L.E.G.O est sur l'écran demandant le LOGIN et MOT DE PASSE (comme pour MyStart+) il faut alors procéder à cette opération :

-	Avant une prise à distance, faire sortir la tablette du Launcher par AirWatch
	-	Liste View / Chercher la tablette / More Actions 
Appuyer sur la roue cranté en bas à droite de l'écran LEGO
Appuyé plusieurs fois sur la flêche ronde "Raffraichir la liste des SDIS" (il n'y a pas d'animation informant l'utilisateur que le refresh est terminé attention)
Appuyé sur "Fermer" pour sortir de la boite de dialogue
Renseigner le champ « Utilisateur » avec **admin**
Renseigner le champ « Mot de passe utilisateur » avec le MdP dans keepass ProSDIS "ECA LEGO PROD"
Dans la liste des SDIS, choisir le libellé **ECA**
Cocher la case « Utiliser un certificat »
Avec la boite de dialogue qui s’ouvre, sélectionner le fichier certificat adéquat. (il se trouve dans "Download")
Mot de passe du certificat : qwerzt
Appuyer sur le bouton « connexion »


EDIT : 04.01.2020 Mise à jour de la procédure pour LEGO


# Procédure d'enrollement SM-T395
**Ne concerne que les tablette VHC SM-T395 (Active Tab2)

- Contrôler que l'IMEI est dans KME (https://www2.samsungknox.com/en/user cred telematique@eca-vaud.ch)
- Section "Knox Mobile Enrollement"
- Dans la section Device, rechercher l'IEMEI de l'appareil
	- Si ce n'est pas le cas, demander à Codalis d'ajouter l'IEMEI de l'appareil dans KME
	- L'IMEI est référencer dans ECADB ou au dos de l'appareil sur l'autocollant interne, il faut enlever la batterie pour le voir)
- Le profile "vhc" doit être assigner, ce profile enverra l'appareil dans le groupe "configuration / VHC" dans Airwatch
- Allumer l'appareil, le garder sous charger est conseillé
- Séléctionner la langue "Français suisse"
- Accepter uniquement le "contrat utilisateur final" cliquer sur suivant en bas à droite
- "Ignorer pour l'isntant" dans la section "Copie anciennes données..." et "Suivant" en bas à droite
- Connecter l'appareil a un wifi qui peut accèder à internet (attention trop d'appareil sur le même wifi ralenti la rapidité des téléchargements et ralenti la configuration global)
- Attender les message de Mise à jour Knox et vérification
- Un menu "Configurer l'appareil" s'affiche, il explique l'installation automatique de l'agent Airwatch "Hub", Accepter et continuer
- Attender que la configuration se fait. Cela prend plusieurs minutes.

# Procédure de configuration
## # Démarrage
- Démarrer la tablette
- Séléctionner Français (Suisse)
- Suivant
- Décocher Donnée de diagnostic
- Suivant
- Suivant
- Ignorer
- Ignorer quand même
- décocher les Aidez-nous
- Suivant
- Ignorer
- Ignorer
- Insérer carte SIM et SD (avec carto)
- Désactiver la rotation auto
- Aller dans les paramètres
- Réseaux Mobile
- Nom des points d'accès
- Ajouter
- Nom = m2m
- APN = shared.m2m.ch
- Enregistrer
- Séléctionner l'APN
- Affichage - Mise en veille sur 30min
- Ecran verrouillage - Désactiver le mode de déverrouillage


## Enrollement
- Cabler la tablette au PC
- Copier l'APK airwatch
- Installer l'APK airwatch
- Aller sur https://srv-airwatch-acc.telem.local
- Accounts
- Séléctionner son utilisateur
- Dans la paramètre de l'utilisateur, choisir ou enroller les tablettes (Demander à SBO en cas de doute)
- Add Device en haut à droite
- Save
- Lancer Agent Airwatch sur la tablette
- Scanner le QR Code reçu dans le mail
- Faire suivant jusqu'à la fin
- Attendre le téléchargement des applications et profils

## Attribution d'une licence Sygic
- Aller sur https://bls.sygic.com/#/
- Cliquer sur Emergency Truck (celui ou il y a 800 licences)
- Deactivate Licenses et entrer l'IMEI de la tablette sur laquelle on veut supprimer la Licence
- Activate Licenses et enter l'IMEI de la tablette à activer
## Configuration des applications
- Lancer le raccourci Certificate
- Lancer le raccourci Host Adder VHC ou CI
- Suivre les informations pour configurer Host
- Lancer Brightness Manager
- Lancer Smart Manager
- Cliquer sur la batterie
- Détail
- Désactiver
- Lancer Sygic
- Cliquer sur Emergency Truck
- Choisir Switzerland
- Lancer le téléchargement
- Dans les paramètre véhicule, choisir camion et faire suivant
- Ignorer l'offre premium

## Configuration Lego
- Lancer Lego
- Si aucune license valide est presente
- URL = lego.118-vaud.ch:443
- Cocher SSL
- Cocher Certifcat
- Choisir le certificate dans Download (fichier .p12)
- Password certificate = qwertz
- Identifiant =  admin
- password = adminxx (xx = numéro du jour, 25 pour 25 septembre par exemple)
- Créer une licence
- Choisir le profil
- Description = Vxxxxxx pour VHC et Cxxxxxx pour CI (xxxxxx = numéro ECA, 060595 par exemple)
- Créer la licence
- Choisir la licence dans la liste
- Démarrer
- Choisir le centre ECA Materiel et Véhicule (pour tablette de stock) ou le centre désiré
- Choisir le véhicule Stock01 (pour tablette de stock) ou le VHC désiré (Choisir STOCK01 pour les tablette CI de stock aussi)
- Aller dans Menu
- Système
- Tout sélectionner
- Synchroniser (a faire plusieurs fois si nécessaire)
- Revenir sur Menu - Acceuil
- Contrôler que les fleches soit vers et que le status soit Disponible

## Configuration Launcher
- Lancer le Launcher
- Entrer en mode admin
- password = codalis
- Activer les notifications
- Désactiver le mode admin
- Redémarrer la tablette

- Faire un contrôle général (démarrage sur Launcher, Fleche Lego)
- Emballer la tablette et livrer

