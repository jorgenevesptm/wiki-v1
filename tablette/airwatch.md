<!-- TITLE: Airwatch -->
<!-- SUBTITLE: How to use Airwatch -->

# Connexion à Airwatch
Voici l'adresse de la page de management d'airwatch : https://srv-airwatch-acc.telem.local

Vous pouvez utiliser votre compte AD telem pour vous connecter par exemple : telem.local\jba
## Consultation de devices
Pour consulter l'état d'un device, je conseille deux façons :

1.  Taper l'IMEI de la tablette dans la recherche (on le trouve derrière la tablette ou dans ECADIS)
2.  Taper le numéro ECA de la tablette (on le trouve derrière la tablette ou dans ECADIS)
# Page d'un device
Sur la page Sumarry d'un device, nous avons des informations très utiles :

1. Profiles (nous indique sur la tablette a bien reçu tous les profiles)
2. Apps (nous indique l'état des applications installées sur la tablette, le auto apps est important est doit être vert)
3. Device Info (nous indique l'état de la RAM, le numéro de série (pratique pour teamviewer) ainsi que les groupes auxquels la tablette est associée)
4. Barre d'était en haut (nous indique la date d'enrollement, l'était de connexion à airwatch (AWCM STATUS), la dernière fois qu'elle a été vu par le système)

## Divers onglets

En navigant dans Profiles ou APPS, on peut forcer la suppression ou l'installation d'un profil ou d'une application sur le device :

Dans More, nous avons le menu Network qui nous est très utile.

Il nous indique les informations réseau, on y retrouve l'IMEI de la tablette et surtout le numéro de carte SIM (pratique pour la rechercher dans le portail m2m Swisscom)

## Changement de groupe, renommage, suppression

Dans le menu More Actions en haut à droit de la page d'un device, nous avons des paramètres Admin très important.

- Reboot Device, pour redémarrer la tablette si elle ne répond plus à teamviewer
- Device Wipe, pour détruire une tablette (attention ça bloque totalement la tablette et la rend unitilisable à jamais)

- Change Organization Group, si nous souhaitons passer une tablette du groupe TEST au groupe FORM par exemple.
- Edit Device, pour renommer le Label en haut de la page
- Delete Device, pour supprimer complètement une tablette du système (attention ça supprime toutes les applications installées par Airwatch)

- Enter Admin Mode, active le mode admin à distance, ça peut être pratique si on a quelqu'un sur le terrain qui a besoin de faire une manipulation en dehors du launcher.

# Ajout d'un device
Pour ajouter un device, nous devons installer Airwatch Agent sur la tablette en question. (se trouve sur le NAS télématique ou directement sur le google play)

Une fois l'application Agent lancé, il nous propose de scanner un QR Code.

Pour générer le QR Code, il faut aller sur Airwatch, ensuite naviguer sous Accounts, choisir son utilisateur.

Sur le Summary de notre utilisateur, on remarque dans Grouping qu'il y a un champ Enrollment Organization Group. Ce qui signifie que les tablettes ajoutés par cette utilisateur iront dans cet Organization Group.

Pour changer ce groupe, Edit en haut à droite.

Ensuite, Add Device en haut à droite.

Spécifier la méthode d'envoi du QR Code, je conseille de mettre EMAIL.

Faites SAVE et vous recevrez par email le QR Code à scanner depuis la tablette.

Suivez ensuite la procédure sur la tablette et le tour est joué.

Pour retrouvez la tablette dans airwatch, taper le numéro de série ou l'IMEI dans la recherche et renommer ensuite le device en mettant le numéro ECA dans le label.

# Gestion des applications
Dans la rubrique de gauche APPS & BOOKS, vous pouvez ajouter des applications ou bookmarks.

## Ajout d'une nouvelle application
- Faire ADD APPLICATION en haut de la page
- Choisir l'organization group associé (on met toujours le plus haut qui est ECA)
- Uploader l'apk
- Informations de l'APK (normalement tout est déjà rempli, si il manque le nom du package, il faut aller chercher sur internet)
- Assignation à un groupe de devices

## Mise à jour d'une application
- Cliquer sur l'application à mettre à jour
- Faire ADD VERSION en haut à droite
- Uploader le fichier
- Assignation à un groupe de devices (les même groupes que sur la version actuelle seront déjà ajouté, si vous supprimez ces groupes lors de la mise à jour, ça ne supprime pas l'application de ces devices mais ça ne la met pas à jour)

# Gestion des groupes
Dans la rubrique de gauche Groups & Settings, vous pouvez ajouter, modifier et supprimer des groupes d'assignements et des groupes d'organisation.

## Organization Groups
Les groupes d'organisation servent à structurer l'arborescence d'Airwatch.

Voici l'arborescence actuelle de l'infrasctructure Airwatch :

PHOTO LIST VEW GROUPS ORGANIZATION

On peut assigner des profils et des applications directement aux Organization Groups.

Pour ajouter un Organization Group :

- Aller sur Details
- En haut de l'écran, aller jusqu'au groupe auquel vous souhaitez y ajouter un groupe fils
- Cliquer sur Add Child Organization Group
- Remplir les champs nécessaires
- Faites SAVE

## Assignment Groups
Les groupes d'assignement ou Smart Groups dans Airwatch permet de grouper des utilisateurs provenant de sous branches différentes. On peut ensuite assigner des profils ou des applications directement à ces Smart Groups.

Pour créer un Smart Group :

- Cliquer sur ADD SMART GROUP en haut de l'écran
- Donner un nom à votre Smart Group
- Séléctionner des Organization Group à mettre dans le Smart Group
- Ou séléctionner SELECT DEVICES OR USERS et ajouter manuellement device par device peu importe leur Organization Group.
- Faites Save

