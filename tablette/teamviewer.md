<!-- TITLE: Teamviewer -->
<!-- SUBTITLE: Comment se connecter via Teamviewer -->

# Connexion via Teamviewer
## Récupérer le numéro de série
Une fois qu'on voit à donner le numéro ECA ou le numéro IMEI de la tablette, vous pouvez l'entrer sur https://srv-airwatch-acc.telem.local pour récupérer le numéro de série.

Pour vous connecter sur Airwatch, mettez telem.local\jba par exemple suivi de votre mot de passe.

Dans la List View de la tablette en question, vous trouverez le Serial Number dans Device Info sans la forme suivante : R52J20M05JZ

Une fois vous aurez tous accès à ECADIS, toutes les informations utiles y seront.

## Connectez vous sur Teamviewer
Le compte télématique est inscrit dans le keypass PROSDIS.

Une fois connecté, allez sous Ordinateurs et Contacts et entrez le numérro de série dans la liste.

La tablette choisi apparaitra à l'écran sous la forme suivante : Samsun_SM-TX85_R52J20M05JZ

Le logo petit écran peut avoir 3 statuts : 
* bleu = prêt à etre connecté
* jaune = en veille mais peut etre réveilé à distance (dans la plupart des cas)
* noir = tablette déconnecté ou éteinte

Si la tablette est noir, vous pouvez essayer de la redémarrer via Airwatch. Quand vous etes sur la fiche de la tablette, cliquez sur More Action - Reboot Device.

