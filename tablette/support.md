<!-- TITLE: Support -->
<!-- SUBTITLE: List d'action de support pour les tablettes -->

# Support tablettes

## Réaplication de profil

* Se connecter à la console Airwatch avec votre compte AD avec le domain inscrit "telem.local\" dans le champs login
	* par exemple telem.local\cri
* Naviguer dans "Devices" -> "List View"
* Dans la recherche à droite entrer l'identification de la tablette (n° ECA situé au dos de la tablette)
	* Peut aussi filtrer via numéro de série, IMEI et d'autre identifiant mais le n° ECA est le plus facile et rapide à obtenir
	* Le numéro ECA est un numéro à 6 Digit usuellement situé au dos de la tablette sur un autocollant ECA
	* Pour les tablettes VHC (Active tab 2 en 2020) il est nécessaire d'enlever la fourre caoutchouc pour y trouver le numéro au dos
* Sélectionner l'appareil afficher en cliquant dessus
* Naviguer dans l'onglet "Profils"
* Sélectionner le profil souhaité en cliquant sur la puce sur la gauche dans la liste des profil
	* Pour réinstaller la configuration de Launcher, sélectionner le profil avec le mot Launcher ou "Bureau sécurisé" pour les tablette CI
* Cliquer sur le boutton "Install" au dessus de la liste
	* Le boutton ne s'affiche pas si il n'y a pas de sélection de profil