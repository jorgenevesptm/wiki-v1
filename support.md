<!-- TITLE: Support -->
<!-- SUBTITLE: A quick summary of Support -->

# Les liens pour le support

* <a href="http://wiki.telem.local/support/depannages-divers">Problèmes récurrents et procédures de rétablissement</a>

* <a href="http://wiki.telem.local/support/services-par-serveur">Liste des services par serveur</a>

* <a href="http://wiki.telem.local/support/chuv-pic">Accès à PIC au CHUV</a>

* <a href="http://wiki.telem.local/support/recuperation-logs-apres-crash">Récupération Logs Apres Crash ESX ou SAN</a>



