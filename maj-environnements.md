<!-- TITLE: Mise à jour des environnements -->
<!-- SUBTITLE: A quick summary of Maj Environnements -->

# Procédure de mise à jour des environnements des Test et Formation

Procédure pour la mise à jour des données de TEST :

0 - Informer le métier que la copie des données va débuter, et que les PO, PC Remote, BO, doivent être coupés!
1 – Appel de Systel pour informer du début des opérations (à ce moment STEL donne le GO / NOGO).
2 – Systel arrête les ETL et commence la copie des données Start
3 – STEL donne le GO à STI (JDA / MPL) pour la copie des données admin (peut être lancé en même temps que la partie Systel vu que les ETL sont arrêtés)
4 – STEL attend la confirmation de la fin de la copie des données des 2 côtés (normalement Systel prend à peine quelques minutes alors que pour la partie admin il faut attendre quelques heures)
5 – STEL demande à Systel de réactiver les ETL ;
6 – STEL procède à la MàJ de l’annuaire IdM, selon la procédure dans le Wiki : wiki.telem.local/idm/mise-a-jour-annuaire-id-m
7 – STEL informe les utilisateurs (.DDIS-ProSDIS_Users) que la mise à jour est terminée.

Pour la mise à jour des données de formation la procédure est la même sauf qu’il ne faut pas faire la mise à jour IdM.



## Rafraichissement des données environnement ProSDIS

<table>
    <caption>FMO / 24.10.2018 v1</caption>
    <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col"></th>
						<th scope="col">Durée [min.]</th>
						<th scope="col" style="width:180px;">DDIS</th>
						<th scope="col" style="width:180px;">Systel</th>
						<th scope="col" style="width:180px;">Telem</th>
						<th scope="col" style="width:180px;">STI</th>
						<th scope="col" style="width:180px;">SDN</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row" colspan="8" style="text-align:left">Préparation / 1 semaine avant</th>
        </tr>
        <tr>
            <th scope="row">1</th>
            <td>Préparer en PROD les données pour utilisation en TEST & FORM</td>
						<td  style="text-align:right">-</td>
						<td>Comptes utilisateurs</td>
						<td>PO START</td>
						<td>Rasters / données carto</td>
						<td></td>
						<td>ALR de test</td>
        </tr>
				<tr>
            <th scope="row">2</th>
            <td>Préparer les données de TEST & FORM</td>
						<td  style="text-align:right">-</td>
						<td style="background-color:gray"></td>
						<td style="background-color:gray"></td>
						<td>Licences Lego</td>
						<td style="background-color:gray"></td>
						<td style="background-color:gray"></td>
        </tr>
				<tr>
            <th scope="row">3</th>
            <td>Assurer alignement des environnements en termes de release Start, ETL, ...</td>
						<td  style="text-align:right">-</td> 
						<td style="background-color:gray"></td>  <!-- DDIS -->
						<td style="font-weight:bold; text-align:center;"> <span>X</span> </td> <!-- Systel -->
						<td style="font-weight:bold; text-align:center;">X</td> <!-- Telem -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- STI -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">4</th>
            <td>Vérifier horaire de synchro tablettes LEGO</td>
						<td  style="text-align:right">-</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">5</th>
            <td>Vider bases annuaire TRIP et Paging</td>
						<td  style="text-align:right">-</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- SDN -->
        </tr>
				<tr>
					<th scope="row" colspan="8" style="text-align:left">Jour J copies</th>
				</tr>
				<tr>
            <th scope="row">1</th>
            <td>Arrêter les interfaces TRIP, ALR manger, IP RCT</td>
						<td  style="text-align:right">20</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">2</th>
            <td>Suspendre les transferts (SGA, ALR, TRIP)</td>
						<td  style="text-align:right">20</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">3</th>
            <td>Suspendre les transferts ETL Paging</td>
						<td  style="text-align:right">5</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">4</th>
            <td>Suspendre les services web</td>
						<td  style="text-align:right">10</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">5</th>
            <td>Copie des données SGA selon procédure STI</td>
						<td  style="text-align:right">180</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">6</th>
            <td>Copie des données SGO selon procédure Systel - <span style="font-size: small;">NB : pas de purge des interventions en TEST</span></td>
						<td  style="text-align:right">120</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">7</th>
            <td>Contrôler compteur des interventions</td>
						<td  style="text-align:right">5</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">8</th>
            <td>Contrôler licences LEGO</td>
						<td  style="text-align:right">5</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">9</th>
            <td>Copie des données SDN selon sa procédure <span style="font-size: small;">(pas de copie Record Manager)</span></td>
						<td  style="text-align:right">10</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">10</th>
            <td>Copie cartographie</td>
						<td  style="text-align:right">5</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">11</th>
            <td>Contrôle des données</td>
						<td  style="text-align:right">30</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
					<th scope="row" colspan="8" style="text-align:left">Post opération de copie</th>
				</tr>
				<tr>
            <th scope="row">1</th>
            <td>Forcer le PW 118 sur TEST & FORM</td>
						<td  style="text-align:right">-</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">2</th>
            <td>Relancer les services web</td>
						<td  style="text-align:right">15</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">3</th>
            <td>Relancer les ETL</td>
						<td  style="text-align:right">15</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- STI -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">4</th>
            <td>Relancer ETL Paging</td>
						<td  style="text-align:right">15</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">5</th>
            <td>Relancer les interfaces TRIP, ALR Manager, IP RCT</td>
						<td  style="text-align:right">15</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- SDN -->
        </tr>
				<tr>
            <th scope="row">6</th>
            <td>Contrôles techniques des appplications - <span style="font-size: small;">En particulier : CRSS et LEGO (gestion des licences)</span></td>
						<td  style="text-align:right">180</td>
						<td style="background-color:gray"></td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- Telem -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- STI -->
						<td style="font-weight:bold; text-align:center;">X</td><!-- SDN -->
        </tr>
				<tr>
					<th scope="row" colspan="8" style="text-align:left">Réception du Métier</th>
				</tr>
				<tr>
            <th scope="row">1</th>
            <td>Tests métier</td>
						<td  style="text-align:right">-</td>
						<td style="font-weight:bold; text-align:center;">X</td><!-- DDIS -->
						<td style="background-color:gray"></td><!-- Systel -->
						<td style="background-color:gray"></td><!-- Telem -->
						<td style="background-color:gray"></td><!-- STI -->
						<td style="background-color:gray"></td><!-- SDN -->
        </tr>
    </tbody>
</table>


