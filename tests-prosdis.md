<!-- TITLE: Tests Prosdis -->
<!-- SUBTITLE: Liens utils et information générale (Env. TEST) -->

# Informations
## Tablettes LEGO TEST
	- JJ :  060533 - 070525
	- YSC : 060412 - 060413
	- DLO : 070522
	- JCK : 060413

## Salle de TEST (bat 58)

![Salletestbat 58](/uploads/salletestbat-58.png "Salletestbat 58")

### Tableau PC Tests

| Nom | IP | IP-Remote | Occupant | Commentaires |
| ------ | --- | --------------- | -------- | -------------------- |
| PC-TEST 1 | 10.118.122.51 | 10.144.254.58 | IC | |
| PC-TEST 2 | 10.118.122.52 | 10.144.254.59 | CTA | |
| PC-TEST 3 | 10.118.122.53 |  | Télématique | |
| PC-TEST 4 | 10.118.122.54 |  | Télématique | |
| PC-TEST 5 | 10.118.122.55 |  | Systel |  |
| PC-TEST Renf | 10.118.122.57 |  |  |  |

### Utilisateur windows pour TEST (version actuelle 117.4)

**User -** TEST
**Pwd -** voir keepass
**Domain -** cta.local

#### Utilisateurs Start Test

**User :** TEST_TEL_1 à TEST_TEL_3
**Pwd :** TEST_TEL_# numéro du login apondu

### Utilisateur windows pour DEV (version actuelle 118)

**User -** DEV
**Pwd -** voir keepass
**Domain -** cta.local

#### Utilisateurs Start Dev (SSO)

**User :** Utilisateur AD
**Pwd :** mot de passe AD

## Numéros de téléphone
- **198** -> 118 (pilote urgences pompier)
- **118** -> intervenant SDIS (appel depuis le terrain)
- **222** -> serveur de mobilisation
- **111** -> administratif
- **000** -> installateur ALR

## Totem
- <span style="color: green; font-weight:bold">Vert</span> : l'opérateur n'est pas en communication
- <span style="color: orange; font-weight:bold">Orange</span> : l'opérateur est en retrait
- <span style="color: red; font-weight:bold">Rouge</span> : l'opérateur est en communication téléphonique
- <span style="color: blue; font-weight:bold">Bleu</span> : l'opérateur est en communication radio
- Flash : l'opérateur demande de l'aide au chef de salle

## iPhone
- iPhone 8 : PIN 123456, SIM 1397
- iPhone 7 : PIN 123456, pas de SIM

## MyStart
Connexion à MyStart dans l'environnement de TEST : 
1. Sur la page de connexion de MyStart, cliquer sur le bouton de paramètre ⚙️
2. Mot de passe = adminXX où XX est le jour courant -> ex. le 23 janvier : admin23
3. Dans le champ *Label* saisir *ECA_Test* (attention à la casse)
4. Saisir le NIP puis le mot de passe *118*

## Lecture EMAIL Test

Une machine virtuelle Windows 10 avec Thunderbird a été installée afin de lire les mails de mobilisation sortant.

- vm-windows-mail-qa-01 / 10.118.129.210
- mail / Bonjour17

Le raccourci RDP se trouve dans "Raccouris TEST" sur les PO de Tests

### Lecture EMAIL Entrant Test

Afin de pouvoir lire et tester les mails entrants, un client thunderbird peut être installé sur votre PC et la configuration réalisée comme suit : 

![Annotation 2020 03 03 143700](/uploads/annotation-2020-03-03-143700.png "Annotation 2020 03 03 143700")

Serveur : 10.64.1.55
Mail : cta_urgences@10.64.1.55
Nom d'utilisateur : cta_urgences
Entrant : IMAP - 143 - STARTTLS - Mot de passe normal
Sortant : SMTP - 587 - STARTTLS - Mot de passe normal 

# Outils
## Répondeur automatique mobilisation
Page web de configuration du système d'automatisation des réponses aux appels de mobilisation dans l'environnement de TEST. 
Le système simule une réponse *1#* (aquittement positif) ou *0#* (aquittement négatif) suite à un appel de mobilisation.

http://srv-linux-test-01.telem.local

SDN doit activer la bascule pour utiliser ou pas le répondeur automatique de mobilisation


Start -> Trip -> Srv-linux-test (db en local avec les options de réponse)

**ATTENTION! le téléphone de mobilisation DOIT ETRE DEBRANCHE pour que le répondeur automatique fonctionne**

### Activation / Désactivation
Le répondeur vocal est toujours activé.
Le répondeur vocal est appelé si le téléphone de mobilisation n° *000123456789* ne répond pas.

Pour faire la transition, il suffit de tirer la prise /éteindre ce téléphone.


### Serveur PHP
Le serveur PHP tourne sur srv-linux-test-01.telem.local. Les credentials sont dans le keepass ProSDIS.
```batchfile
/var/www/html
```

### Source

```batchfile
git clone git@10.96.128.150:automatic-answering-machine.git/
```

## Syslog
Consultation des logs Asterisk de mobilisation et téléphonie.
**Note:** Dans l'état actuel, nous constatons que certains messages de logs sont dropés sur le Syslog de TEST. Ceci doit faire l'effet d'un correctif sur l'aggrégateur de log/réseau (supprimer cette note une fois corrigé).

http://srv-syslog-qa-01.telem.local/

Le fichiers de log peuvent également être consulté directement via SSH: 

```batchfile
ssh administrator@10.96.31.11 #pwd devrait être dans le keepass

cd /var/log/remote/
```

## Statut des voters F8

Consultation du statut des deux voters F8. 
Voir keepass pour le mot de passe.

**Note:** Les ports 3000/1 sous Linux et 502/3 sous Windows ont été choisi arbitrairement. Ils sont modifiables, attention juste à les adapter aussi dans l'url des page web...

### Linux

```batchfile
ssh -L 3000:192.168.1.202:80 gianmarco@10.118.129.65
ssh -L 3001:192.168.1.203:80 gianmarco@10.118.129.65
```

Puis ouvrir les deux pages web : 
- localhost:3000 pour le voter A
- localhost:3001 pour le voter B

### Windows

1. Ouvrir une connexion SSH sur 10.118.129.65
2. Login *gianmarco* voir keepass pour le mot de passe
3. Ouvrir un tunnel SSH sur les deux voters 192.168.1.202 et 192.168.1.203

<img src="http://srv-affi-01.telem.local/wiki-images/putty_voters.png" alt="Config PuTTY" height="350px">

Puis ouvrir les deux pages web : 
- localhost:502 pour le voter A
![Votera](/uploads/images/votera.jpeg "Votera")
- localhost:503 pour le voter B
![Voterb](/uploads/images/voterb.jpeg "Voterb")

# Tests de Charge

## Swissdotnet

### Téléphonie

Un script pour simuler les appels téléphoniques massifs a été développé par swissdotnet afin de stresser la partie téléphonique de ProSDIS (Asterisk et StartPhone) 

Le script s'utilise comme suit : 

Pour afficher l'aide : 
```bash
python3 /opt/auto-caller/src/main.py -h
```

Ensuite on peut passer un fichier source en csv avec "," comme séparateur. Puis il y a plusieurs options utiles : 

```text
-k : kill tous les appels 
-s [link to csv] : le fichier csv à lire
-l [nombre] : répète la boucle
-c [cadence en seconde] : pour limiter la vitesse des appels
```

Il y a deux exemples de fichier csv dans /

`/opt/auto-caller/csv`

Exemple de csv : 
```csv
Source,Destination,Nombre,Intervalle
+410790000000,197,1,1
```
(nombre = nombre de fois que doit-être effectué l'appel)
(intervalle = temps avant la répétition de l'appel (ligne))

## DC-09 CLI Client

The DC-09 CLI client allow sending DC-09 messages to any IP-RCT to generate load on servers.

It works by taking a CSV file as input and following options:

```
Usage:  [-h] [--exit-on-fail] -f=<csvFile> [-g=<group>] [-i=<interval>]
      --exit-on-fail     If the soft exits when alarm could not be sent
  -f, --file=<csvFile>   Scenario file with format (account_number, key,
                           alarm/restore)
  -g, --group=<group>    How many alarm are sent grouped
  -h, --help             display a help message
  -i, --interval=<interval>
                         Interval between alarm being sent (ms)
```

The CSV file format is (header is mandatory):

```
ACCOUNT_NUMBER,KEY,ALARM,RCT_IP,RCT_PORT
```

For example:

```
java -jar sia-dc-09-cli-client.jar -f scenario-1.csv
```

with `scenario-1.csv` content:

```
ACCOUNT_NUMBER,KEY,ALARM,RCT_IP,RCT_PORT
"1234","ABCDEFABCDEFABCDEFABCDEFABCDEFAB","NFA0001","1.2.3.4",11223
```

### Interval
The interval option (by default 1 second or 1000 milliseconds) allows waiting after each alarm (or group of alarms) after sending a new alarm.

### Group
The group option (by default 1) allows grouping alarms together after waiting on the interval. It might be used to simulate bursts. 

## Organisation
![Organigramme](/uploads/organigramme.png "Organigramme")