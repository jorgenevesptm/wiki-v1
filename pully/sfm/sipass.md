<!-- TITLE: Sipass -->
<!-- SUBTITLE: Connexion au serveur sipass -->

# Sipass
Serveur Siemens utilisé par SFM pour le contrôle d'accès.
Il est utilisé pour la configuration de badge pour les nouveaux collaborateur de l'ECA par SFM.

Connexion via vnc à l'adresse 172.27.29.30
MdP sur Keepass Telematique