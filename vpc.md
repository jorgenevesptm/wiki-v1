<!-- TITLE: Vpc -->
<!-- SUBTITLE: Dépannages divers -->

# Tablettes domotique
Deux tablettes Wago sont disposées dans le VPC (vers chacune des portes de la cellule arrière). 

Si elles ne réagissent plus (pas d'action qui suit la mise en ou hors service), le Wago peut être redémarré (credentials dans le keepass).

http://172.29.xx.20/wbm

Se loguer via le lien en haut à droite de la page, les credentials sont dans le Keepass. 

Ensuite, aller dans le menu Administration -> Reboot. 

Pour accéder à la WebVisu :

http://172.27.xx.20/webvisu/webvisu.htm


Les adressages des véhicules sont les suivants (remplacer les xx) : 

LOSA0        10
RIVIERA0    20
ZORBA0     40
YDON0      50

# Télécommande de secours
Si la télécommande ne fonctionne plus (physiquement), il est possible d'accéder à une télécommande virtuelle via la Webvisu du Wago (http://172.27.xx.20/webvisu/webvisu.htm). 

Se rendre dans télécommande de secours et se connecter (credentials dans le Keepass). 
# Antenne satellite
Il est possible de déployer ou ranger l'antenne satellite de façon manuelle. Attention à bien déconnecter le connecteur situé sous l'antenne, sur le toît du véhicule, avant toute opération manuelle ! Une douille de 13 est nécessaire pour ce faire. 

# Prise à distance des consoles TRIP
Pour prendre les consoles TRIP à distance via VNC, il est nécessaire de démarrer le service x11vnc via SSH. 

La marche à suivre est la suivante :

* Se connecter à la console en SSH (172.29.x1.40/41)
* Exécuter la commande x11vnc (attention, en fermant la connexion SSH le service se stop. Pour éviter cela, rajouter un "&" après la commande). 
* Utiliser VNC client pour se connecter à la console (directement via son IP). 

Une fois la connexion terminée avec VNC, le service se stop. Il est nécessaire de le relancer en SSH pour reprendre une nouvelle session. 



# Dépannage poste opérateur
**Symptômes :**

Le poste opérateur démarre, se log, puis affiche un écran noir. Il est possible d'ouvrir le Task Manager (CTRL + SHIFT + ESCPAPE) ou d'avoir le menu avec la combinaison CTRL + ALT + DEL mais rien d'autre. 

**Cause probable : **

Corruption de certains fichiers systèmes. Le temps entre l'extinction "soft" et l'extinction "hard" (via la prise) est très court. En cas de mise à jour du système et d'installation de certains éléments, il se peut que le PC soit éteint de manière malpropre dans un processus critique. 

**Résolution :**

Nécessite une connexion internet, ou à défaut, de télécharger le dossier de ressources Windows pour la réparation. 

Ouvrir une invite de commande en admin (depuis le compte administrateur, task manager -> fichier -> Exécuter une nouvelle tâche -> cmd (cocher en tant qu'admin). 

DISM /Online/Cleanup-image /Restorehealth (peut prendre 15 à 20 minutes suivant les cas)
sfc /scannow

https://support.microsoft.com/fr-fr/topic/utilisez-l-outil-v%C3%A9rificateur-des-fichiers-syst%C3%A8me-pour-r%C3%A9parer-les-fichiers-syst%C3%A8me-manquants-ou-endommag%C3%A9s-79aa86cb-ca52-166a-92a3-966e85d4094e