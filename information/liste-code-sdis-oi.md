<!-- TITLE: Liste Code Sdis Oi -->
<!-- SUBTITLE: A quick summary of Liste Code Sdis Oi -->

# Liste code SDIS et OI

## Liste code SDIS
cd_sdis	li_sdis
S-132	Lausanne - Epalinges
S-608	Haut-Lac
S-609	Mèbre
S-610	Etraz Région
S-612	Sorge
S-650	SDISPE
S-665	Terre Sainte
S-673	Région Venoge
S-674	Nyon - Dôle
S-680	Gland-Serine
S-685	Gros-de-Vaud
S-690	Morget
S-701	Chablais
S-702	Vallée de Joux
S-710	Ouest-Lavaux
S-711	Alpin
S-733	Coeur de Lavaux
S-735	de Malley, Prilly et Renens
S-738	Chamberonne
S-741	Riviera
S-755	Haut-Talent
S-765	régional du Nord vaudois
S-766	Plaine de l'Orbe
S-767	Haute-Broye
S-768	de la Broye - Vully
S-769	Vallorbe Région
S-770	Ste-Croix/Pied-de-la-Côte
S-777	Les Salines
S-780	Fortifications
S-790	Oron-Jorat
S-795	ALSP_GE
S-801	Céligny
S-810	Monthey
S-811	Vouvry
S-812	CIMO
S-814	Office cantonal du feu VS
S-820	Estavayer-le-Lac
S-821	Morat
S-822	Romont
S-823	Bulle
S-824	Châtel-St-Denis
S-831	Gstaad
S-888	SDIS ECA
S-889	ECA-ASSURANCE
S-891	SDIS ECA-STL
S-892	ECA CTA
S-893	ECA-Télématique
S-894	ECA-Général
S-895	Infra Formation
S-896	ECA-STL
S-900	DGE - DIREV
S-901	OFCO Office de la consommation
S-903	DGE-Eau
S-904	DGE-GEODE
S-905	DGE - Foret
S-906	DGE-BIODIV
S-907	DGAV

## Liste code OI

cd_oi	li_oi
132C1	Epalinges DPS
132H1	Lausanne DPS
132I1	Lausanne UAPP
132J1	Soutien sanitaire opérationnel
132K1	SPSL Lausanne (ressources sanitaires)
132Z1	Lausanne DAP
132Z2	LAUSANNE Personnel manifestation
132Z3	Epalinges DAP
608D1	Haut-Lac DPS
608Z1	Haut-Lac DAP
609A1	Romanel-sur-Lausanne DPS
609B1	Cheseaux-sur-Lausanne DPS
609C1	Mont-sur-Lausanne DPS
609Z1	Mont-sur-Lausanne DAP
609Z2	Cheseaux-sur-Lausanne DAP
609Z3	Romanel-sur-Lausanne DAP
610B1	Bière DPS
610B2	Gimel DPS
610C1	Rolle DPS
610E1	Aubonne DPS
610Y1	Gilly DAP
610Y2	Longirod DAP
610Z1	Aubonne DAP
610Z2	Rolle DAP
610Z4	Gimel DAP
610Z5	Perroy DAP
610Z7	Mollens DAP
612C1	Bussigny DPS
612C2	Crissier DPS
612Z1	Bussigny DAP
612Z2	Crissier DAP
650B1	Rougemont DPS
650F1	Château-d'Oex DPS
650Y1	L'Etivaz DAP
650Z1	Château-d'Oex DAP
665D1	Terre Sainte DPS
665Z1	Terre Sainte DAP
673A1	La Sarraz DPS
673B1	L'Isle DPS
673E1	Penthalaz DPS
673Y1	Boussens DAP
673Z1	Cossonay-Penthalaz DAP
673Z2	L'Isle DAP
673Z3	La Sarraz DAP
673Z5	Veyron-La Tine DAP
674A1	Genolier DPS
674B1	St-Cergue DPS
674G1	Nyon DPS
674N1	PROV (protection respiratoire de l'ouest vaudois)
674Y1	Bonmont  DAP
674Z1	Nyon DAP
674Z2	St-Cergue DAP
674Z3	Genolier DAP
680D1	Gland DPS
680Y1	Serine DAP
680Z1	Gland DAP
685A1	Bercher DPS
685E1	Echallens DPS
685Y1	Essertines-Yverdon DAP
685Y2	Etagnières DAP
685Y3	Goumoëns DAP
685Y4	Jorat-Menthue DAP
685Y5	Montilliez DAP
685Z1	Echallens DAP
685Z2	Bercher DAP
685Z3	Oulens-sous-Echallens DAP
685Z4	Orzens DAP
685Z5	Villars-le-Terroir DAP
690B1	St-Prex DPS
690B2	Denges DPS
690F1	Morges DPS
690Y1	Yens-Lully DAP
690Y2	Apples DAP
690Y3	Senoge-Arénaz DAP
690Y4	Vufflens-le-Château DAP
690Z1	Morget relève SIS DAP
690Z2	Morget EFO DAP
690Z3	Denges DAP
701F1	Aigle DPS
701Z1	Chablais DAP Plaine
701Z2	Chablais DAP Montagne
702B1	L'Abbaye-Le Lieu DPS
702F1	Le Chenit DPS
702Z1	Le Chenit DAP
702Z2	L'Abbaye-Le Lieu DAP
710B1	Belmont DPS
710C1	Pully DPS
710C2	Lutry DPS
710Z1	Pully DAP
710Z2	Lutry DAP
710Z3	Belmont DAP
711B1	Les Mosses DPS
711C1	Les Diablerets DPS
711E1	Leysin DPS
711Z1	Leysin DAP
711Z2	Les Diablerets DAP
711Z3	Les Mosses DAP
733B1	Cully DPS
733C1	Forel DPS
733Z1	Forel DAP
733Z2	Cully DAP
733Z3	Puidoux DAP
733Z4	Savigny DAP
735C1	Prilly DPS
735C2	Renens DPS
735Z1	Prilly DAP
735Z2	Renens DAP
738A1	Chavannes DPS
738C1	Ecublens DPS
738Z1	Ecublens DAP
738Z2	Chavannes DAP
741C1	St-Légier DPS
741C2	Jongny DPS
741G1	Montreux DPS
741G3	Vevey DPS
741Z1	Montreux DAP
741Z2	Vevey DAP
741Z3	St-Légier DAP
741Z5	Jongny DAP
741Z6	Les Avants DAP
755B1	Haut-Talent DPS
755Z1	Haut-Talent DAP
765B1	Yvonand DPS
765B2	Concise DPS
765C1	Grandson DPS
765G1	Yverdon-les-Bains DPS
765Y1	Donneloye DAP
765Y2	Belmont-sur-Yverdon DAP
765Y3	Montagny-près-Yverdon DAP
765Y4	Bonvillars DAP
765Z1	Chavannes-le-Chêne DAP
765Z5	Yverdon-les-Bains DAP
766F1	Orbe DPS
766Y1	Croy DAP
766Y2	Chavornay DAP
766Y3	Baulmes DAP
766Z1	Orbe-Bofflens DAP
766Z3	Rances DAP
766Z4	Bavois DAP
767B1	Thierrens DPS
767E1	Moudon DPS
767Y1	Lucens DAP
767Z1	Moudon DAP
767Z2	Montanaire (Thierrens) DAP
767Z3	Montanaire (Peyres-Possens) DAP
767Z4	Forel DAP
767Z5	Chesalles DAP
768A1	Vully DPS
768B1	Valbroye DPS
768C1	Avenches DPS
768F1	Payerne DPS
768Y1	Grandcour DAP
768Y2	Prévonloup DAP
768Z1	Combremont-Le-Grand DAP
768Z2	Valbroye DAP
768Z3	Villarzel DAP
768Z4	Corcelles-près-Payerne DAP
768Z5	Avenches DAP
768Z6	Chabrey DAP
769E1	Vallorbe DPS
769Y1	Nozon DAP
769Z1	Vallorbe DAP
769Z2	Ballaigues DAP
770B1	Pied de la Côte DPS
770F1	Ste-Croix DPS
770Z1	Ste-Croix DAP
770Z2	Pied de la Côte DAP
777A1	Ollon DPS
777B1	Gryon DPS
777C1	Bex DPS
777E1	Villars DPS
777Z1	Salines Plaine DAP
777Z2	Salines Montagne DAP
780D1	Lavey-St-Maurice DPS
780Z1	Lavey-St-Maurice DAP
790B1	Mézières DPS
790E1	Oron DPS
790Y1	Palézieux DAP
790Y2	Vucherens DAP
790Z1	Oron DAP
790Z2	Mézières DAP
795Z1	ALSP
801	Céligny
810F1	Monthey
811F1	Vouvry
CIM	CIMO
814	Office cantonal du feu VS
EST-L	Estavayer-le-Lac
MUR	Morat
ROM	Romont
BUL	Bulle
CHA	Châtel-St-Denis
831F1	Gstaad
888AD	Administratif
888CF	CFECA
888DD	Info DDIS (via SMS => Personnel ECA)
888FO	Personnel cours ECAFORM
888GG	ECA Sites Général-Guisan
888IC	Inspectorat
888IN	Instructeurs
888IS	Inspecteurs de districts
888OR	ORCA SDIS
888RA	La Rama
888RZ	ECA site C.F. Ramuz
889EG	Evénement généralisé Assurance
889SS	Service sinistres ECA
891MA	Matériel STL Stock
891OP	Matériel STL opérationnel
892DE	ECA - Débordement CTA
892EM	ECA - EM CTA
892OP	ECA - Opérateurs CTA
893TE	Service télématique
894CF	ECA Centre de formation
894CO	ECA Service communication
894DG	ECA Direction
894GG	ECA Secouristes Général-Guisan
894RZ	ECA Secouristes CF-Ramuz
895TR	Technique RAMA
896OP	Matériel STL opérationnel
900	DGE - DIREV
901	OFCO - Division Denrées alimentaires
903CE	DGE - Cours d'eau
904GE	DGE - GEODE
905FO	DGE - Foret
906BD	DGE-Biodiversité
907AV	Affaires vétérinaires


