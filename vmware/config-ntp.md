<!-- TITLE: Configuration NTP -->

# Configuration de NTP sur un serveur Linux avec timedatectl
Edit file /etc/systemd/timesyncd.conf

Add NTP=srv-ntp-01.telem.local


```text
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
# Entries in this file show the compile time defaults.
# You can change settings by editing this file.
# Defaults can be restored by simply deleting this file.
#
# See timesyncd.conf(5) for details.

[Time]
NTP=srv-ntp-01.telem.local
#FallbackNTP=ntp.ubuntu.com

```

# Set new config
Copy all lignes ...
```text
sudo timedatectl set-ntp on && \
sudo timedatectl set-timezone Europe/Zurich && \
sudo systemctl restart systemd-timedated.service && \
sudo systemctl restart systemd-timesyncd.service
```

# Check status
		
```text
timedatectl
```

Si la configuration est ok, le résultat doit être

```text
      Local time: Tue 2018-12-11 15:51:22 CET
  Universal time: Tue 2018-12-11 14:51:22 UTC
        RTC time: Tue 2018-12-11 14:51:22
       Time zone: Europe/Zurich (CET, +0100)
 Network time on: yes
NTP synchronized: yes
 RTC in local TZ: no
```
	
