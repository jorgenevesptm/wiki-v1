<!-- TITLE: Récupérer une VM dans VirtualBox -->
<!-- SUBTITLE: A quick summary of Récupérer une VM dans VirtualBox -->

# Récupérer une sauvegarde d’une VM et la lancer avec VirtualBox
Il faut récupérer le backup dans Veeam. Attention il faut récupérer que les disques !

Créer une VM dans VirtualBox en choisissant le disque de la sauvegarde ou ajouter le disque directement à une VM existante.
Pour ajouter un disque à une VM existante, il faut aller dans Storage -> Controller:SATA +

Si lors de l’ajout du disque, il y a une erreur, il faut convertir le disque par la commande suivante :

`VBoxManage convertfromraw srv-sig-telem-01-flat.vmdk srv-sig-telem-01.vdi`

## Nouvelle VM - configuration

Pour accéder aux services, il faudra faire du NAT avec du port forwarding.

<img src="/uploads/virtualbox-network-nat.png" alt="virtualbox network nat config" style="margin-left: 20px; width:550px;">

## Configuration des interfaces réseau
Probablement que les interfaces sont différentes en VMware et VirtualBox.
Se connecter dans la VM et faire la cmd : `ip addr`
L’interface à utiliser par défaut sera enp0s3.

Remplacer la configuration réseau (ip static) par du dhcp

`sudo nano /etc/netplan/50-cloud-init.yaml`

```text
network:
    ethernets:
        enp0s3:
            dhcp4: true
    version: 2
```


`sudo nano /etc/network/interfaces `  (ancienne version)

```text
auto enp0s3
iface enp0s3 inet dhcp

```

