<!-- TITLE: Cache Serveur Linux -->
<!-- SUBTITLE: A quick summary of Cache Serveur Linux -->

# Serveur de cache
Un serveur de cache pour les fonctions apt et les mises à jours a été déployé

**srv-apt-01.telem.loca**l / *10.96.128.5*

# Paramètrage

Il suffit de modifier le fichier 01acng sous /etc/apt/apt.conf.d/ avec les paramètres suivants : 

> Acquire::http {
>         Proxy "http://srv-apt-01.telem.local:9999";
>         };

# Firewall
Une règle a été implémentée dans les 3 Firewall pour les machines du groupe **GRP_LINUX_ECA** pour laisser passer les flux