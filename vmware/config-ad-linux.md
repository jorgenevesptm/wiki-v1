<!-- TITLE: Config Ad Linux -->
<!-- SUBTITLE: Authentification à l'aide de l'Active Directory -->

# Préparation
- Suppression du fichier de cache Proxy (voir http://wiki.telem.local/vmware/cache-serveur-linux)
- Téléchargement et installation en SUDO des packages (attention à bien mettre à jour apt-get avant)
> sudo apt-get install winbind samba libnss-winbind libpam-winbind krb5-config krb5-locales krb5-user
 # Configuration Kerberos
 - Modification du fichier de config Kerberos
> sudo nano /etc/krb5.conf
- Copier la configuration suivante
> [libdefaults]
> ticket_lifetime = 24000
> default_realm = TELEM.LOCAL
> default_tgs_entypes = rc4-hmac des-cbc-md5
> default_tkt__enctypes = rc4-hmac des-cbc-md5
> permitted_enctypes = rc4-hmac des-cbc-md5
> dns_lookup_realm = true
> dns_lookup_kdc = true
> dns_fallback = yes
> kdc_timesync = 1
> ccache_type = 4
> forwardable = true
> proxiable = true
> fcc-mit-ticketflags = true
> [realms]
> TELEM.LOCAL = {
> kdc = TELEM.LOCAL
> default_domain = TELEM.LOCAL
> }
> [domain_realm]
> .telem.local = TELEM.LOCAL
> telem.local = TELEM.LOCAL
> 
> [appdefaults]
> pam = {
> debug = false
> ticket_lifetime = 36000
> renew_lifetime = 36000
> forwardable = true
> krb4_convert = false
> }
> 
> [logging]
> default = FILE:/var/log/krb5libs.log
> kdc = FILE:/var/log/krb5kdc.log
> admin_server = FILE:/var/log/kadmind.log

# Test de connexion
- Tester la connexion Kerberos à l'aide de la commande 
> kinit DOMAINE_USER@TELEM.LOCAL

- A ce moment, vous devriez pouvoir entrer votre mot de passe Active Directory et une réponse positive doit vous être donnée

# Configuration de Samba
- Modifier le fichier de configuration de Samba
> sudo nano /etc/samba/smb.conf
- Copier la configuration suivante (faite un backup du fichier d'origine Samba avant)
- Attention à modifier le nom **NETBIOS** (max 15 caractères)
> [global]
>         workgroup = TELEM
>         realm = TELEM.LOCAL
>         netbios name = SRV-MGT-LX-SDN
>         security = ADS
>         dns forwarder = 10.96.128.39
> 
> idmap config * : backend = tdb
> idmap config *:range = 50000-1000000
> 
>    template homedir = /home/%D/%U
>    template shell = /bin/bash
>    winbind use default domain = true
>    winbind offline logon = false
>    winbind nss info = rfc2307
>    winbind enum users = yes
>    winbind enum groups = yes
> 
>   vfs objects = acl_xattr
>   map acl inherit = Yes
>   store dos attributes = Yes

# Configuration NSS
- Configuration de NSS pour que le système utlise winbind pour le login 
> sudo nano /etc/nsswitch.conf
- Copier la configuration suivante 
> passwd:         compat winbind
> group:          compat winbind
> shadow:         compat
> gshadow:        files
> 
> hosts:          files dns
> networks:       files
> 
> protocols:      db files
> services:       db files
> ethers:         db files
> rpc:            db files
> 
> netgroup:       nis

# Jonction au domaine
- Le mot de passe de "readAD" se trouve dans le KeePass ProSDIS
> sudo net ads join -U readAD
- L'erreur DNS n'est pas blocante pour la suite
- Contrôler sur le serveur Active Directory que le serveur s'est bien inscrit (srv-pdc-telem-01 - 10.96.128.39)

# Restart des services et tests
> sudo service winbind restart; service nmbd restart; service smbd restart

- Après avoir fait un restart des services (ou de la machine), tester si les commandes suivantes donnent en résultat les informations provenant de l'AD

> wbinfo -u
> wbinfo -g
> wbinfo -i USER_DOMAINE => 
dlo::50000:50004:Dan DLO. Loup:/home/TELEM/dlo:/bin/bash
> getent passwd 
> getent group

- Les commandes "getent" permettent d'obtenir toutes les informations liés aux comptes utilisateurs. Il faut attendre quelques secondes pour que les données provenant de l'AD se chargent 

# Intégration de PAM
- PAM permet de gérer l'authentification et les autorisations 
> pam-auth-update
- Dans la commande qui s'ouvre (très rose), cocher les 4 options (à l'aide d'espace) et confirmer avec Enter

# Ajout des groupes AD pour les droits
- Pour ajouter les droits root à un groupe AD, procédez ainsi
> visudo
- Sous "# Allow members of group sudo to execute any command" ajouter les groupes nécessaires (attention, ajouter un "\" avant un espace)
>%groupe\ swissdotnet ALL=(ALL) ALL
>%groupe\ télématique ALL=(ALL) ALL

Tester d'utiliser votre login AD
Ne pas ajouter de remettre le proxy avant la mise en production de la machine

