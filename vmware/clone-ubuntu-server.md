<!-- TITLE: Clone Ubuntu Server -->
<!-- SUBTITLE: A quick summary of Clone Ubuntu Server -->

# Actions à faire après le clone du serveur par VMWare
Depuis VMWare client, se connecter à la machine

Si besoin : modifier la mémoire de glassfish :

### List current JVM options

```text
asadmin list-jvm-options
```

### Change JVM Options

```text
asadmin delete-jvm-options -Xmx4096m
asadmin create-jvm-options -Xmx1024m
```

1. arrêter le serveur glassfish : sudo service glassfish stop
2. Pour le nom du serveur, changer les fichiers : /etc/hostname et /etc/hosts
3. Pour la configuration réseau, changer le fichier : /etc/network/interfaces
4. Commande pour activer l'interface eth0 : sudo ifup eth0
5. ( Si nécessaire - Commande pour désactiver l'interface eth0: sudo ifdown eth0 )
6. Faire un restart du serveur : sudo reboot

### Test
Se connecter par ssh au serveur

### Problème de connexion réseau
Si la commande pour remonter l'interface réseau échoue bien que l'option *Conecté* soit affiché dans les paramètres de vSphere.
1. Mettre la machine hors tension : clique droit sur le serveur -> alimentation -> mettre hors tension
2. Actions -> Modifier les paramètres -> cocher la case *Connecter lors de la mise sous tension* sous *Adapateur réseau 1*
