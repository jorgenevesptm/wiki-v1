<!-- TITLE: Server Linux - ajouter d’un nouveau disque sans rebooter :) -->
<!-- SUBTITLE: A quick summary of Server Linux - ajouter d’un nouveau disque sans rebooter :) -->

# Server Linux - ajouter d’un nouveau disque sans rebooter :)
Dans vSphere, ajouter le nouveau disque dans la VM

<img src="/uploads/linux-add-new-disk.png" alt="Linux Add New Disk" style="margin-left: 20px; width:550px;">

Se connecter dans la VM par ssh et passer en root : `sudo -i`

### Créer et Formater la partition

Rescanner les SCSI HOST : `echo "- - -" > /sys/class/scsi_host/host0/scan`

Mettre host0 , host1, hostX.

Afficher les partitions des disques : `fdisk -l`
Le nouveau disque doit apparaître /dev/sdb par exemple.

Créer la nouvelle partition dans le nouveau disque : `fdisk  /dev/sdb`

Tapper : m pour avoir la liste des commandes

Il faudra tapper **n** (add a new partition) puis enter pour avoir les valeurs par défaut (primary, number 1, ...)

Puis **w** pour sauvegarder. Vérification `fdisk -l`

Formater la nouvelle partition en ext4 : `mkfs.ext4 /dev/sdb1`

### Monter la partition

Définir où le disque sera monté, par exemple : `mkdir -p /mnt/data`

Pour connaître les id des disques : `blkid`

Ajouter un point de montage dans fstab, il faudra adapter UUID en fonction du résultat de la commande blkid


```text
UUID=6cfefc98-2a68-4364-916a-c53302f7a9db /mnt/data       ext4   errors=remount-ro  0        1
```


Pour éditer fstab: `nano /etc/fstab`

Pour monter le disque : `mount /dev/sdb1`

Vérification : `df -h` qui devrait retourner dans le résultat ->  /dev/sdb1       493G   20M  493G   0%  /mnt/data


