<!-- TITLE: Veeam -->
<!-- SUBTITLE: A quick summary of Veeam -->

# VEEAM

## Informations

**Serveur** : 10.80.110.84 - srv-backup-repo

**Physique** : Serveur HPe DL380 Gen9
**Disques** : 2 LUN de 8 disques avec un total de 5'868Gb - Utilisé pour les backups des machines Systel
**Disques** : 1 NAS Synology, raccordé via un iSCSI Connector Windows directement dans la VM - Utilisé pour les backups de nos VM

## Jobs

![Backup](/uploads/backup.png "Backup")

## Rapports

https://srv-backup-repo.cta.local:1239/Dashboard/#Main

Aller dans "VEEAM BACKUP AND REPLICATION"

Pour voir les VM non protégées 

- PROTECTED VMS (en bas à gauche)
- Clique sur le menu 
- Drill Down Report
- Create Report 

## Montage du disque Synology

Ouvrez une invite de commandes et tapez diskpart.

À l’invite DISKPART, tapez list disk. Notez le numéro du disque manquant ou hors connexion que vous voulez mettre en ligne. Les disques manquants portent les numéros M0, M1, M2, etc. ; les disques hors connexion portent les numéros 0, 1, 2, etc.

À l’invite DISKPART, tapez select disk <disknumber>.

À l’invite DISKPART, tapez online disk

## Restauration de fichier

Restauration fichier avec VEEAM 10
1.	Dans « Home » allez sur « Backups » puis « Disk »
 
2.	Faire click-droit le job « PROD_ECA_NAS » puis « Restore » et « Files and folders »
3.	Puis choisir le fichier à restaurer, click-droite et choisir si vous souhaitez écraser, ou pas, le fichier (si existe toujours)
 
4.	Vérifier ensuite que le fichier est bien restauré 
