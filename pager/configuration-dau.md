<!-- TITLE: Configuration Dau -->
<!-- SUBTITLE: Configuration DAU -->
# Etape pour configuration DAU
Port 9001 (PP1)

Connection en Telnet sur les DAU depuis le srv-pp qui est SLAVE
__ATTENTION__ Il faut kill le logiciel DAG4000 (alarme sur sup Paging, avertir le CTA)
Kill aussi process nscp.exe (client nagios) pour éviter le redémarrage auto de DAG4000 (processus à relancer après intervention)

Plage de VREF +- 10 (décimal) attention faire les conversions en hexadécimal pour la configuration du VREF
## Configuration VREF:

`<PAR><VREF>#007b #0085</VREF></PAR>`

## Configuration DAU Id:
`<PAR><DID>54</DID></PAR>`
`<PAR>`
`<VREF>#0061 #006b</VREF>`
`</PAR>`

## Polarisation modulation:

`<PAR><ITX>0</ITX></PAR>`  conf de base
`<ITX>1</ITX>`

## Choix de la radio

`<PAR><FUG>2</FUG></PAR> `

>A vallamand, FUG =1

`<PAR/>`
`<CLR/>`
`<RESET/>`
`<PFA/>`
`<VER/>`
`<PAR><VREF>#0073 #0087</VREF></PAR>`
`<VREF>#0039 #0043</VREF>`

## LAN
`<LAN/>`
`<LAN><MAC>00:50:C2:42:EC:41</MAC></LAN>`

## Test:
`<TEST>3</TEST>`
`<TEST>1</TEST>` droite
`<TEST>2</TEST>` gauche
`<TEST>0</TEST>`