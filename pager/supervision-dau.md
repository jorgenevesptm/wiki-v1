<!-- TITLE: Supervision Dau -->
<!-- SUBTITLE: Script de supervision des DAU -->

# Script de supervision des DAU
Sur chaque PP, un script ping chaque DAU et remonte le status à Nagios.

Voici ce script : il se trouve dans le dossier "C:\Program Files\NSClient++\scripts\check_dau.bat

```
@echo off
set Counter=0
set DauName=
ping -n 1 192.168.102.101 >NULL
if not %errorlevel% == 0 ( 
    set /A Counter+=1
	set DauName=%DauName%Chalv
	echo %Date%/%Time% PP2 Chalv disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.102 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Choex
	echo %Date%/%Time% PP2 Choex disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.103 >NULL
if not %errorlevel% == 0 (
    set Counter+=1
	set DauName=%DauName% Chtel
	echo %Date%/%Time% PP2 Chtel disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.104 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Corjo
	echo %Date%/%Time% PP2 Corjo disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.105 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Diabl
	echo %Date%/%Time% PP2 Diabl disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.106 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Gcour
	echo %Date%/%Time% PP2 Gcour disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.107 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Lesen
	echo %Date%/%Time% PP2 Lesen disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.108 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Ligne
	echo %Date%/%Time% PP2 Ligne disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.109 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Lucen
	echo %Date%/%Time% PP2 Lucen disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.110 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Mnthy
	echo %Date%/%Time% PP2 Mnthy disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.111 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Moudo
	echo %Date%/%Time% PP2 Moudo disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.112 >NULL
if not %errorlevel% == 0 ( 
    set /A Counter+=1
	set DauName=%DauName% Nyon
	echo %Date%/%Time% PP2 Nyon disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.113 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Goumv
	echo %Date%/%Time% PP2 Goumv disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.114 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Stecr
	echo %Date%/%Time% PP2 Stecr disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.115 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% UNIL
	echo %Date%/%Time% PP2 UNIL disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.116 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Ville
	echo %Date%/%Time% PP2 Ville disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.117 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Yverd
	echo %Date%/%Time% PP2 Yverd disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.118 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% TDI
	echo %Date%/%Time% PP2 TDI disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.124 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Valla
	echo %Date%/%Time% PP2 Valla disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.125 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Mex
	echo %Date%/%Time% PP2 Mex disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.97 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Dole
	echo %Date%/%Time% PP2 Dole disconnected >>logdauPP2.txt
)
ping -n 1 192.168.102.98 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Dvaul
	echo %Date%/%Time% PP2 Dvaul disconnected >>logdauPP2.txt
)
ping -n 1 172.25.58.126 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Vevey
	echo %Date%/%Time% PP2 Vevey disconnected >>logdauPP2.txt
)
ping -n 1 172.25.57.127 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Herma
	echo %Date%/%Time% PP2 Herma disconnected >>logdauPP2.txt
)
ping -n 1 172.25.32.128 >NULL
if not %errorlevel% == 0 (
    set /A Counter+=1
	set DauName=%DauName% Lusse
	echo %Date%/%Time% PP2 Lusse disconnected >>logdauPP2.txt
)

if %Counter% == 1 (
echo WARNING: Le DAU de %DauName% est hors ligne
exit /B 1
)
if %Counter% GEQ 2 (
echo ERROR: %Counter% DAU sont hors ligne : %DauName% 
exit /B 2
)
if %Counter% == 0 (
echo OK: Tous les DAU sont en ligne
exit /B 0
)
```

Il remplit également un fichier texte de log disponible ici : "C:\Program Files\NSClient++\scripts\logdauPP1.txt" et "C:\Program Files\NSClient++\scripts\logdauPP2.txt"

Pour que Nagios récupère cette information, il faut ajouter le script dans le fichier nsclient.ini sur chaque PP stocké dans "C:\Program Files\NSClient++"

```[/settings/external scripts/scripts]
check_dau = scripts\check_dau.bat
```

Ensuite dans Nagios, créer un service check_nrpe standard et mettre comme argument "check_dau" pour que le script soit exécuté à chaque fois que Nagios le demande.
