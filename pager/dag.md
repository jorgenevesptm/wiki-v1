<!-- TITLE: Dag -->
<!-- SUBTITLE: Alerte srv-pp-01 -->

# Symptôme
Alerte sur l'hyperviseur concernant un serveur de paging

# Cause
Logiciel DAG4000 n'opère plus correctemernt

# Solution
* Récupéré l'IP du serveur via IP Admin
		* Très probable que le problème viens du server 10.144.1.10 (srv-pp-01)
* Se connecter en ** RDP** sur le serveur
* Login Administrateur
* Mot de pass dans Keepass ProSDIS
* Une fois la connection établie
	* Quitter le logiciel DAG4000 (la fenêtre est probablement déjà ouverte directement), fermer la fenêtre suffi
	*  **NE FERMER QUE DAG4000**
	*  Double cliquer sur l'icone DAG4000 sur le bureau
	*  Attendre quelques instant que logiciel se charge
	*  Cliquer sur l'arborescence dans **Zones** sur la gauche et ouvrez chaque sites
	*  Sur chaque site, il doit apparaitre une bar qui décroie (démarrant en vert vers rouge)
	*  Quitter le RDP, **NE PAS FERMER LA SESSION**