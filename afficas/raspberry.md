<!-- TITLE: Raspberry -->
<!-- SUBTITLE: A quick summary of Raspberry -->

# Préparation d'un nouveau raspberry (modèle 3B+)

1 - Copier en local l'image qui se trouve dans smb://ha-syno-01/data/SAE/ProSDIS-START/05%20Affichage%20caserne/PIimage/3B+/afficas3b+shrank.img

2 - Insérer une carte SD (8GB minimum) dans le lecteur et lancer la commande: sudo dd bs=4M of=/dev/mmcblk0 if=~/afficas3b+shrank.img

3 - Une fois l'image copiée, insérer la carte dans le raspberry, brancher clavier/souris, cable réseau, HDMI et à la fin l'alimentation. Le raspberry s'alume. * 
( * Normalement, le expandfs doit se faire tout seul lors du premier démarrage pour occuper la totalité de la carte SD. Si non, il faut le faire manuellement via l'interface raspiconfig)

4 - Entrer les informations demandées (Nom et caserne d'attribution)

5 - Autoriser le nouvel appareil dans afficas-admin.118-vaud.ch

6 - Si besoin, copier les parametres d'affichage / informations hors alarme depuis un autre affichage du même centre