<!-- TITLE: Srv Airwatch Remote 01 -->
<!-- SUBTITLE: Info Serveur srv-airwatch-remote-01 -->

# Serveur Assist Airwatch
Le service Assist est le serveur nécessaire pour le remote control sur Airwatch (Workspace One).

Le serveur est asocié à l'adresse https://remote.118-vaud.ch avec le certificat *.118-vaud.ch

## Redémarrage des services

Si besoin de redémarrer les services d'Assist, il est nécessaire de le faire dans l'ordre suivant:

Éteindre tout les services *AetherPal* l'ordre d'extinction n'est pas important.

Activer les services *AetherPal* dans l'ordre suivant !

1. AetherPal Service Coordinator(automatic)
2. AetherPal Data Tier Proxy(automatic)
3. AetherPal Management Entity(automatic Delayed)
4. AetherPal Messaging Entity(automatic Delayed)
5. AetherPal Tool Controller(automatic Delayed)
6. AetherPal Connection Proctor(automatic Delayed)