<!-- TITLE: Ntp -->
<!-- SUBTITLE: Configuration NTP Cellnet site radio -->

# NTP Cellnet radio

## Connexion SSH

Connecter au serveur cellnet en ssh avec utilisateur "cellnet", mot de passe dans Keepass Telematique (Sites/_COMMUN_) "Serveur de supervision".

## Daemon NTP
Daemon NTP par défaut d'Ubuntu est systemd
La configuration se fait en éditant le fichier timesyncd.conf `sudo nano /etc/systemd/timesyncd.conf`

```text
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
# Entries in this file show the compile time defaults.
# You can change settings by editing this file.
# Defaults can be restored by simply deleting this file.
#
# See timesyncd.conf(5) for details.

[Time]
NTP=10.118.110.70
FallbackNTP=10.118.110.71

```

NTP = 10.118.110.70 => cta-srv-ntp-01 (le ntp à Pully)
FallbackNTP = 10.118.110.71 => 
cdc-srv-ntp-01 (le ntp à Echallens au CDC en backup)

Par défaut le champ NTP et Fallback est commenté, il faut enlever le dièse (#) pour activer la prise en compte des paramètres dans le fichier de configuration.

Plus de détails via la commande `man timesyncd.conf`

## Configuration timedatectl

L'outil de gestion de la date/heure d'Ubuntu est "timedatectl"

Il faut configurer 3 points:
* Définition de la timezone pour la correction des +2h car le ntp est en UTC et pas GMT
	* `sudo timedatectl set-timezone Europe/Zurich`
* Activation de la prise en compte du NTP
	* `sudo timedatectl set-ntp true`
* Control de la configuration
	* `timedatectl status`

Exemple de réponse

```text swissdotnet@ville-server-01:~$ sudo timedatectl status
Local time: Tue 2020-07-14 10:13:42 CEST
Universal time: Tue 2020-07-14 08:13:42 UTC
RTC time: Tue 2020-07-14 08:13:42
Time zone: Europe/Zurich (CEST, +0200)
Network time on: yes
NTP synchronized: yes
RTC in local TZ: no

```
