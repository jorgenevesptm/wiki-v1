<!-- TITLE: Tn Ericsson -->
<!-- SUBTITLE: A quick summary of Tn Ericsson -->

# Switching sur TN Ericsson
## Création d'un WAN
Le WAN permettra le transport des VLANs entre les TN via les FH (TRUNK)

Une NPU permet la gestion de 5 WAN = 5 directions différentes (dans le réseau actuel, nous avons maximum 3 directions par site).

- Sous l'arborescente de la NPU
- Choisir un RL-IME
- Cliquer droit sur le RL-IME
- Configure
- Sous Settings, Ajouter le Packet Link Available dans Assigned Packet Links (un Packet Link = une MMU, il contient le numéro de position de la carte par ex. 1/3/1)
- Save
- Choisir un Switch port libre sous Ethernet -> Ethernet Switch
- Cliquer droit dessus -> Configure
- Changer le Port Role pour I-NNI (I-NNI = Trunk, UNI = Access)
- Un port WAN apparait maintenant sous Ethernet -> WAN Interfaces
- Cliquer droit sur le WAN créé (ex. WAN 1/7/100)
- Configure -> General
- Sous interface Usage, Choisir le switch port configuré avant et mettre le port sur admin status UP
- Save
- Cliquer droit sur Ethernet Switch-> Configure -> VLAN
- Créer un VLAN trunk qui acceuillera les autres VLAN
- Choisir un ID de VLAN
- Choisir un Nom
-  Sous Ports / Member, Ajouter tous les ports concernant par le WAN (port accès qui sera dans le trunk ainsi que les port WAN)
-  Sous Ports / Untagged, Ajouter tous les ports accès
-  Save

## Activer le RSTP sur les ports WAN
Le RSTP ou Rapid Spanning Tree permettra d'éviter les boucles réseaux entre les TN.

- Cliquer droit sur Ethernet Switch -> Configure -> Spanning Tree
- STP Enable coché
- Cliquer sur Configure Rapid Spanning Tree
- Dans le tableau, cocher Port Enable pour les interfaces WAN
- Save

## Création de VLAN et Attribution aux interfaces LAN

- Choisir un Switch Port libre
- Cliquer droit dessus
- Configure
- Changer le Port Role pour UNI
- Save
- Sous Ethernet -> Lan Interfaces -> Choisir le LAN désiré
- Cliquer droit dessus
- Configure -> General
- Sous Interface Usage, choisir le switch port aucun le LAN sera rattaché
- Sous General, Admin Status mettre sur UP
- Save
- Cliquer droit sur Ethernet Switch -> Configure -> VLAN
- Créer un nouveau VLAN
- Choisir son ID
- Choisir un Nom
- Sous Ports / Member, choisir le Switch Port configuré avant
- Sous Ports / Untagged, laisse sur None
- Save

## Création de Liens Layer 1
Dans le cadre du renouvellement des routeurs de sites, nous avons choisi de faire le routage au niveau des Fortigates. De ce fait, il faut créer des liens Layer 1 entre les FH et les ports LAN de chaque MMU.

### Associer un lien FH sur un WAN

- Allez sur la NPU, cliquer droit sur RL-IME 1/7/100 (choisir le RL-IME qu'on désire) et faire Configure
- Sous Settings, Ajouter le Packet Link (qui correspond au lien FH désiré)
- Faire Save
- Faire de même pour chaque lien FH
- Sous Ethernet -> Layer 1 Connection, cliquer droit et Configure
- Créer un nouveau lien Layer 1
- Choisir un LAN et le WAN correspondant (WAN de la même MMU que le LAN)
- Faire Save
- Dans chaque MMU3 A, cliquer droit sur LAN
- choisir Configure -> Général
- Il dois y avoir sous interface Usage = Layer 1 si le mapping a bien été réalisé
- Mettre le Admin Status sur Up
- Définir le Alias Name avec le lien en question (ex: VILL-UNIL) 
