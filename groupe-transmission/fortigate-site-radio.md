<!-- TITLE: Fortigate Site Radio -->
Configuration des Fortigates sur Site Radio
# Configuration FW externe
## Configuration interface
A Définir :

- Méthode de raccordement des FH Pully sur FW Externe
- Actuellement port 8 dc1-fw-externe-01 raccordé sur PULLYB58
## Configuration OSPF
Sous Network>OSPF
- Router id 1.1.1.29
- Area : 0.0.0.0
- Networks :
		- 172.27.173.0/24 area 0.0.0.0
		- 10.96.0.0/16 area 0.0.0.0
- Interfaces :
		- PULL54-VILL port8

Pour vérifier qu'on reçoit bien les routes OSPF des autres sites : 

Aller dans Monitor>Routing Monitor

## Configuration VPN 
Tous les routeurs se connectent sur le même tunnel VPN à Pully.

Pour configurer l'accès VPN à Pully, aller sur VPN>IPsec Wizard

- Create New
- Name : VPN-Sites
- NAT Configuration : The remote site is behind NAT
- Incoming interface : Wan port9
- Preshared-key -> dans le Keepass ProSDIS sous Fortigate_Sites_Radio > Key VPN IPSEC
- Local interface : DMZ-Access-VPN
- Local subnet 0.0.0.0/0
- remote subnet 172.27.0.0/16
- Interne access : None
- Create

Ensuite Editer le VPN créé
- convertir en Custom
- Authentication : IKE Version 2
- Phase 2 Selectors : advanced> cocher Autokey Keep Alive

## QoS
Gestion : https://cookbook.fortinet.com/traffic-shaping-priq/
Traffic shaping permet de prioriser les flux selon des règles FW
Policy & Object
	- Traffic Shapper
	- Suppression des pré-config
	- Création d'un traffic shapper "VLAN_RADIO"
		- Priority = High
		- BD Garantie = 10'000kb/s
	- Traffic Shapping Policy
			- Création d'une règle (QoS_LAN_RADIO)
			- Création d'un range d'adresse
				-"LAN_RADIO"
				- Color orange
			  - Type = range
			  - 172.27.192.0/28
		- Destination = les liens FH 

## Configurer la route VPN comme backup
- se logger en CLI
- config vpn ipsec phase1-interface
- edit "nom du vpn"
- set distance 200
- end

# Configuration FW site
## Avant l'installation
- Ajout du Fortigate dans GLPI
- Création d'une étiquette avec les informations
- Licenciement du Fortigate
## Configuration de base
Modification du hostname (dashboard - main - hostname)
Network / interface
	- Création de deux "hardware switch" (port 4-6-8 "LAN_RADIO - port 1-14 "LAN_SUP)
		- Utiliser les IP du fichier Excel
		- LAN_RADIO => activation du PING et du "Device Detection"
		- LAN_SUP => activation de toutes les fonctions Forti sauf "Telemetry"
		- Activation du DHCP sur le LAN_SUP - range .251-.254
	- Laisser les ports 15 et 16 - SFP libre
	- Port wan1, wan2, ha1, ha2
		- Alias = FH1-2-3-4
		- Role = LAN
		- /!\ - IP .1 toujours en haut (WAN1 et HA1)
		- Activation du PING et "Device Detection"
		- Ajout de l'IP selon fichier Excel
	- Création d'une interface "FortiExtender
		- Name 4G
		- Alias "FortiExtender_40D"
		
## FortiExtender Configuration Step

Connexion : IP 192.168.1.2
Mettre à jour le fortiExtender avec le FW 4.0.0
Mettre à jour le modem V.18 
Modification de l'IP fixe du FortiExtender par l'IP définitive

Branchement sur le port 14 du FortiGate

Ajout du FortiExtender dans le FortiGate (voir doc : https://fortinetweb.s3.amazonaws.com/docs.fortinet.com/v2/attachments/d6ac43b8-3ad6-11e9-94bf-00505692583a/FortiExtender-4.0.0-Admin-Guide.pdf)
https://cookbook.fortinet.com/fortiextender-installation/
## Configuration VPN 

Chaque routeur Fortigate doivent être configuré avec un accès VPN à travers le Fortiextender 4G

Pour configurer l'accès VPN, aller sur VPN>IPsec Wizard

- Name : VPN-xxx (xxx = nom du site)
- NAT Configuration : This site is behind NAT
- IP Address : 195.20.147.130
- Outgoing Interface : Fortiextender
- Pre-shared key -> dans le keypass télématique sous Sites>Commun>Clé VPN
- Local interface : LAN_SUP
- Local Subnets : 172.27.xx.0/24 (xx = site)
- Remote Subnets : 10.0.0.0/8
- Internet Access : None
- Create

Ensuite Editer le VPN créé
- convertir en Custom
- Authentication : IKE Version 2
- Phase 2 Selectors : advanced> cocher Autokey Keep Alive

## Configuration OSPF
Sous Network>OSPF
*  Router ID : fichier Excel 
* Areas : 0.0.0.0 
* Networks : ajout de chaque réseaux 
* Interfaces : LAN_SUP, LAN_RADIO, FH1 et FH2 
![Annotation 2019 06 14 142234](/uploads/annotation-2019-06-14-142234.png "Annotation 2019 06 14 142234")
Pour vérifier qu'on reçoit bien les routes OSPF des autres sites : 

Aller dans Monitor>Routing Monitor

## Règles FW
![Image Sans Titre](/uploads/image-sans-titre.png "Image Sans Titre")