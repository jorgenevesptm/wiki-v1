<!-- TITLE: Nouvel utilisateur -->

# Créer un nouvel utilisateur

Créer l'utilisateur dans l'AD (telem.local - 10.96.128.39)
Ne pas oublier d'ajouter le groupe "Utilisateurs Radius"

Créer l'utilisateur dans le FortiAutgenticator 
	User Management - Remote Users - RADIUS - Create NEW
	User Management - User Groups - Utilisateurs RADIUS - Ajouter l'utilisateur aux acceptés
	
Créer l'utilisateur dans le FortiManager
	Object Configuration - User&Device - Créer l'utilisateur ou voir si il s'est créé tout seul
	User Group - Rajouter l'utilisateur dans le bon groupe 