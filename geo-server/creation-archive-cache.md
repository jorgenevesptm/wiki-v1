<!-- TITLE: Création Archive Cache -->
<!-- SUBTITLE: A quick summary of Création Archive Cache -->

# Procédure pour créer l’archive du cache des couches cartographiques
## Démarche
1. Se connecter à un des GeoServer TEST ou FORM. Voir le keePass pour l’url et utilisateur / mot de passe
2. Se connecter par ssh sur le serveur nfs où le cache est présent. Voir la partie stockage sur les serveurs nfs http://wiki.telem.local/geo-server
3. Se mettre en root par la commande su -
4. Aller dans le répertoire /DATA/geoserver/geoserver_data/external_data/eca_vaud/blobstore/
5. find eca_vaud_07_Orthophoto | cpio -o > 20190320-07.cpio
6. si plusieurs couches sont à mettre dans l’archive find eca_vaud_01_Fond_ASIT_VD eca_vaud_02_Fond_cadastral_ASIT_VD eca_vaud_03_Routes | cpio -o > 20190320-01-02-03.cpio