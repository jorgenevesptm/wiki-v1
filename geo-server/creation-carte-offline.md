<!-- TITLE: Création Carte Offline -->
<!-- SUBTITLE: A quick summary of Création Carte Offline -->
# Procédure pour la création de la carte offline destinée aux tablettes LEGO
Remarque : la création de la carte offline ne se fait pas sur la PROD mais en TEST pour ne pas impacter les performances en production.

Prérequis : mettre à jour les différentes couches utilisées notamment les BH.

## Configuration des couches dans GeoServer
Vérifier qu’il y a 2 couches agrégées : Carte SD offline 01 et Carte SD offline 02

## Configuration de «  Carte SD offline 01 »
![Config carte offline 01](/uploads/geoserver/screenshot-from-2019-08-16-15-23-16.png "Screenshot From 2019 08 16 15 23 16")

## Configuration du cache
![Config carte offline 01 - cache](/uploads/geoserver/screenshot-from-2019-08-16-15-25-42.png "Screenshot From 2019 08 16 15 25 42")

## Configuration de «  Carte SD offline 02 »
![Config carte offline 02](/uploads/geoserver/screenshot-from-2019-08-16-15-26-23.png "Screenshot From 2019 08 16 15 26 23")

La configuration du cache est identique à la carte 01

## Génération des tuiles du cache
![Menu génération du cache](/uploads/geoserver/screenshot-from-2019-08-16-15-32-27.png "Screenshot From 2019 08 16 15 32 27")

## Carte SD offline 01

Aller dans Seed/Truncate. Mettre les paramètres ci-dessous

Le temps de traitement sera d’environ 10 minutes. La taille du cache environ 450 MB

![Carte Offline 00 04](/uploads/geoserver/carte-offline-00-04.png "Carte Offline 00 04")

## Carte SD offline 02

Pour cette couche le cache se calcule en 2 phases.

Aller dans Seed/Truncate. Mettre les paramètres ci-dessous

### Calcul du cache phase 1

Le temps de traitement sera d’environ 60 minutes. La taille du cache environ 14 GB

![Carte Offline 05 06](/uploads/geoserver/carte-offline-05-06.png "Carte Offline 05 06")

### Calcul du cache phase 2

Pour les zooms de 08 à 10, on utilise un rectangle qui va délimiter le canton pour réduire le nombre de tuile.

Il faut spécifier : Bounding box	2494300	1115000	2585500	1198700

Le temps de traitement sera d’environ 5 heures. La taille du cache au final sera d’environ 60 GB

![Carte Offline 07 08](/uploads/geoserver/carte-offline-07-08.png "Carte Offline 07 08")

## Création de l’archive du cache avec la cmd cpio
  
Se connecter par ssh au GeoServer TEST `systel@10.144.2.54`
 
Voir le keePass pour l’url et utilisateur / mdp
    
Se connecter par ssh sur le serveur nfs : `ssh 10.118.112.8` la console doit afficher systel@srv-geo-nfs-test
    
Se mettre en root par la commande `su -`

Aller dans le répertoire /DATA/geoserver/geoserver_data/external_data/eca_vaud/blobstore/

Créer l'archive avec : `find eca_vaud_Carte_SD_offline_01 eca_vaud_Carte_SD_offline_02 | cpio -o > YYYYMMDD-offline.cpio &`
		
& : pour mettre la cmd en background. Pour voir le job : `jobs`

Optionnel : si on veut quitter / fermer la console sans arrêter sa commande en cours, il faut exécuter : `disown`

## Récupération de l’archive avec la cmd scp
`scp systel@10.144.2.54:/mnt/shared/geoserver_data/external_data/eca_vaud/blobstore/YYYYMMDD-offline.cpio .`

## Extraire l'archive dans un répertoire YYYYMMDD-offline [20190801-offline]

Sous linux cpio -D YYYYMMDD-offline -idu < YYYYMMDD-offline.cpio

## Copie des fichiers dans la carte SD

Dans la carte SD, avoir les répertoires carte/offline

Copier à l'intérieur du répertoire offline, les différents répertoires Offline_00 à Offline_08
















