<!-- TITLE: Copie Archive Cache -->
<!-- SUBTITLE: A quick summary of Copie Archive Cache -->

# Procédure pour copier le cache des couches cartographiques sur un autre serveur
## Démarche

1. Se connecter à un des GeoServer TEST ou FORM. Voir le keePass pour l’url et utilisateur / mot de passe
2. Se connecter par ssh sur le serveur nfs où l’archive est présente. Voir la partie stockage sur les serveurs nfs
3. Aller dans le répertoire /DATA/geoserver/geoserver_data/external_data/eca_vaud/blobstore/
4. scp YYYYMMDD-archive.cpio  srv-nfs-cible:/DATA/geoserver/geoserver_data/external_data/eca_vaud/blobstore/