<!-- TITLE: Copie Rasters -->
<!-- SUBTITLE: A quick summary of Copie Raster -->

# Procédure pour copier les rasters dans les GeoServers
## Démarche

1. Se connecter à un des GeoServer TEST, FORM ou PROD. Voir le keePass pour l’url et utilisateur / mot de passe et créer un répertoire YYYYMMDD, par exemple 20190701
    Par ssh + mkdir : `ssh systel@ip-geoserver "mkdir /mnt/shared/geoserver_data/external_data/eca_vaud/raster/20190701"`

2. Il faut copier les rasters dans le répertoire du serveur : /mnt/shared/geoserver_data/external_data/eca_vaud/raster/YYYYMMDD/
    Avec scp : `scp -r CN_25-gdal/ systel@ip-geoserver:/mnt/shared/geoserver_data/external_data/eca_vaud/raster/20190701/`
 