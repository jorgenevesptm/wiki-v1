<!-- TITLE: Calcul Du Cache -->
<!-- SUBTITLE: A quick summary of Calcul Du Cache -->

# Procédure pour calculer le cache des couches cartographiques
## Généralité
Pour calculer le cache des tuiles des couches cartographiques, on utiliser GeoWebCache [GWC] la documentation est disponible
via cette url https://docs.geoserver.org/stable/en/user/geowebcache/index.html

Normalement le calcul du cache ne se fait pas sur le geoserver de production car le calcul utilise beaucoup de CPU.

Il faut utiliser les serveurs de FORM et de TEST. Une fois le cache généré, on le copie sur le serveur de production.

### Le cache est calculé pour :
1. une couche cartographique donnée
2. un ensemble de niveaux (0 à 11)
3. un périmètre rectangulaire (Bounding box)

## Prérequis
S’assurer que le ou les serveurs qui seront utilisés pour le calcul du cache, ont bien la même configuration que la PROD

## Démarche
1. Se connecter à un des GeoServer TEST ou FORM. Voir le keePass pour l’url et utilisateur / mot de passe
2. Aller dans le menu Tile Caching puis ouvrir Tile Layers. La liste des couches doit s’afficher
3. Ordonner les couches en cliquant sur Layer Name
4. Optionnel prévisualisation de la couche avant le calcul du cache. Ouvrir la selectbox et choisir EPSG:2056_eca_vaud / png. La couche doit apparaître dans une fenêtre séparée
    Pour info, se balader dans la fenêtre à différents zooms calcule du cache
5. Calcul du cache, cliquer sur Seed/Truncate de la couche voulue. Une fenêtre de GeoWebCache doit s’ouvrir
    Il faut aller dans Create a new task et remplir les options suivantes :
        a) Number of tasks to use: 1 à 16
				    Remarque : 16 est le nombre de threads maximal donc si on met 16 il n’y a plus de ressource pour les autres couches à calculer
        b) Type of operation: Seed
        c) Grid set : EPSG:2056_eca_vaud
        d) Format : image / png
        e) Zoom start : 00
        f)  Zoom stop: 09
   		      Attention plus le zoom est élevé plus on se rapproche du sol. A un zoom de 11 l’échelle est de 500 et il faut 5023 x 4800 = 24 110 400 images … donc beaucoup de temps
	      g) Bounding box:

| Min X | Min Y	| Max X |	Max Y |
| --------------- | --------------- | --------------- | --------------- |
| 2 494 000 | 1 115 000	| 2 586 000 |	1 200 000 |

h) Cliquer sur Submit pour lancer le calcul
				
et attendre car suivant les couches c’est plusieurs jours
