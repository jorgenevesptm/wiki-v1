<!-- TITLE: Extraction Archive Cache -->
<!-- SUBTITLE: A quick summary of Extraction Archive Cache -->

# Procédure pour extraire l’archive du cache des couches cartographiques
## Démarche
1. Se connecter à un des GeoServer TEST ou FORM. Voir le keePass pour l’url et utilisateur / mot de passe
2. Se connecter par ssh sur le serveur nfs où l’archive est présente. Voir la partie stockage sur les serveurs nfs http://wiki.telem.local/geo-server
3. Se mettre en root par la commande su -
4. Aller dans le répertoire /DATA/geoserver/geoserver_data/external_data/eca_vaud/blobstore/
5. cpio -idu  < YYYYMMDD-archive.cpio