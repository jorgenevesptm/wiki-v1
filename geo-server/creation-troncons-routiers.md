<!-- TITLE: Création Troncons Routiers -->
<!-- SUBTITLE: A quick summary of Création Troncons Routiers -->

# Header todo doc TR
drop index planet_osm_line_roads_idx;
drop table planet_osm_line_roads;
drop table planet_osm_troncons_routiers;
create table planet_osm_line_roads as
select *
from planet_osm_line
where
-- QGIS Filter : start
  highway is not null
  and highway != 'footway'
  and highway != 'cycleway'
  and highway != 'steps'
  and highway != 'pedestrian'
  and highway != 'bus_stop'
  and highway != 'via_ferrata'
  and highway != 'motorway'
  and highway != 'motorway_link'
  and highway != 'secondary_link'
  and highway != 'elevator'
  and highway != 'corridor'
  and ST_Length(way) >= 50
-- QGIS Filter : end
  and ST_Contains((select geom from zonecouverte), way);

create index planet_osm_line_roads_idx on planet_osm_line_roads using gist(way);

-- Stop sql script
-- Run qgis Points along geometry function with distance 200 m after create table by name "planet_osm_troncons_routiers" id, geom, ...

![Troncons Routiers](/uploads/geoserver/troncons-routiers.png "Troncons Routiers")

-- drop table planet_osm_troncons_routiers_bk;
create table planet_osm_troncons_routiers_bk as select * from planet_osm_troncons_routiers;
create index planet_osm_troncons_routiers_idx on planet_osm_troncons_routiers using gist(geom);

-- After the restore -> start from here don't delete the backup !!!
delete from planet_osm_troncons_routiers where id in (select id from (SELECT id, ROW_NUMBER() OVER (PARTITION BY geom ORDER BY id asc) AS Row, geom FROM ONLY planet_osm_troncons_routiers) dups where dups.Row > 1);

-- Restore from planet_osm_troncons_routiers_bk
-- drop table planet_osm_troncons_routiers;
create table planet_osm_troncons_routiers as select * from planet_osm_troncons_routiers_bk;
create index planet_osm_troncons_routiers_idx on planet_osm_troncons_routiers using gist(geom);

-- create table troncons_routiers_start
-- (
--     id bigint not null
--         constraint troncons_routiers_start_pkey
--             primary key,
--     id_start bigint not null,
--     commune varchar(100) not null,
--     npa integer,
--     localite varchar(30),
--     nom varchar(50),
--     genre varchar(50),
--     sous_genre varchar(50),
--     precision varchar(256),
--     plan_deploiement varchar(100),
--     secteur_sinistre varchar(100),
--     geom geometry(Point,2056) not null
-- );
--
--
-- create index troncons_routiers_start_idx on troncons_routiers_start using gist(geom);
--
-- create index points_supp_start_idx on points_supp_start using gist(geom);
--
-- alter table troncons_routiers_start owner to admin;

-- create table points_supp_start_bk as select * from points_supp_start;

select * from troncons_routiers_start;

select tr.id tr_id, ps.id, ps.id_start, ps.localite
from troncons_routiers_start tr
         left join points_supp_start ps on ST_DWithin(tr.geom, ps.geom, 99)
where ps.id is not null
order by tr.id;

select tr.id adr_id, ps.id, ps.id_start, ps.localite
from adresseregblbatrcb tr
         left join points_supp_start ps on ST_DWithin(tr.geom, ps.geom, 99)
where ps.id is not null
order by tr.id;

****A ne plus faire, les sql sont dans un ETL !

-- Remove points supp à moins de 100m des points tronçons routiers
with ps_id_delete as (select ps.id
                      from troncons_routiers_start tr
                               left join points_supp_start ps on ST_DWithin(tr.geom, ps.geom, 99)
                      where ps.id is not null)
delete
from points_supp_start
where id in (select ps_id_delete.id from ps_id_delete);

-- Remove points supp à moins de 100m des adresses regbl
with ps_id_delete as (select ps.id
                      from adresseregblbatrcb tr
                               left join points_supp_start ps on ST_DWithin(tr.geom, ps.geom, 99)
                      where ps.id is not null)
delete
from points_supp_start
where id in (select ps_id_delete.id from ps_id_delete);

-- Remove points supp à moins de 100m des adresses manuelles
with ps_id_delete as (select ps.id
                      from adresses_manuelles_start tr
                               left join points_supp_start ps on ST_DWithin(tr.geom, ps.geom, 99)
                      where ps.id is not null)
delete
from points_supp_start
where id in (select ps_id_delete.id from ps_id_delete);

-- Remove points supp à moins de 100m des adresses SAE1
with ps_id_delete as (select ps.id
                      from "Adresses_SAE1" tr
                               left join points_supp_start ps on ST_DWithin(tr.geom, ps.geom, 99)
                      where ps.id is not null)
delete
from points_supp_start
where id in (select ps_id_delete.id from ps_id_delete);




