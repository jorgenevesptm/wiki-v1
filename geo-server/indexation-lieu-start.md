<!-- TITLE: Indexation Lieu Start -->
<!-- SUBTITLE: A quick summary of Indexation Lieu Start -->

# Procédure pour indexer les lieux de Start après une importation avec l'ETL GeoData
## Démarche
1. Se connecter aux serveurs Start par l'url http://ip-serveur-start:8989/systel-rest/poluxeye/#/search
2. Lancer indexation sur tous les serveurs.

Pour la PROD, voir start et start-dmz

Pour TEST recherche dans IP Admin : [startt] -> 10.118.131.33 - 10.118.131.34 - [dmz*start*qa] -> 10.64.2.161/162/165 	srv-dmz-startmob-qa-0x