<!-- TITLE: Fortimanager -->
<!-- SUBTITLE: Utilisation du FortiManager pour gérer les FortiGate -->

# Partage Inter-VDOM
Pour la création d'objet inter-ADOM (partage d'objet entre les FortiGate 100D du SPSL et les 500D de la PROD) il faut déclarer les objets dans l'ADMIN "GLOBAL"

1. **Adom Global**
![Global](/uploads/global.png "Global")
2. **Create New**
	- Création de l'obet mentionné selon :
> Adresses & groupes d’adresses
>  
> Adresses    [TYPE]_[adresse_ip]_[hostname]    Ex : SRV_10.118.128.39_srv-ad-01
> - Types :
> 	o Serveurs :         SRV_
> 	o Poste client :         WS_ (workstation)
> 	o Firewall :         FW_
> 	o Automate :         PLC_
> 	o Téléphone :         TEL_
> 	o CONSOLE :        CONS_
> 	o Contrôle daccès :     AC_
> 	o Caméra :         CAM_
> 	o Interphone :          INT_
> 		 
> Virtual IP ext    NAT_[fqdn ou ip]            Ex : NAT_lego-test.118-vaud.ch
> Virtual IP int    NAT_[hostname]            Ex : NAT_ws-04
> FQDN        FQDN_[fqdn]                Ex : FQDN_wowzalicence1.wowzamedia.com
> Groupes    GRP_[service]                Ex : GRP_ad-telem, GRP_esx
> VLANs        [VLAN]_[ip/mask]            Ex : DMZ-EXTERNE_10.64.0.0/16
>  
> Services & groupes de services
>  
> Services     [PROTOCOLE]_[PORT]_[service]        Ex : TCP_80_http, UDP_53_dns
> Groupes    GRP_[service]                Ex : GRP_ad_services, GRP_sccm_server

3. Une fois l'objet créé, se rendre dans "**Policy Package**"
![Package](/uploads/package.png "Package")
4. Selectionner les 2 ADMIN 
> 	- ProSDIS_FortiGate500D et ProSDIS_FortiGate100D
5. Cliquer sur "**Assign Selected**"
![Assign](/uploads/assign.png "Assign")
6. Puis, "**Assign ALL Objects**"
7. Enfin, les objets seront disponibles dans les 2 ADOM 


# Pousser des règles dans le FortiManager
1. Selection de l'ADOM voulu
 - Soit le ProSDIS_FortiGate500D pour la PROD
 - Soit le ProSDIS_FortiGate100D pour le SPSL
 - Soit le GlobalDatabase pour réaliser un partage d'objet

2. Selectionner le cluser désiré (Patenaire ou Externe)
3. Vérifier si une règle n'existe pas déjà
4. Si tel n'est pas le cas, créer une règle en mentionnant
- Titre : DE -> A [ENV] (prod - qa - test)
5. Réaliser ensuite la règle
6. Pousser la règle, pour se faire 
- Aller dans "**Device Manager**"
- Clique droite sur le, ou les, cluster concernés
- **Re-Install Policy**
- **Ok**
- Contrôler que le contrôle de parité soit OK, si OK alors pousser les règles ! 
- Si **KO** avertir le responsable de l'infrastructure @SBO

# Architecture FortiGate
![Archi Fg](/uploads/archi-fg.png "Archi Fg")

 