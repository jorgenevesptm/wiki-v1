<!-- TITLE: Conditions Repli -->

<!-- SUBTITLE: Procédure regroupant les conditions impliquant un repli vers le SPSL -->

<img src="http://srv-affi-01.telem.local/wiki-images/cri/media/eca-logo-mini.svg" alt="ECA_Logo" >

# Conditions de Repli SPSL

## Table des matières

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Conditions de Repli SPSL](#conditions-de-repli-spsl)
	- [Table des matières](#table-des-matires)
	- [1. Locaux non utilisable](#1-locaux-non-utilisable)
		- [Symptômes](#symptmes)
		- [Actions à faire](#actions-faire)
	- [2. Coupure électrique](#2-coupure-lectrique)
		- [Symptômes](#symptmes)
		- [Actions à faire](#actions-faire)
	- [3. Coupure internet](#3-coupure-internet)
		- [Symptômes](#symptmes)
		- [Actions à faire](#actions-faire)
	- [4. Coupure téléphonique](#4-coupure-tlphonique)
		- [Symptômes](#symptmes)
		- [Actions à faire](#actions-faire)
	- [5. Matériels métier disfonctionels](#5-matriels-mtier-disfonctionels)
		- [Symptômes](#symptmes)
		- [Actions à faire](#actions-faire)
	- [Liste des abréviations](#liste-des-abrviations)

<!-- /TOC -->

<div style="page-break-after: always;"></div>

<img src="http://srv-affi-01.telem.local/wiki-images/cri/media/eca-logo-mini.svg" alt="ECA_Logo" >

## 1. Locaux non utilisables

### Symptômes

-   Impossibilité physique d'utiliser les locaux du CTA
-   Mise en danger de la santé du personnel

### Actions à faire

1.  Exécution du mode Repli
2.  Alarmer piquet STEL
3.  Informer le piquet STEL lorsque l'opérateur est à nouveau opérationnel au SPSL

<div style="page-break-after: always;"></div>

<img src="http://srv-affi-01.telem.local/wiki-images/cri/media/eca-logo-mini.svg" alt="ECA_Logo" >

## 2. Coupure électrique

### Symptômes

-   Pas de lumière dans la salle
-   Ordinateur et écrans des PO Start ne s'allument pas
-   Le mur d'image est éteint et ne s'allume pas
-   Pas de présence électrique dans l'étage

### Actions à faire

1.  Exécution du mode Repli
2.  Alarmer piquet STEL
3.  Informer le piquet STEL lorsque l'opérateur est à nouveau opérationnel au SPSL

<div style="page-break-after: always;"></div>

<img src="http://srv-affi-01.telem.local/wiki-images/cri/media/eca-logo-mini.svg" alt="ECA_Logo" >

## 3. Coupure internet

### Symptômes

-   Service météo non fonctionel sur le mur d'images
-   Service traffic routier non fonctionnel sur le mur d'images
-   Les appels entrant n'arrivent pas en centrale
-   Alarmes Critique CellNet : Internet Status

### Actions à faire

1.  Alarmer piquet STEL
2.  STEL informe CTA si le mode repli doit être exécuté
3.  Informer le piquet STEL lorsque l'opérateur est à nouveau opérationnel au SPSL

<div style="page-break-after: always;"></div>

<img src="http://srv-affi-01.telem.local/wiki-images/cri/media/eca-logo-mini.svg" alt="ECA_Logo" >

## 4. Coupure téléphonique

### Symptômes

-   Les appels entrant n'arrivent pas en centrale
-   Les appels sortant ne s'effectuent pas

### Actions à faire

1.  Test de mobilisation
2.  Test d'un appel 118 extérieur
3.  Alarmer piquet STEL
4.  Informer STEL des résultats des tests précédent
5.  Exécution du mode Repli
6.  Informer le piquet STEL lorsque l'opérateur est à nouveau opérationnel au SPSL

<div style="page-break-after: always;"></div>

<img src="http://srv-affi-01.telem.local/wiki-images/cri/media/eca-logo-mini.svg" alt="ECA_Logo" >

## 5. Matériels métier disfonctionels

### Symptômes

-   TOUS les PO Start et TOUS les portables de secours ne sont plus fonctionnelles

### Actions à faire

1.  Alarmer piquet STEL
2.  STEL informe CTA si le mode repli doit être exécuté
3.  Informer le piquet STEL lorsque l'opérateur est à nouveau opérationnel au SPSL

<div style="page-break-after: always;"></div>

<img src="http://srv-affi-01.telem.local/wiki-images/cri/media/eca-logo-mini.svg" alt="ECA_Logo" >

## Liste des abréviations

| Abréviation | Définition                                     |
| ----------- | ---------------------------------------------- |
| CTA         | Centre de Traitement des Alarmes               |
| SPSL        | Service de Protection et Sauvetage de Lausanne |
| STEL        | Service Télématique de l'ECA                   |
