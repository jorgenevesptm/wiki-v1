<!-- TITLE: Références Téléphonique pour le CTA -->
<!-- SUBTITLE: Références de la documentation concernant la téléphonie du CTA -->

## Téléphonie CTA
# Primaire Swisscom
|  |  |
| --- | --- |
| Numéro du primaire administratif | 021 213 20 00 |
| Numéro du primaire 118 | 021 343 21 97 (Lausanne) 021 343 21 98 (Vaud) | 
| Numéro de Backup (au SPSL) | 021 213 28 00 |

# Numéros directs des consoles
|  |  |
| --- | --- |
| Consoles 1-7: | 021 213 20 31 ==> 021 213 20 37 |
| Consoles SPSL: | 021 213 20 41 et 021 213 20 42 | 
| Console Telem | 021 213 20 44 |

# Numéros UPC - DLWL
021 545 53 97
021 545 53 98

Ces deux numéros sont les numéros 118 de secours en cas de panne de Swisscom. Ces deux numéros remontent sur le Cube en tant que 118.

Ce deux numéros peuvent être utilisés pour tester l'infrastructure et la remontée complète du 118 sur la PROD lorsque la bascule Swisscom a été réalisée sur le SPSL

# Références dans PROSDIS

> \\PROSDIS\Documentation\Telephonie\

> \\docsites\CTA - CTA Pully\Docs\5_Acces téléphonie_internet_FO\118-Acheminement-Swisscom\Situation (2012-) ACTUELLE\

# Numéro de téléphone pour les SMS sur START
+41798070639


# Fonctionnement téléphonie (StartPhone + Asterisk) 
Le système de queue fonctionne comme suit : 

**Pilote**

On définit des pilotes
* Celui-ci sert à savoir si l'appel est traité par Asterisk -> (pas de pilote -> busy)
* A définir la musique en attente (feu / non-feu)
* A définir un comportement dans start (opérateurs à appeler, priorité, ...)

**Distribution**

Chaque pilote a 3 queues associé (avec un suffix)

[pilote]1 : Queue de réception, permet de mettre à disposition de start l'appel, si l'appel n'est pas pris par start -> go to queue 9
[pilote]2 : Queue d'attente, permet de mettre un appel en attente pendant 30 secondes. START ré-insère l'appel toute les 30 secondes pour s'assurer que l'appel ne soit pas perdu si START plante
[pilote]9 : Ring-all : fait sonner toutes les consoles (+ natel si DSI), est appelé lorsque START ne prend pas les appels en queue 1 ou timeout de queue 2 + mode dégradé

Ci-dessous une image de mauvaise qualité mais présente dans le manuel admin TRIP

![Fonctionnementtelephonie](/uploads/images/fonctionnementtelephonie.png "Fonctionnementtelephonie")

# Fonctionnement bascule téléphonique au SPSL / DSI

La DSI est un cas particulier, et on utilsait un unique pilote 800, mais j'ai l'impression que celui-ci a été supprimé au profit d'un pilote 118.

***Note:*** J'ai été contrôler dans mes dialplans et le 800 est effectivement définit dans ceux-ci. Donc c'est bien le 800 la configuration "historique"

**But**
La DSI est la en tant que repli de la PROD. Ceci via deux objectifs : 
1.	Gérer la transition des opérateurs de Pully à la DSI 
a.	Pour cela les appels en mode dégradé sont en "ring-all" y compris sur des téléphones mobiles. Cela permet aux opérateurs de décrocher des appels pendant leur déplacement
2.	Recréer un environnement presque complet en cas de défaillance du site de Pully
a.	La radio est physiquement connectée à Pully
b.	Les ALR sont aussi physiquement acheminés à Pully
c.	Le FAX et les mobilisations indirectes ne sont pas déviés par Swisscom

**Mode dégradé**
Le mode dégradé est un mode ou START est coupé au maximum du "backbone TRIP", cela veut dire que les actions suivantes ne sont plus disponibles sur START
1.	Réception des appels
2.	Login/logout des consoles
3.	Contrôle du totem
4.	Radio
5.	"Appel sortant" (avec réserve, voir ci-dessous)
Note : Le design logiciel de START ne nous permet pas de couper complètement son accès à l'asterisk. En effet, celui-ci dispose d'un accès direct via son API. Ce qui signifie : 
1.	START peut intercepter des appels et les rediriger 
2.	En théorie START peut faire des appels sortants

**Scénario de la DSI**
La DSI est par défaut en mode dégradé, lorsque l'état n'est plus en mode dégradé, une alarme de supervision est déclenchée, ceci afin d'éviter que le système ne redirige pas sur les natels en cas d'urgence.

Lors d'un cas : 
1.	Le scénario de déviation de Swisscom est déclenché
2.	Les opérateurs se déplacent à la DSI
3.	Les opérateurs se loguent sur les postes START
4.	Un seul opérateur exécute la séquence d'activation du mode normal (appel 1000 -> DTMF -> puis code ***** -> puis #
5.	Le "plateau" est maintenant fonctionnel et les appels sont redirigés selon START
Mobilisation

La mobilisation n'est pas impactée normalement par le mode dégradé, mais le mode dégradé supprime certains droits à START, ce qui peut créer des effets de bords sur START. Je ne peux pas statuer sur l'utilisation du mode dégradé et des mobilisations sur START, mais du côté TRIP cela ne semble pas poser de problème.

