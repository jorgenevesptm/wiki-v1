<!-- TITLE: Docker Container -->
<!-- SUBTITLE: A quick summary of Docker Container -->
Concerne les serveurs : srv-crss-web-01 / srv-crss-01 - RIN - CRSS

# Howto about docker container 

* <a href="http://wiki.telem.local/docker-container/how-to-save-load-docker-images">How to save and load docker images</a>
* <a href="http://wiki.telem.local/docker-container/how-to-deploy-application">How to deploy docker application</a>
* <a href="http://wiki.telem.local/docker-container/how-to-log">How to log docker</a>
* [Docker Command Cheat Sheet](/uploads/docker-command-cheat-sheet.pdf "Docker Command Cheat Sheet")
# Docker container tips

### Start docker compose with many docker-compose files

- execute `docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d`

### To show last 100 lines for each container and follow logs output

- execute `docker-compose logs --tail=100 -f` or `docker logs --tail=100 -f container-name` 

### Stop docker compose

- execute `docker-compose stop`

### Down docker compose -> stop, remove containers and networks. Volumes are still present

- execute `docker-compose down`

### Execute command into a running container

- execute `docker exec -i container-name bash -c "script or command"`

- exemple : docker exec -i container-name bash -c "./appserver/glassfish/bin/asadmin -u admin -W /opt/payara/passwordFile -s true deploy --force --enabled=true deployments/webApp.war"

### Get bash into a running container

- execute `docker exec -it container-name bash`

### Docker volumes information

Docker volumes are in path /var/lib/docker/volumes/ without specific mount point

Use `docker volume COMMAND` with command [ ls, inspect, ... ]

### Delete containers, networks, volumes ... db will be empty ... be careful with data

- `docker-compose down -v`

### Purge old containers and images -> safe cmd

- `docker rmi $(docker images -f dangling=true -q)`

### Purge unused volumes -> safe cmd

- `docker volume rm $(docker volume ls -f dangling=true -q)`

