<!-- TITLE: Redemarrage Vlan 29 -->
<!-- SUBTITLE: Procédure sur le redémarrage du VLAN29 -->

# Symptômes 
 Les interactions avec le réseau  dit « radio » ne fonctionnent plus (caméras, commandes des portes depuis Cellnet).
 
# Causes
Le core routeur du réseau radio pêche à router les paquets correctement entre le réseau SAE2 et le réseau radio. 

# Procédure
 
Se connecter sur chacun des routeurs puis les redémarrer (un à un). Le plus souvent, les caméras de la cafeteria ainsi que celle de l’entrée du 54 ne reviennent pas à l’image. Pour ce dernier point, il est nécessaire de redémarrer ces équipements. Pour ces caméras en question, il est possible de se connecter sur le switch où elles sont raccordées et de leur couper l’alimentation PoE. Pour finir, vérifier sur l’interface de l’AGC Wall que toutes les caméras sont à nouveau affichées puis demander une confirmation à l’opérateur et lui faire valider l’ouverture des portes. 
 
* cc-router-01.eca-supervision.local          172.27.255.5
* cc-router-02.eca-supervision.local          172.27.255.6
* switch-cameras-54                                  172.27.29.10      (attention, pas de routage, y accéder depuis un équipement sur le même VLAN, tel qu’un des core router ou établir la connection ssh depuis un serveur sur le même réseau). 

## Rebooter un core routeur :
Si le problème ne concerne que les portes, la partie caméra peut être mise de côté.

* user@machine:~$ ssh admin@172.27.255.**5**
* Entrer le mot de passe situé dans le Keepass Télématique (nom "core router")
* Sélectionner l’option 8) Shell
* Entrer la commande « reboot »
 
Exécuter un ping et attendre que le core routeur  revienne disponible (valider par une connexion SSH). 

Une fois le router en ligne, exécuter la même commande pour le second router 172.27.255.**6**

## Caméra
Pour redémarrer les caméras de la cafeteria ainsi que celle de l’entrée du bâtiment 54, il est nécessaire de se connecter sur le switch sur lequel elles sont raccordées puis de couper l’alim et de la rétablir :
 
* user@machine:~$ ssh admin@172.27.255.5
* Entrer le mot de passe situé dans le Keepass Télématique (ECA -> Sites -> Pully -> Serveurs & Switchs -> <u>Core Router</u>)
* Sélection l’option 8) shell
* [2.3.1-RELEASE][admin@cc-router-02.eca-supervision.local]/root: ssh manager@172.27.29.10
* Renseigner le mot de passe (par défaut de Allied Telesis) dans le Keepass Télématique (ECA ->  Sites -> Pully -> Serveurs & Switchs -> <u>MdP défaut Allied Telesis</u>)
* Afficher la configuration
	* B54-SW-04> en
	* B54-SW-04> sho ru
* Faire défiler la configuration à l’aide de la barre espace et repérer les interfaces des caméras qui vont devoir être redémarrées. 
	* interface port1.0.3            Caméra entrée 54
	* interface port1.0.4            Caméra cafeteria 2
	* interface port1.0.5            Caméra cafeteria 1
* Eteindre puis remettre le PoE sur les ports concernés :
	* B54-SW-04> conf t
	* B54-SW-04(config)# interface port1.0.3-port1.0.5
	* B54-SW-04(config-if)# no power-inline enable
	* B54-SW-04(config-if)# power-inline enable
	* B54-SW-04(config-if)# exit
	* B54-SW-04(config)# exit
	* B54-SW-04> exit
* Vérifier sur l’AGC Wall que les caméras sont à nouveau affichées
	* http://10.96.128.141/AGCWeb/
	* S’identifier à l’aide du Keepass ProSDIS
	* Sélectionner le mur sur lequel sont affichées les caméras
 
Les affichages du bureau télématique indiqueront les sites radios comme déconnectés, il est nécessaire de rafraîchir ces pages pour avoir un affichage et des informations à jour (p.ex. en redémarrant le raspberry [pi@172.27.100.21]). 
## AGC Wall 
Voici la vue du mur gauche dans son état normal 
![Agc Normal](/uploads/agc-normal.png "Agc Normal")
