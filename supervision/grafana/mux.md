TITLE: Monitoring trafic Mux -->
SUBTITLE: Ajout de graph de monitoring pour le trafic MUX -->
# Grafana MUX
## Visualisation
Les connexions sur les MUX peuvent être visualisées aux adresses suivantes:
>* **dc1-mux-dwdm-01**
>http://10.96.31.90:3000/d/3gOOanfGk/traffic-dc1-mux-dwdm-01
>* **dc2-mux-dwdm-01**
>http://10.96.31.90:3000/d/ggrinpBGk/traffic-dc2-mux-dwdm-01
>* **b56-mux-dwdm-01**
>http://10.96.31.90:3000/d/EcQ3vtfMz/traffic-b56-mux-dwdm-01
## Backend
Le backend de cette supervision est fourni via deux containers docker hostés sur srv-nagios-01 (10.96.31.100)
Lss fichier de configuration se trouve sur ~/mux-perf/
Reboot du container nécessaire après toute modification de son fichier de configuration respectif.
Les dits fichiers peuvent être consultés sur leur repo gitlab https://gitlab-stel.118-vaud.ch/radio-trans/mux-perf
MIB utilisé : LUM-PM-MIB de Infinera fourni par Trigon
* prom-mux
* Container Prometheus basé sur l'image prom/prometheus
* Fichier de configuration prometheus.yml
* URL http://10.96.31.100:9090
* Scrap des données toutes les 15 minutes, cela est dû au fait que le Mux rafraichit ses valeurs de trafic toutes les 15 minutes
* Timeout pour le scrap de 5 minutes

* snmp-exporter-mux
* Container snmp-exporter basé sur l'image prom/snmp-exporter
* Exporter Prometheus, génère des requêtes SNMP et présente les metrics réceptionnées dans une page web compatible Prometheus.
* URL http://10.96.31.100:9116 IChamp IP, l'ip du mux que vous souhaité et dans le champ module, utilisé "mux-mib" sans apostrophe
* Fichier de configuration snmp.yml **Attention** ce fichier doit être généré avec l'outil generator. Cette outil parse les mibs fournies et génère sa configuration pour les mibs souhaitées. Fichier generator.yml disponible sur le repo gitlab
* Pour une nouvelle configuration du generator, merci de suivre les instructions sur la page Github du logiciel https://github.com/prometheus/snmp_exporter/tree/master/generator
Ces deux containers tournent avec le paramètre -d et --restart unless-stopped
## Configuration d'un nouveau Graph
* Générer la list des identifiants des différentes interfaces du MUX en faisant un WALK de l'OID suivante **.1.3.6.1.4.1.8708.2.15.2.10.1.1.2**
Exemple de résultat:
```
OID: .1.3.6.1.4.1.8708.2.15.2.10.1.1.2.13000
Value: port:1:13:3:1-2
Type: OctetString
OID: .1.3.6.1.4.1.8708.2.15.2.10.1.1.2.13002
Value: port:1:13:4:1-2
Type: OctetString
OID: .1.3.6.1.4.1.8708.2.15.2.10.1.1.2.13004
Value: port:1:13:2:1-2
Type: OctetString
OID: .1.3.6.1.4.1.8708.2.15.2.10.1.1.2.13005
Value: port:1:13:2:3-4
Type: OctetString 
```

Les derniers digit de l'OID fourni l'identifiant de l'interface.
Dans le cas du port **1:13:2:1-2** sont identifiant est **13004**
Cette liste est aussi disponible sur le repo Gitlab. **Merci de rafraichir cette liste si vous faites un nouveau walk !!** Nom de la liste : interface_*nom-du-mux*
* Connectez-vous sur Grafana http://10.96.31.90:3000 (credentials disponible dans Keepass ProSDIS)
* Créé un nouveau dashboard et inséré un nouveau graph
* Formule pour le calcul du trafic en MB/s pour le Tx
* >((pmEthTdCurrentTxOctets{instance="**IP du MUX**",pmEthTdIndex="**Identifiant de l'interface**"})/(15*60))/(2^20)
* Formule pour le calcul du trafic en MB/s pour le Rx
* >((pmEthTdCurrentRxOctets{instance="**IP du MUX**",pmEthTdIndex="**Identifiant de l'interface**"})/(15*60))/(2^20)

La partie "(pmEthTdCurrentRxOctets{instance="**IP du MUX**",pmEthTdIndex="**Identifiant de l'interface**"}" renvoie le trafic sur l'interface Rx en Bytes par tranche de 15 minutes
On divise cette valeur pour obtenir le nombre de Bytes par seconde.
On divise aussi par 2^20 pour la conversion en Mega Bytes.