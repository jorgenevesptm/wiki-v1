<!-- TITLE: Syslog -->
<!-- SUBTITLE: Description de la configuration du serveur Syslog -->

# Configuration du serveur Syslog
## Consultation des Logs
Pour consulter les logs, vous devez être Root

```text
sudo -i
```


Les logs sont stockés sous


```text
cd /var/log/remote/
```


Il y a ensuite un dossier par type de Log

* Ecapage
* Mobilisation
* Radio
* Telephonie
* Systemd_Asterisk
* Net
* Net_err
* Cos
* Switch

Dans chaque dossier, il y a des sous dossiers pour le tri par Année/Mois/Jour/

par exemple:


```text
cd /var/log/remote/telephonie/2018/09/18/telephonie.log
```


## Scripts de compression et suppression des anciens Logs
Les logs sont actuellement retenus une année.

Les scripts se trouve sous 


```text
cd /home/administrator/scriptsyslog/
```


Le script compresslog.sh est lancé tous les jours à 00h30 par crontab est compresse les logs d'avant hier

```batchfile
#!/bin/bash
annee=$(date +%Y)
mois=$(date +%m)
jour=$(date -d "-2 days" +%d)
#Zip les logs d'avant hier
gzip "/var/log/remote/net/$annee/$mois/$jour/net.log"
gzip "/var/log/remote/net_err/$annee/$mois/$jour/net_err.log"
gzip "/var/log/remote/telephonie/$annee/$mois/$jour/telephonie.log"
gzip "/var/log/remote/radio/$annee/$mois/$jour/radio.log"
gzip "/var/log/remote/ecapage/$annee/$mois/$jour/ecapage.log"
gzip "/var/log/remote/cos/$annee/$mois/$jour/cos.log"
gzip "/var/log/remote/mobilisation/$annee/$mois/$jour/mobilisation.log"
gzip "/var/log/remote/systemd_asterisk/$annee/$mois/$jour/systemd_asterisk.log"
gzip "/var/log/remote/switch/$annee/$mois/$jour/switch.log"
```

Le script logsup.sh est lancé tous les 30 du mois ainsi que le 28 Février est supprime tous les fichiers plus vieux d'un an.

```batchfile
#!/bin/bash
annee=$(date +%Y)
mois=$(date +%m)
jour=$(date +$d)
#obtenir 1 annee precedente a celle d aujourd hui
let "annee1 = $annee - 1"
#supprimer le dossier des logs 1 an avant ce jour
rm -r "/var/log/remote/radio/$annee1/$mois/"
rm -r "/var/log/remote/net/$annee1/$mois/"
rm -r "/var/log/remote/systemd_asterisk/$annee1/$mois/"
rm -r "/var/log/remote/telephonie/$annee1/$mois/"
rm -r "/var/log/remote/ecapage/$annee1/$mois/"
rm -r "/var/log/remote/cos/$annee1/$mois/"
rm -r "/var/log/remote/mobilisation/$annee1/$mois/"
rm -r "/var/log/remote/net_err/$annee1/$mois/"
rm -r "/var/log/remote/switch/$annee1/$mois/"
```

Le script logsupAnnee.sh est lancé chaque 1er Janvier de chaque année et permet la suppression des dossiers "Années" plus vieux d'une année


```batchfile
#!/bin/bash
annee=$(date +%Y)
mois=$(date +%m)
jour=$(date +$d)
#obtenir l annee deux an auparavant
let "annee2 = $annee - 2"
#SUpprimer les dossiers d'il y a 2 ans
rm -r "/var/log/remote/radio/$annee2/"
rm -r "/var/log/remote/net/$annee2/"
rm -r "/var/log/remote/systemd_asterisk/$annee2/"
rm -r "/var/log/remote/telephonie/$annee2/"
rm -r "/var/log/remote/ecapage/$annee2/"
rm -r "/var/log/remote/cos/$annee2/"
rm -r "/var/log/remote/mobilisation/$annee2/"
rm -r "/var/log/remote/net_err/$annee2/"
rm -r "/var/log/remote/switch/$annee2/"
```

## Crontab -e
Voici le contenu du Crontab actuel.

```text
#le 1 janvier de chaque année
0 0 1 1 * bash /home/administrator/scriptsyslog/logsupAnnee.sh
#Tous les 30 du mois
0 0 30 * * bash /home/administrator/scriptsyslog/logsup.sh
#Le 28 fevrier chaque année
0 0 28 2 * bash /home/administrator/scriptsyslog/logsup.sh
#compresse les fichiers de logs du jour précédent du mois précédent tous les jours à 00h30
30 00 * * * bash /home/administrator/scriptsyslog/compresslog.sh
```

## Ajout d'un nouveau log
La configuration des filtres se fait ici:

```text
sudo vi /etc/syslog-ng/syslog-ng.conf
```

Aidez vous du lien suivant pour l'ajout de nouveau Log.

https://phelepjeremy.wordpress.com/2017/06/20/configuration-dun-serveur-syslog-ng/#confclt

Sous "Remote coming from remote", ajouter une ligne pour créer le fichier ainsi que les dossiers pour votre nouveau log.


```text
destination d_net_telephonie {file("/var/log/remote/telephonie/$YEAR/$MONTH/$DAY/telephonie.log"); };
```

Sous la partie "Filters", définissez le filtre que vous souhaitez.

```text
filter f_telephonie { host("*dc2-ip-pbx*" type(glob)) or host("*dc1-ip-pbx*" type(glob))and program(asterisk);};
```

Et finalement, définissez le lien entre la source des logs et le fichier de log sous "Log Paths"

```text
log { source(s_net);  filter(f_telephonie); destination(d_net_telephonie); };
```

Une fois le fichier de config sauvegardé, relancer le service syslog-ng.

```text
sudo systemctl restart syslog-ng.service
```










