<!-- TITLE:  Supervision trafic LEGO sur WAF -->
<!-- SUBTITLE: Supervision du trafic LEGO qui passe au travers de nos WAF -->

# Supervision trafic LEGO sur WAF

## Périmètre

- Surveille le nombre de session active qui correspond à la policy srv-multi-01-lego-certif et génère une alarme critique si le nombre de session est inférieur à 1 pour l'un ou l'autre WAF.

## Concerne

- srv-waf-01    : 10.118.110.10:90
- srv-waf-02    : 10.118.110.11:90
- srv-nagios-01 : 10.96.31.100

## Scripts

Deux scripts python3 ont été généré pour la mise en place de la surveillance.
Les deux sont disponible dans le projet "scripts nagios" sur le gitlab télématique.

- check_waf-01_lego.py : surveillance du srv-waf-01
- check_waf-02_lego.py : surveillance du srv-waf-02

## Prérequis

Un utilisateur local avec permission unique R/W sur la partie Système du WAF a été créé, cet utilisateur a été directement répliqué sur le second WAF.
User :  ApiFortiTelem
Mot de passe dans le keepass ProSDIS

Pour utilisé l'API de nos WAF, nous avons dut établir une règle ouvrant le port 90 entre le serveur nagios et les deux waf.

Au niveau de la requête, il est nécessaire de transmettre les credentials utilisé encodé de la forme suivante username:password en Base64

Détail se trouve dans la documentation ici https://help.fortinet.com/fweb/622/api_html/

## Comportement du script

1. Génération de la requête https://IP-du-WAF:90/api/v1.0/System/Status/PolicyStatusDetail/srv-multi-01-lego-certif
   - Nous désactivons la véréfication SSL via l'argument "verify=False" pour éviter les problème des certificats auto-signés
   - La fonction "requests.packages.urllib3.disable_warnings()" désactive le prompt de warning du aux certificats auto-signés
2. Lors de la réception de la réponse:
   1. Test du code HTTP si différent de 200 envoie une alarme critique avec le message "Failed to get data: suivi du code d'erreur HTTP"
   2. Test de la valeur de "sessionCount" correspondant aux nombre de connexion en court pour la policy demandée.
      - Si la valeur est inférieur à 1, envoie une alarme critique avec le message "Traffic LEGO Bloqué srv-waf-01" ou srv-waf-02 si c'est pour le second waf.
      - Sinon envoie OK avec le message "Traffic LEGO normal srv-waf-01" ou srv-waf-02 si c'est pour le second waf.
