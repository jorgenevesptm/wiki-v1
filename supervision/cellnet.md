<!-- TITLE: Cellnet -->
<!-- SUBTITLE: Mode d'emploi Cellnet Hyperviseur -->

# Connexion sur Cellnet Hyperviseur
Pour vous connecter au cellnet hyperviseur qui se trouve à l'adresse 10.96.31.51:8080. 
Utiliser votre compte RADIUS de l'AD télématique sous la forme suivante username:  jba et password: xxxxxxx 
Il ne faut pas mettre de @telem.local ou autre.

## Configuration Radius sur Cellnet
Dans Administration - System Properties, il y a deux champs utilisé pour la connexion entre le serveur Radius et Cellnet.
- Radius server IP : qui défini l'adresse IP du serveur Radius
- Radius Shared Key : qui est la clé d'échange du protocole Radius

## Ajout d'un utilisateur Radius
- Créer simplement un utilisateur dans User Management en utilisant le même login que dans l'AD par exemple "jba".
- Créer un Contact pour cette utilisateur pour lui associé un numéro de téléphone (pour qu'il reçoit les SMS de son groupe) et une adresse mail (pour qu'il reçoit les Emails de son groupe)
- Ajouter l'utilisateur dans un Group en fonction des droits que vous voulez lui accorder.
Les utilisateurs de la Télématique sont tous dans les groupes Administrateurs et Telematique.

# Réception des alarmes
Cellnet Hyperviseur reçoit des alarmes du Nagios télématique et du Nagios Systel via des traps SNMP.
Les alarmes sont filtrés via deux Cases, un Case nommé Alarmes Nagios Telem et un autre Case nommé Alarmes Systel.
Vous trouverez l'ensemble des log de traps reçus dans Log Entries List. (Pratique pour le débug)

## Format d'un event
Un event correspond à une alarme ou à un quittancement d'une alarme reçu de la part d'un des superviseurs.
Dans la liste Events, on retrouve l'ensemble des Events en cours. Pour accéder à l'historique, aller sur Events - Event History List.

Description des champs:
- Ack = l'aquittement ou non d'une alarme, un petit "vu" vert apparait lorsqu'une alarme est aquitté par un opérateur
- Description = output de l'alarme défini par le supervision qui la génère suivi du nom d'hôte
- Info = nom du service en question
- Count = le nombre d'occurence reçu
- Fisrt Occurence = date de la première occurence
- Last Occurence = date de la dernière occurence
- Device = Nom du superviseur qui a généré l'alarme
## Traps SNMP du Nagios Télématique
Une trap SNMP est composé de plusieurs OID permettant d'y associer plusieurs variables
Voici les OID SNMP utilisés pour les Traps de la télématique :
- 1.3.6.1.2.1.1.3.0 = Uptime = data1
- 1.3.6.1.4.1.10388.1.1.9.0 = snmpTrapOID = data2
- 1.3.6.1.4.1.10388.1.1.2.0 = Hostname = data3
Cette information correspond au nom de l'hôte en question dans Nagios
- 1.3.6.1.4.1.10388.1.1.1.0 = HostGroup de Nagios = data4
Cette information correspond au groupe d'hôtes auquel fait partie l'hôte en question, il est utilisé pour la définition des groupes d'alarmes (ex: Infrastructure, Paging, SPSL,...) 
- 1.3.6.1.4.1.10388.1.1.3.0 = Sévérité = data5
Cette information défini l'état de l'alarme comme ceci
	- 0=OK
	- 1=Critical pour un Host
	- 2=Critical pour un Service
	
- 1.3.6.1.4.1.10388.1.1.4.0 = Output = data6
Cette information correspond au texte de sortie de l'alarme
- 1.3.6.1.4.1.10388.1.1.5.0 = ID de l'hôte = data7
Cette information est l'ID unique défini par Nagios et qui est utilisé dans le script d'envoi des traps
- 1.3.6.1.4.1.10388.1.1.6.0 = nom du service = data8 
Cette information correspond au nom du service en question dans Nagios
### Filtrage par Regex
Dans le Case Alarmes Nagios Telem, le filtrage des alarmes et la catégorisation de celle-ci se font via des REGEX.
Il faut savoir que dans Nagios les output de services commencent toujours par S: et les output d'hôtes par H: ce qui permet d'identifier facilement une alarme hôte d'une alarme service.

A l'intérieur du Case, dans Nagios Telem sous Activation Actions, nous pouvons lister tous les Regex configurés.

Exemple Regex pour alarme Critique d'un Service PDU : 
- data4 = PDU
- data5 = 2
- data6 = S:

Exemple Regex pour alarme Critique d'un hôte Switch :
- data4 = Switch
- data5 = 1
- data6 = H:
## Traps SNMP du Nagios Systel
Systel utilise la MIB Nagios pour l'envoi des traps.

Nous ne prenons en compte que les alarmes critiques et leurs quittancements.

Voici les OID SNMP utilisés pour les Traps de Systel:
- 1.3.6.1.4.1.20006.1.3.1.2 = Hostname = data3
- 1.3.6.1.4.1.20006.1.3.1.10 = HostGroup = data4
Nous filtrons sur le HostGroup CORES_SERVICES si nous recevons une trap d'un autre hostgroup de la part de Systel, elle ne sera pas traitée.
- 1.3.6.1.4.20006.1.3.1.6 = ServiceName = data5
- 1.3.6.1.4.1.20006.1.3.1 = Severity = data6
- 1.3.6.1.4.1.20006.1.3.1.17 = Output = data7

L'ordre des données dans la Trap Systel n'est pas le même que dans la Trap du Nagios Telem, il faut faire attention lorsqu'on change les regex ou qu'on débug pour regarder au bon endroit.

# Backup DB Cellnet

Se connecter à cellnet  en SSH

Identifié la base de données

$ mysql -u UTILISATEUR_SQL -p
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| cellnet_hyp        |
+--------------------+
2 rows in set (0.00 sec)

mysql> 

taper exit et entrer pour sortir

exécuter la commande:
mysqldump -u UTILISATEUR_SQL -p NOM_DATABASE > DATE-cellnet.dump

uitiliser cellnet comme utilisateur
Attention pour l'hyperviseur, utiliser cellnet_hyp

mot de passe du user cellnet dans le keepass ProSDIS


