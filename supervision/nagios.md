<!-- TITLE: Nagios -->
<!-- SUBTITLE: Mode d'emploi Nagios -->

# Connexion à Nagios
Utilisez votre compte LDAP défini dans l'AD Telem pour vous connectez.

Pas de domaine à ajouter (@telem ou autre).

## Paramètre LDAP
L'intégration LDAP est configuré dans Admin - Users - LDAP/AD Integration.

Voici la configuration LDAP actuelle :

- Connection Method : Active Directory
- Base DN : DC=telem,DC=local
- Account Suffix : @telem.local
- Domain Controlers : 10.96.128.39,10.96.128.19
- Security : None

## Ajout d'utilisateur
Pour ajouter un utilisateur LDAP, aller dans Admin - Users - Manage Users

- Selectionner Add Users From LDAP/AD
- Utilisez votre compte personnel ou le compte administrateur de l'AD pour importer les utilisateurs
- Choisissez la OU contenant l'utilisateur désiré
- Cocher les utilisateurs à ajouter
- Cliquer sur Add Selected Users
- Retourner sur Manage Users
- Ajuster le champ Auth Level sur les nouveaux utilisateurs pour leur donner les droits désirés

# Consultation des alarmes
Il existe une multitude de façon de filtrer les alarmes à afficher dans Nagios.
## Description des différents états
Il existe 5 sévérités possible pour une alarme de service et 4 sévérités possible pour une alarme d'hôte

### Sévérités de service
- Critical = état critique qui est représenté par la couleur Rouge
- Warning = état mineur qui est représenté par la couleur Jaune
- Unknown = état inconnu qui est représenté par la couleur Orange
- Ok = état OK qui est représenté par la couleur Verte
- Pending = état en attente qui est représenté par la couleur Grise

### Sévérités d'hôte
- Critical =état critique qui est représenté par la couleur Rouge
- Warning =état mineur qui est représenté par la couleur Jaune
- Unreachable = état inconnu qui est représenté par la couleur Orange
- Pending =  état en attente qui est représenté par la couleur Grise
## Description d'une alarme
Une alarme dans Nagios est toujours représentée de la façon suivante :

AJOUTER PHOTO D ALARME NAGIOS

La 1ère colonne correspond au nom de l'hôte.

La 2ème colonne correspond au nom du service. Il peut y avoir plusieurs services par hôte.

La 3ème colonne correspond à l'état du service. 

La 4ème colonne correspond à la durée depuis le dernier changement d'état du service

La 5ème colonne correspond au nombre de check pour vérifier l'exactitude de l'alarme, on parle de Hard State et Soft State mais ceci n'est pas utilisé dans la supervision actuelle.

La 6ème colonne correspond au dernier check effectué.

La 7ème colonne correspond à l'Output de l'alarme.
## Etats des Services
Dans le menu de gauche sous Details, cliquer sur Service Status pour afficher la liste de tout les services ainsi que leurs états actuels.

Pour forcer l'actualisation d'un service ou d'un hôte, on clique dessus et ensuite dans Quick Actions, il y a "Force a immediate check".

Dans Status Details, on peut voir l'horodatage du dernier check et quand s'effectuera le prochain.

On se trouvais jusqu'à maintenant dans l'overview du service, il y'a 7 onglets en tout.

- Overview, affiche l'état du service, depuis quand il est dans cette état, l'horodatation du last check et du next check, les quick actions pour désactiver les notification et pour forcer un check immédiat.
- Performance Graphs, affiche le graphique de l'état du service
- Advanced, permet d'afficher des informations avancés, ce que je trouve utile ici c'est pour simuler une fausse alarme on peut aller dans Commands, Submit passive check result et forcer un state temporairement. Au prochain check, le statut est remis à jour. On peut aussi définir un downtime si on veut bosser sur des équipements.
- Configure, renvois sur la page de configuration des services
- Capacity Planning, fonction pas utilisé 
- Free Variables, fonction pas utilisé
- Network Traffic Analysis, fonction pas utilisé

# Ajout d'un service ou d'un hôte
## Via un wizard
Dans le menu principale en haut de l'écran, aller sur Configure et Configuration WIzards.

Il existe une 50ène de wizard préconfiguré par Nagios pour des équipements standard comme par exemple :

- switch réseau
- serveur dhcp
- mail server
- docker
- client NRPE
- Linux server
- windows server
- et bien d'autres

Une fois le wizard désiré choisi, il ne vous reste qu'à suivre ce qui est à l'écran.

En général il vous faut :

- Entrer une adresse IP
- Entrer un hostname
- Cocher les services à superviser
- Définir les seuils

Par défaut, vous constaterez que le check time est à 5min sur Nagios. Dans notre cas, il faut le mettre a 1min.

## Via le Core Config Manager
Dans le menu principale en haut de l'écran, aller sur Configure et Core Config Manager.

Choisissez ensuite si vous souhaitez créer un Host, un Service, un Host Group, un Service Group,...

Je conseille de faire une copy d'un service existant et de l'éditer, ça évitera d'oublier un paramètre.

Si vous souhaitez quand même créer un nouveau :

- Cliquer sur +Add New
- Mettre le hostname dans Config Name (si vous souhaitez associer ce service un groupe d'hôte, mettez un nom parlant pour le groupe d'hôtes)
- Mettre une description du service
- Choisir comment superiviser via Check Command (si vous ajoutez un hôte, vous n'avez pas besoin d'indiquer de check command, il prendra le ping par défaut)
- Choisir l'hote ou le groupe d'hôtes via Manage Hosts ou Manage Host Groups
- Cocher Active pour activer la supervision de ce service

Passer ensuite à l'onglet Check Settings :

- Check interval à 1min
- Retry interval à 1min
- Max check attempts à 5
- Check Period à 24x7
- Possiblité d'ajouter un Event Handler (une commande qui s'exécute sur la machine lorsque le trigger de cette alarme s'active, par exemple: nous redémarrons le service SMTP sur ecapage quand il plante)

Passer ensuite à l'onglet Alert Settings :

- Notification period à 24x7
- Faire Save

Le service ou l'hôte créé apparait dans la liste en rouge. Pour appliquer la configuration, vous avez Apply Configuration sur la gauche de l'écran.


## Réparation DB MySQL
Lors d'un crash du serveur, il se peut que des erreurs MySQL soit présent

La commande suivante permet de résoudre les erreur dans la DB

Se connecter en SSH en root@10.96.31.100 (mdp dans Keepas PROSDIS/Domain telem.local/srv-nagios-01 )
Exécuter la commande:

>/usr/local/nagiosxi/scripts/repair_databases.sh

source: https://assets.nagios.com/downloads/nagiosxi/docs/Repairing_The_Nagios_XI_Database.pdf


