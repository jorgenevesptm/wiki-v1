<!-- TITLE: Processus -->

# Processus 

## Création machines 
Pour la création des machines, une DCI est obligatoire, dans cette DCI, veuillez préciser les éléments suivants : 

- IP (les IP de PROD se terminent par .0, .1, .2, .3 ou .4 - Les IP de QA par .5 et suivant et la FORM suit la QA. Ceci vaut, bien entendu, **uniquement pour les VLANs qui ne sont pas séparés**)
- Réseau 
- Nom (hostname)
- OS
- RAM
- CPU
- Disque

Merci d'inclure aussi un **schéma des flux** avec "**de** (Host-name + IP) - **à** (Host-name + IP) - **Port** (+nom du protocole)"

Dans le processus de patch management, il est évident que **l'antivirus** Sentinel One doit être installé ! 

N'oubliez pas de mettre à jour le document du patch management ici : \\\ha-syno-01.telem.local\data\prosdis\Patch_Management\Télématique\20210224_EtatMAJ_Telem_BASE.xlsx

Enfin, merci d'indiquer si vous souhaitez que cette machine soit backupée, ou pas. 

![Annotation 2020 06 22 112231](/uploads/annotation-2020-06-22-112231.png "Annotation 2020 06 22 112231")

## Gestion des piquets
![Annotation 2020 05 26 080030](/uploads/annotation-2020-05-26-080030.png "Annotation 2020 05 26 080030")

## Gestion des tickets 
![Gestion Ticket](/uploads/gestion-ticket.png "Gestion Ticket")

## Gestion des évolutions
![Gestion Evo](/uploads/gestion-evo.png "Gestion Evo")

## Gestion des mises à jours MyStart+
![Gestion Mystart](/uploads/gestion-mystart.png "Gestion Mystart")

## Gestion des mise à jours 
![Gestion Maj](/uploads/gestion-maj.png "Gestion Maj")

## Départ d'un collaborateur

Supprimer l'utilisateur des plusieurs applications / systèmes:
* AgendaCTA
* Start
* Listes d'émails
* ECADIS
* Cellnet
* Nagios
* Fortigate
* TRACcess (accès sites radios)
* Protime mobile


Outillages et matériels
* Pager
* Laptop
* Valise outils 
* Badge accès sites