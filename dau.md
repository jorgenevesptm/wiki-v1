<!-- TITLE: Dau -->
<!-- SUBTITLE: Procédure de rétablisseement d'un DAU -->

# Reconnexion DAU

A faire par l'équipe Infrastructure

Lors de la visualisation des DAU sur Dag4000 un ou des DAU sont pas connecté.

- Reboot du DAU directement via Cellnet

Si cela ne fonctionne pas:

- Se connecter en RDP au serveur télémaintenance 10.96.201.10 (login domain telem.local)
- Exécuter logiciel USCT R11B02 SP01 en administrateur
- Sélectionner "system manager" (pas de mot de passe)
- Cliquer sur "Management network" -> "setup/connect" -> "run"
- Sélectionner "info1" ou "info2" et connect (en bas)
- Cliquer sur "file" -> "upload" (Upload import la configuration du MUX sur le logiciel)
**NE JAMAIS FAIRE DE DOWNLOAD**
    - Si une erreur lors de l'upload, désactiver le par-feu windows
- Double clic sur carte "ETHER1" à droite de la carte "CODBUX"
- Aller dans "TDM interface"
- Vérifier que le site impacté est dans la liste, sinon refaire la même procédure avec INFO2 (ou INFO1 dépendant de la connexion)
- Décocher "Enable" sur le site impacté puis sur "OK"
- Cliquer sur "File" -> "Partial download"
- Attendre quelques minutes

- Refaire les même action mais cocher "Enable" pour reconnecter le site
- Contrôler que le site est denouveau en ligne sur DAG4000 (cela peut prendre un certain temps)


Il se peut qu'une arme de temps arrive sur le site

- Aller sur USCT "Management Network" -> Setup/Connect
- Si le site ne figure pas dans la liste en bas, il faut l'ajouter
- Cliquer sur Add à droite
    - Laisser Type
    - Name entrer le nom du site
    - IP adresse entreé l'IP du DAU (172.27.<site>.8)
    - Pas de domain et password
    - Cliquer OK
- Sélectionnez le site et cliquer connect
- Cliquer "File" -> "Upload" pour récupérer la configuration
**NE JAMAIS FAIRE DE DOWNLOAD**
- Refuser la demande de sauvegarde
Une fois l'upload fait, 
- Cliquer sur "Fault" (en haut) -> "Network Element"
- Cliquer sur Get pour récupérer les détails des alarmes

Pour rétablire le temps
- Aller dans "NE Configuration" (en haut) -> "Set Time"
- Cliquer plusieurs fois "Get" puis "Set" jusqu'à ce que l'horloge soit synchrone

