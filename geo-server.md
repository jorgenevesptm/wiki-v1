<!-- TITLE: GeoServer -->
<!-- SUBTITLE: A quick summary of GeoServer -->

# GeoServer
Dans cette rubrique, vous trouverez toutes les informations nécessaires à la configuration, au dépannage, etc des serveurs GeoServer de Systel et celui de la Télématique


* <a href="http://wiki.telem.local/geo-server/calcul-du-cache">Calcul du cache</a>

* <a href="http://wiki.telem.local/geo-server/creation-archive-cache">Création de l'archive du cache</a>

* <a href="http://wiki.telem.local/geo-server/copie-archive-cache">Copie de l'archive du cache</a>

* <a href="http://wiki.telem.local/geo-server/extraction-archive-cache">Extraction de l'archive du cache</a>

* <a href="http://wiki.telem.local/geo-server/copie-rasters">Copie des rasters dans les GeoServer</a>

* <a href="http://wiki.telem.local/geo-server/creation-carte-offline">Création de la carte offline</a>

* <a href="http://wiki.telem.local/geo-server/creation-troncons-routiers">Création des tronçons routiers</a>

* <a href="http://wiki.telem.local/geo-server/indexation-lieu-start">Indexation des lieux Start après importation par ETL GeoData</a>

### Les adresses IP des geoservers :

* 	PROD		srv-geoserver-01 [10.144.2.37]
*   FORM		srv-geoserver-1-01-form [10.144.2.58]
*   TEST		 srv-geoserver-1-01-test [10.144.2.54]

### Le cache est stocké sur le serveur nfs :
	
* 	PROD		srv-geo-nfs [10.118.112.9]
*   FORM		srv-geo-nfs-form [10.118.112.7]
*   TEST		 srv-geo-nfs-test [10.118.112.8]

Pour s’y connecter : ssh sur un GeoServer puis ssh sur le serveur nfs

Pour être root sur les serveurs : su -

### Chemin d’accès :

srv-geoserver :	/mnt/shared/geoserver_data/external_data/eca_vaud/blobstore/

srv-geo-nfs :		/DATA/geoserver/geoserver_data/external_data/eca_vaud/blobstore/

Pour voir la taille disque du cache : **du -sh eca_vaud*** attention peut être long avec un gros cache

Pour avoir le nombre de fichier : **find ecavaud01 -type f | wc -l** un peu plus rapide

### Pour mettre une cmd en backgroud :

1. Exécuter sa commande
2. Faire un crtl+z
3. Exécuter la cmd : bg
4. Optionnel : si on veut quitter / fermer la console sans arrêter sa commande en cours, il faut exécuter la cmd : disown






