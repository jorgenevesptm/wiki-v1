<!-- TITLE: Integration Mibs Ups -->
<!-- SUBTITLE: Configuration de MiBs sur Cellnet pour supervision des UPS -->

# Intégration des MiBs d'UPS dans Cellnet

## Introduction
Ce document déclare comment intégré les MiBs d'UPS dans Cellnet (spécialement les cellnets des sites radios). L'exemple utilisé concerne particulièrement les UPS de la marque Socomec.
Le même processus peut être utilisé pour d'autre marque d'UPS

## 1. Ajout de l'UPS dans Cellnet
### 1.1 Création du type de device UPS Socomec

1. Ouvrir le cellnet du site radio, aller dans "Devices Management" -> "Devices Type list"
2. Click sur Create pour créer un nouveau device et entrer les informations suivantes:
<img src="http://10.96.128.100/wiki-images/cri/media/capt1.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt2.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt3.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt4.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt5.PNG">

### 1.2 Création de l'UPS basé sur le Type
1. Une fois le type de device créé, Allez dans "Devices Management" -> "Device List"
2. Créer un nouveau device en cliquant sur "Add Device" et entrer les informations suivantes:
<img src="http://10.96.128.100/wiki-images/cri/media/capt6.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt7.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt8.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt9.PNG">

**CLIQUEZ SUR "SAVE" en bas de la fenêtre pour enregistrer**

### 1.2 Création du lecteur de Trap
1. Une fois le type de device créé, Allez dans "Devices Management" -> "Device List"
2. Créer un nouveau device en cliquant sur "Add Device" et entrer les informations suivantes:
<img src="http://10.96.128.100/wiki-images/cri/media/capt10.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt11.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt12.PNG">
<img src="http://10.96.128.100/wiki-images/cri/media/capt13.PNG">


## 2. Création des Events Templates
1. Aller dans le menu "Events Configuration" -> "Event Template"
2. Créer les évènements suivant si non existant (Cela peut être déjà présent) :
<img src="http://10.96.128.100/wiki-images/cri/media/capt18.PNG">


## 3. Création du Case
1. Ouvrir "Case List"
2. Créer un nouveau Case
<img src="http://10.96.128.100/wiki-images/cri/media/capt14.PNG">
3. Ajouter les Devices Trap Socomec dans le AND -> OR (Attention à bien les déplacer dans le OR)
4. Pour les deux trigger (SNMPV1 et SNMPV2) Désactivé le "Publish Info" en faisant un clique droit "Edit"
<img src="http://10.96.128.100/wiki-images/cri/media/capt15.PNG">
5. Ajouter le SOCOMEC Trap aussi sur la droite dans la section "Activations actions"
6. Click droit dans "Socomec UPS (SNMP trap)" dans le "Activation actions" et entrer les règles suivantes

<img src="http://10.96.128.100/wiki-images/cri/media/capt23.PNG">

*  Ou uploader le json suivant:



```text
[{"id":"","rules":[{"id":"tmp_18619","attribute":"enterprise","rule":"1.3.6.1.4.1.4555.1.1.7.2"},{"id":"tmp_969","attribute":"specific","rule":"^1$"}]},{"id":"tmp_27258","rules":[{"id":"tmp_22456","attribute":"enterprise","rule":"1.3.6.1.4.1.4555.1.1.7.2"},{"id":"tmp_88870","attribute":"specific","rule":"^23$"}]},{"id":"tmp_31497","rules":[{"id":"tmp_65305","attribute":"enterprise","rule":"1.3.6.1.4.1.4555.1.1.7.2"},{"id":"tmp_90065","attribute":"specific","rule":"^6$"}]},{"id":"tmp_31805","rules":[{"id":"tmp_74478","attribute":"enterprise","rule":"1.3.6.1.4.1.4555.1.1.7.2"},{"id":"tmp_93883","attribute":"specific","rule":"^22$"}]},{"id":"tmp_89249","rules":[{"id":"tmp_42369","attribute":"enterprise","rule":"1.3.6.1.4.1.4555.1.1.7.2"},{"id":"tmp_86681","attribute":"specific","rule":"^11$"}]},{"id":"tmp_63712","rules":[{"id":"tmp_33222","attribute":"enterprise","rule":"1.3.6.1.4.1.4555.1.1.7.2"},{"id":"tmp_56171","attribute":"specific","rule":"^24$"}]},{"id":"tmp_98731","rules":[{"id":"tmp_62502","attribute":"enterprise","rule":"1.3.6.1.4.1.4555.1.1.7.2"}]},{"id":"tmp_37658","rules":[{"id":"tmp_92525","attribute":"enterprise","rule":"1.3.6.1.4.1.4555.1.1.7.2"},{"id":"tmp_83960","attribute":"specific","rule":"^24$"}]}]
```
