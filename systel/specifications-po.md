<!-- TITLE: Specifications Po -->
<!-- SUBTITLE: A quick summary of Specifications Po -->

# Specifications techniques poste opérateur
## Station de travail 
- HP Z240 CMT / Xeon E3-1245V5                           
-	1 SSD Intel 535 360Go 2,53'' interne SATA/600                   
-	Carte Graphique HPQuadro NVS540 – 2Go GDDR5 SDRAM – PCI Express – 4 mini DisplayPort                 
-	PCI Express – 2 voie DisplayPort / 512Mo / PCIEx16 / Nvidia quadro                      
-	Convertisseur DD&SSD 2,5'' 0 3,5'' et connectique                           
-	Connectiques SATA/Vidéo et réseau                      

## Postes opérateurs
- SHUTTLE DH170
-	Core i5-6400 / SSD 256Go / 8 Go / clavier-souris
-	OS MS Windows 10 Pro 64 bits / LAN dual Gigabit
-	3 sorties vidéo (2 DisplayPort + 1 HDMI)
