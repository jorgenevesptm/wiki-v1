<!-- TITLE: Création Raccourci -->
<!-- SUBTITLE: A quick summary of Création Raccourci -->

# Création d'un raccourci sur un PO
1. Se connecter avec le compte administrateur sur le PO
2. Ouvrir l'explorateur de fichier
3. Aller dans \\cta.local\SYSTEL\Profils
4. Créer un nouveau raccourci (clique droit nouveau -> raccourci)
