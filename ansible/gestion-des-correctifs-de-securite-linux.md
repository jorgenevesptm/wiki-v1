<!-- TITLE: Linux - Gestion des correctifs de sécurité via Ansible-->
<!-- SUBTITLE: Procédure -->

# Présentation
Ansible est un outil de gestion de configuration qui permet d'automatiser des tâches avec des scripts d'automatisation. C’est	un	système	agentless	capable	de	piloter	des	systèmes	Windows,	Linux,	Unix	et	aussi	des	équipements	réseau	tel	que	Cisco	ou	Juniper.

Le serveur central d'Ansible est **srv-linux-mgt-01** avec un compte **ansible** local. Pour fonctionner ansible central a besoin l'accèss **ssh** à tous ses hôtes ainsi qu'une connexion transparente (sans le mot de passe). Ceci se fait au niveau du protocol ssh (port 22) et à l'échange de clé publique **ssh** (réf. **Intégration de serveur linux dans l'Ansible**).

L'ensemble des scripts pour les tâches du patching se trouvent dans le répertoire **/home/ansible/ansible/patch** du serveur **srv-linux-mgt-01**.
Toutes les étapes citées dans ce document doivent être éxécutées depuis le serveur ansible central **srv-linux-mgt-01** avec le compte **ansible**. Actuellement seules les distros **RedHat/Centos** et **Debian/Ubuntu** sont supportées.
# Les étapes principales
1. Préparation des fichiers inventaires hosts
2. Déclaration du type d'action dans le fichier principal d'ansible (**/home/ansible/ansible/patch/main.yml**)
3. Ouverture l'accès temporaire à l'internet pour cas spéciaux
4. Application des correctifs de sécurité via l'ansible-playbook
5. Établissement du rapport global des correctifs appliqués aux serveurs clients
6. Troubleshooting

# Préparation des fichiers inventaires hosts
Ansible contient un fichier d’inventaire, communément appelé inventory et dans lequel on renseigne les serveurs sur lesquels on effectue nos actions. 
Il existe actuellement 4 fichiers inventaires prédéfinis dans le répertoire **/home/ansible/ansible/patch** :
- hosts_prod
- hosts_management
- hosts_formation
- hosts_qa

Il est important que les serveurs hôtes soient classés correctement dans leur fichiers d'inventaire correspondant par leur environnement et qu'**ansible** puisse les effectuer des commandes ssh sans le mot de passe. Pour valider la connexion avec les serveurs hôtes d'un fichier inventaire:
se connecter au serveur **srv-linux-mgt-01** avec le compte **ansible**
```text
	cd ansible/patch
	ansible -i hosts_qa all -m ping
```
assurer que le retour de chaque serveur hôte soit **SUCCESS** !

Il se peut qu'il y ait ce genre de message de WARNING:

>[DEPRECATION WARNING]: Distribution Ubuntu 18.04 on host srv-ipa-qa-01 should use **/usr/bin/python3** , but is using /usr/bin/python for backward compatibility with prior Ansible releases. A future Ansible release will default to using the discovered platform python for this host. See https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information. This feature  will be removed in version 2.12. Deprecation warnings can be disabled by setting deprecation_warnings=False in ansible.cfg.

Il suffit d'éditer le fichier inventaire en question et d'ajouter la variable **ansible_python_interpreter** comme ci-dessous
nom-du-linux-serveur            **ansible_python_interpreter=/usr/bin/python3**
	

# Définition du type d'action dans le fichier principal d'ansible
Un playbook a été prédifini permettant d’exécuter un ensemble de tâches sur	les	machines hôtes de	manière	séquentielle.
Les variables ci-dessous du playbook **/home/ansible/ansible/patch/main.yml** définissent les actions précises lors de son éxécution.

  vars:
	#OS Patch level. Options = none | security | full | test
	 - ospatch_level: **security**
	#kernel cleanup. Options = true | false
	- ospatch_remove_old_kernel: **false**
	#Reboot server. Options = true | false
	- ospatch_reboot: **false**

**Ces variables doivent être déclarés et vérifiés avant chaque lancement de son éxécution !**

ospatch level:
**security**   : appliquer uniquement des correctifs de sécurité
	full          : appliquer tous les correctifs de la distribution linux
  none       : pas de correctifs. Il est util si vous voulez  rebooter uniquement les serveurs hôtes
	
ospatch_autoremove:
 true         : désinstaller les librairies ou dépendances qui ne sont utilisées par aucun programme
 **false**        : ne rien désinstaller

ospatch_reboot:
true         : vérifier si le serveur hôte néccessiterait un reboot. Si le cas, le reboot aura lieu. Sinon, pas de reboot!
**false**        : ne pas rebooter le serveur hôte après l'application des patches
> Il est recommandé de ne pas activer le reboot automatique des serveurs de PROD mais plutôt de le faire de manière manuelle serveur par serveur !

# Ouverture l'accès temporaire à l'internet pour cas spéciaux
Lors d'application des correctifs de sécurité, le serveur hôte accèdera uniquement aux dépôts de distribution Linux via un proxy APT cache. Les serveurs hébergeant des applications Oracle/java8 doivent  avoir l'accès à l'internet afin de pouvoir télécharger ses paquets de sécurité. Il est important donc d'ouvrir temporairement l'accès à l'internet pour ces hôtes avant de lancer l'éxécution. A présent, ces serveurs suivants sont concernés:
- srv-ecapage-01
- srv-ecapage-02
- srv-ecapage-qa-01
- srv-ecapage-qa-02
- srv-ecapage-sec
- srv-nagios-01
- srv-unifi-01
- srv-syslog-01
- srv-syslog-qa-01
- srv-scm-01
- srv-grafana-01



Une fois que les correctifs ont eu lieu, l'accès à l'internet doit être retiré.
PS: Un groupe **GRP_TELEM_PATCH_INTERNET**, contenant les serveurs mentionnés ci-dessus, a été crée dans le Fortinet que vous pouvez lui donner l'accès temporairement à l'internet durant cette session de patch.
# Application des correctifs de sécurité via l'ansible-playbook
## Automatisation de la gestion des correctifs de sécurités
Un ensemble de tâches suivantes seront éxécutées sur les	machines hôtes de	manière	séquentielle et automatisée:
1. Création du répertoire **/var/log/ansible**
2. Désactivation de l'option **auto-upgrades**
3. Désactivation de l'option **auto periodic packages upgrade**
4. Configuration du **proxy d'APT cacher** (sauf les serveurs **\*ecapage\***)
5. Mise à jour la liste des fichiers disponibles dans les dépôts APT présents dans le fichier de configuration /etc/apt/sources.list
6. Création de la liste **/etc/apt/security.sources.list** comprenant uniquement des dépôts de sécurités 
7. Application de correctifs basant uniquement de la liste **/etc/apt/security.sources.list**
8. Création du rapport local de correctifs appliqués ansi qu'une copie envoyée vers le server ansible master **srv-linux-mgt:/var/log/ansible**
9. Si la variable **ospatch_autoremove = true**, désinstallation de librairies ou dépendances qui ne sont utilisées par aucun programme
10. Si la variable **ospatch_reboot = true**, le serveur hôte sera redémarré (seulement en cas de nécessité)

## Exécution du playbook main.yml
```text
cd /home/ansible/ansible/patch
ansible-playbook main.yml -i <hosts_xxx>
```
A remplacer le fichier inventaire par celui correspondant à l'environnement que vous voulez appliquer les correctifs de sécurité.
Exemple:  **ansible-playbook main.yml -i hosts_prod**

# Établissement du rapport global des correctifs appliqués aux serveurs clients
Une fois que les correctifs ont été appliqués, il est temps de consolider tous les logs provenant de serveurs hôtes dans **/var/log/ansible**.
```text
cd /var/log/ansible
/home/ansible/scripts/patch_logs.pl  -startdate yyyymmdd  -enddate yyyymmdd
```

**yyyymmdd** : correspond à la date du log généré automatiquement lors de son éxécution de correctifs
L'option **-startdate** et **-enddate** comprend la période de jours pour les logs à consolider

Le log consolidé sera crée dans **/var/log/ansible** sous le nom **linux-patching-yyyymmdd-yyyymmdd.log**

Pour avoir plus de détail du Perl script, éxécuter cette commande
```text
/home/ansible/scripts/patch_logs.pl -h
```


# Troubleshooting
## APT Cacher - OUT OF DISK SPACE
Le serveur **srv-apt-01** est une solution proxy de mise en cache des paquets Debian/RedHat. À travers ce proxy, un ensemble d'ordinateurs clients accède indirectement aux dépôts. 
Quand un paquet est demandé pour la première fois, il est téléchargé par le proxy et transmis au client tout en conservant une copie en local. Pour toute future demande du même paquet, le proxy ne télécharge pas les paquets mais transmet la copie locale. Il se pourrait que l'espace disque du serveur soit saturée et le processus de l'upgrade depuis le client ne serait possible:
>W: Failed to fetch http://archive.ubuntu.com/ubuntu/dists/bionic-updates/InRelease  503  OUT OF DISK SPACE [IP: 10.96.128.5 9999]
W: Failed to fetch http://archive.ubuntu.com/ubuntu/dists/bionic-backports/InRelease  503  No space left on device [IP: 10.96.128.5 9999]
W: Failed to fetch http://archive.ubuntu.com/ubuntu/dists/bionic-security/InRelease  503  No space left on device [IP: 10.96.128.5 9999]
W: Some index files failed to download. They have been ignored, or old ones used instead.

Pour y remédier, il faut augmenter l'espace du disque du serveur ou effacer certains paquets caches obsolètes dans **srv-apt-01:/var/cache/apt-cacher-ng/**
