<!-- TITLE: Intégration de linux serveur dans l'Ansible-->
<!-- SUBTITLE: Procédure -->

# Les étapes principales
1. Mettre à jour le **Custom Attribute** dans vCenter
1. Mettre à jour le **DNS**
1. Création du compte **ansible** sur chacun des serveurs client
1. Déployer la clé publique SSH d'**ansible** sur chacun des serveurs client
1. Test de connexion

# Mettre à jour **Custom Attribute** dans vCenter
Le but est de classer le serveur par environnement afin d'être patché ultérieurement par Ansible selon leur calendrier. Ces **Custom Attribute** peuvent être extraits à l'aide du script **Telem_Snapshot.ps1** afin de générer la liste des serveurs selon leur environnements qui serviront comme **inventory host** pour Ansible.

Les environnements prédefinis sont:
- TELEM-LINUX-PROD
- TELEM-LINUX-FORMATION
- TELEM-LINUX-MANAGEMENT
- TELEM-LINUX-QA
- TELEM-LINUX-TEST

## Procédure
Se connecter au vCenter -> selectionner le serveur -> selectionner **Custom Attribute** -> Click **Edit...** -> Ajouter le nom du server dans **Attribute** et son environnement dans **Value** -> Click **Add**

![Vcenter 01](/uploads/ansible/vcenter-01.jpg "Vcenter 01")

# Mettre à jour le **DNS**
Assurer que le serveur client soit défini dans DNS afin que **ansible** puisse envoyer ses commandes à distance via leur hostname.

# Création du compte **ansible** sur chacun des serveurs client
Afin qu'**ansible** puisse s'éxécuter ses commandes via le protocole SSH vers le serveur client sans le mot de passe, il est recommandé de créer un compte **ansible** local sur le serveur du client et une	configuration	sudo	sans	mot	de	passe.

## Procédure
1. Se connecter au serveur client, sudo à **root**
1. Créer le compte **ansible**, selon la distribution de linux ci-dessous

Ubuntu distribution

	adduser --quiet --disabled-password --uid 2001 --gecos "Ansible Management" ansible
	usermod -aG sudo ansible

Centos distribution

	adduser ansible -m -u 2001 -c "Ansible Management" 
	usermod -aG wheel ansible

Ensuite exécuter ces commandes (valable pour toutes distributions)

	echo "ansible ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/ansible
	sudo passwd ansible  (définir votre mot de passe) à vérifier si nécessaire - jet 210127

# Déployer la clé publique SSH d'**ansible** sur chacun des serveurs client
On placera la clé publique SSH d'**ansible** du master vers ce compte local.

	su - ansible
	ssh-keygen  (presser enter à chaque prompt)

	cd .ssh
	vi authorized_keys


A coller la clé publique SSH d'**ansible** du master (se trouvant sur **srv-linux-mgt:/home/ansible/.ssh/id_rsa.pub**) ou celle ci-dessous

	ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDOcsp0o0GNVAz2xrxmvvdW7GesJbhc+Wra83gCiVSEvKB6TNw3pF1HPUg4/WhhKQNww9WA8Ve2YsMyqA6tkgBQCYgcHk/ZekmtcStZuVqf6X304rV72ASV9RkghQYQLYEkUPEXqJVXEac1jTZtqBccIAL9DLGVg+x3kvGga8K42XFe1QJr+jNR7D4QNhTlwyAgVpWUx+FVdff/ACPu83leI8lBam/Pv/gIXD+gAgFiBNglsPv8n+8a/ZrEUHWIibUn3V3kP/EaAsy7A6kZbu5vwfx9e0/2Y8pb1a0tqE3qqLCtGh/8qbNo8zjlwehgOrA/QTH2jFejDeeMeIxJB6O+PNUmRu8EzFhR2zIyqQ6D3OsG9h9hLEC9lV87KfMuZqqsyip5bCL5yAf0W/vfyCOdmWyMKJ3gO4EAuaZJzJ1BPBKcGonpVmu9Z54vKTsCWVPRdILQndP5p4JldR7V+MOpjg8+auWFeGe8mJI1TsPrrBijIg5kDviw9x9IjwwHdGk= ansible@srv-linux-mgt-01
				
Ensuite changer l'attribue d'accès du fichier **authorized_keys**
```text
	chmod 600 authorized_keys	
```

# Test de connexion
Il est important de vérifier le bon fonctionnement de l'**ansible** du master au nouveau serveur client.
Se connect au seveur **srv-linux-mgt-01** avec le compte **ansible** du master (son mot de passe se trouve dans Keepass)
```text
	cd /home/ansible/test
	vi host_test (ajouter le nom du nouveau serveur client, ex. srv-linux-client-01, et le sauvegarder)
	ansible -i host_test all -m ping
```
Si l'échange de clé se passe correctement, vous devriez obtenir **SUCCESS** comme ci-dessous
```text
	srv-linux-test-01 | SUCCESS => {
			"changed": false,
			"ping": "pong"
	}
```
Si ce n'est pas le cas, il faut revérifier la procédure décrite ci-dessus!



