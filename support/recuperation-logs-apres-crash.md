Ce document est utile en cas de problème sur un des ESX ou SAN
Le but étant de récupérer les « logs bundle » afin d’ouvrir différents cas auprès des fournisseurs (HPe et VMWare)
# 	Récupérer les logs VMWare sur l’ESX
a.	Première étape, se connecter sur l’ESX concerné par le problème via un navigateur
i.	Login : root / mot de passe dans Keepass
b.	Puis, « Actions / Générer un Bundle de Support »
 ![Vcenter](/uploads/vcenter.png "Vcenter")
c.	Une fois le Bundle téléchargé, il faut créer le cas chez VMWare
d.	Sébastien doit donner des accès au compte VMWare Support
 
# Ouvrir le cas de support chez HPe
a.	Se connecter sur https://ahsv.support.hpe.com/hpesc/public/home/signin
i.	Login et mot de passe dans KeePass
b.	Access Cases
c.	Submit case
i.	Entrée du numéro de série de l’appareil 
d.	Puis, dans le formulaire, remplir le maximum d’informations possible
e.	Enfin, upload des différents bundle de logs décrit aux points suivants 

# Récupérer les logs de l’ESX côté HPe
a.	Se connecter sur l’ESX concerné via l’ILO
i.	Mot de passe sur Keepass et IP dans IPAdmin
b.	Remplir les différents champs décrits
 ![Hpe](/uploads/hpe.png "Hpe")
Suite de quoi, le document téléchargé pourra être envoyé à HPe

# Récupérer les logs du SAN-ILO
a.	Se connecter sur le SAN concerné via l’ILO
i.	Mot de passe sur Keepass et IP dans IPAdmin
b.	Puis même procédure que pour l’ESX 

# Récupérer les logs du SAN
a.	Se loguer dans le CMC (Centralized Management Console)
i.	10.64.3.254
b.	Clic droit sur « Management Group »
c.	Export Management Group Support Bundle
 ![Cmc](/uploads/cmc.png "Cmc")

# Récupérer les logs des Fortigate

get system status
get system startup-error-log
diag sys session stat
diag sys top 1 30 <- allow this to cycle about 5 times then press q to stop it.
diag sys top-summary <- allow this to cycle about 5 times then press q to stop it.
diag debug crashlog read
