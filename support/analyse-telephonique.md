<!-- TITLE: Analyse Telephonique -->
<!-- SUBTITLE: Page d'aide pour faire des analyses téléphoniques COS et Asterisk (DTMF et Mobilisation) -->

# Analyse des logs téléphonique Swissdotnet
Se connecter sur **srv-syslog-01.telem.local** (pass sur le KeePass)
Utiliser l’utilisateur root pour avoir accès au répertoire */var/log/remote*
* sudo –i (le même mdp qu'administrator)

## Prérequis
* Numéro d’intervention
	* **6763**
* Les NIPs des sapeurs-pompiers qui ont eu le problème
	* **50435**

## Rapport Engagement CTA
* Chercher sur Rapport Engagement CTA l’intervention
* Ouvrir le ‘Déroulement de l’intervention’
* Chercher le NIP, récupérer le numéro de téléphone sur lequel la Synthèse Vocal est sorti et contrôler les logs START pour la mobilisation
	* TEL: **41793237828**
	
	![Rap Get Phone Number](/uploads/rap-get-phone-number.png "Rap Get Phone Number")

## Pour faire une analyse des codes DTMF
* Chercher le log sur */var/log/remote/cos/YYYY/MM/DD*
* Faire la recherche
	* **zcat cos.log.gz | grep -a "41793237828"**

> Nov 19 10:04:47 dc1-srv-cos-01 [Phone mobilisation thread-55] ch.swissdotnet.prosdis.cos.managers.mobilisation.phone.PhoneMobilisationAmi [Phone Mobilisation AMI] Call target [+41793237828] notification id [d48d2ddb-9bbb-493c-9e30-22b2dda58eeb]
> Nov 19 10:04:56 dc1-srv-cos-01 [Asterisk-Java ManagerConnection-7-Reader-0] org.asteriskjava.manager.internal.backwardsCompatibility.bridge.BridgeState Members size 2 org.asteriskjava.manager.event.BridgeEnterEvent[dateReceived='Tue Nov 19 10:04:56 UTC 2019',privilege='call,all',linkedid='1574154287.1802688',server=null,calleridname='mobilisation-voip',bridgeuniqueid='54cf5063-f897-4e2d-ac7e-e557edc787cd',channel='Local/+41793237828@mobilisation-voip-000379f7;2',language='fr',exten='+41793237828',calleridnum='+41212132222',context='mobilisation-voip',bridgetechnology='simple_bridge',connectedlinenum=null,uniqueid='1574154287.1802690',channelstatedesc='Up',timestamp='1.57415429687721E9',systemname=null,bridgetype='basic',bridgenumchannels='2',connectedlinename='mobilisation-voip',swapuniqueid=null,bridgecreator=null,sequencenumber=null,priority='6',channelstate='6',bridgename=null,accountcode='',systemHashcode=527699288]
> Nov 19 10:04:56 dc1-srv-cos-01 [Asterisk-Java ManagerConnection-7-Reader-0] org.asteriskjava.manager.internal.backwardsCompatibility.bridge.BridgeState Bridge Local/+41793237828@mobilisation-voip-000379f7;2 SIP/cablecom-trunk-0009d17f
> Nov 19 10:04:56 dc2-srv-cos-01 [Asterisk-Java ManagerConnection-0-Reader-0] org.asteriskjava.manager.internal.backwardsCompatibility.bridge.BridgeState Members size 2 org.asteriskjava.manager.event.BridgeEnterEvent[dateReceived='Tue Nov 19 10:04:56 UTC 2019',privilege='call,all',linkedid='1574154287.1802688',server=null,calleridname='mobilisation-voip',bridgeuniqueid='54cf5063-f897-4e2d-ac7e-e557edc787cd',channel='Local/+41793237828@mobilisation-voip-000379f7;2',language='fr',exten='+41793237828',calleridnum='+41212132222',context='mobilisation-voip',bridgetechnology='simple_bridge',connectedlinenum=null,uniqueid='1574154287.1802690',channelstatedesc='Up',timestamp='1.57415429687721E9',systemname=null,bridgetype='basic',bridgenumchannels='2',connectedlinename='mobilisation-voip',swapuniqueid=null,bridgecreator=null,sequencenumber=null,priority='6',channelstate='6',bridgename=null,accountcode='',systemHashcode=571536181]
> Nov 19 10:04:56 dc2-srv-cos-01 [Asterisk-Java ManagerConnection-0-Reader-0] org.asteriskjava.manager.internal.backwardsCompatibility.bridge.BridgeState Bridge Local/+41793237828@mobilisation-voip-000379f7;2 SIP/cablecom-trunk-0009d17f
> Nov 19 10:04:58 dc1-srv-cos-01 [AJ DaemonPool-1.962] ch.swissdotnet.prosdis.cos.managers.mobilisation.phone.agi.MobilisationAgiScript [Mobilisation script] Receive AGI script request with validation code [1] and invalid code [0] for channel [Local/+41793237828@mobilisation-voip-000379f7;1] and id [1574154287.1802688]
> Nov 19 10:04:58 dc1-srv-cos-01 [AJ DaemonPool-1.962] ch.swissdotnet.prosdis.cos.entities.mobilisation.MobilisationDtmfEntry [Mobilisation DTMF] chain is erased for channel [Local/+41793237828@mobilisation-voip-000379f7;1] and id [1574154287.1802688]
> Nov 19 10:05:02 dc1-srv-cos-01 [AJ DaemonPool-1.962] ch.swissdotnet.prosdis.cos.managers.mobilisation.phone.agi.MobilisationAgiScript **Receive DTMF [1] for channel [Local/+41793237828@mobilisation-voip-000379f7;1]** and id [1574154287.1802688]
> Nov 19 10:05:02 dc1-srv-cos-01 [AJ DaemonPool-1.962] ch.swissdotnet.prosdis.cos.entities.mobilisation.MobilisationDtmfEntry [Mobilisation DTMF] chain is now [1] for channel [Local/+41793237828@mobilisation-voip-000379f7;1] and id [1574154287.1802688]
> Nov 19 10:05:02 dc1-srv-cos-01 [AJ DaemonPool-1.962] ch.swissdotnet.prosdis.cos.managers.mobilisation.phone.agi.MobilisationAgiScript **Receive DTMF [#] for channel [Local/+41793237828@mobilisation-voip-000379f7;1]** and id [1574154287.1802688]


## Pour faire une analyse sur les mobilisations
* Chercher le log sur */var/log/remote/telephonie/YYYY/MM/DD*
* Faire la recherche
	* **zcat telephonie.log.gz | grep –a "41793237828"**

	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255]**[C-00081313]** pbx.c: Executing [+41793237828@mobilisation-voip:1] NoOp("Local/+41793237828@mobilisation-voip-000379f7;2", "Do mobilisation call with primary voip") in new stack
	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] pbx.c: Executing [+41793237828@mobilisation-voip:2] Set("Local/+41793237828@mobilisation-voip-000379f7;2", "CALLERID(num)=+41212132222") in new stack

	Récupérer le channel **[C-00081313]** et faire le grep en utilisant le numéro de téléphone et le channel pour être sûr que nous avons toutes les logs pour la mobilisation.

	* **zcat telephonie.log.gz | grep –a "41793237828\\|C-00081313"**

	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] pbx.c: Executing [+41793237828@mobilisation-voip:1] NoOp("Local/+41793237828@mobilisation-voip-000379f7;2", "Do mobilisation call with primary voip") in new stack
	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] pbx.c: Executing [+41793237828@mobilisation-voip:2] Set("Local/+41793237828@mobilisation-voip-000379f7;2", "CALLERID(num)=+41212132222") in new stack
	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] pbx.c: Executing [+41793237828@mobilisation-voip:3] GotoIf("Local/+41793237828@mobilisation-voip-000379f7;2", "0?testMode") in new stack
	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] pbx.c: Executing [+41793237828@mobilisation-voip:4] Gosub("Local/+41793237828@mobilisation-voip-000379f7;2", "convert-to-internationnal-plus,+41793237828,1") in new stack
	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] pbx.c: Executing [+41793237828@convert-to-internationnal-plus:1] Return("Local/+41793237828@mobilisation-voip-000379f7;2", "+41793237828") in new stack
	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] pbx.c: Executing [+41793237828@mobilisation-voip:5] Set("Local/+41793237828@mobilisation-voip-000379f7;2", "FORMATTED=+41793237828") in new stack
	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] pbx.c: Executing [+41793237828@mobilisation-voip:6] Dial("Local/+41793237828@mobilisation-voip-000379f7;2", "SIP/cablecom-trunk/+41793237828") in new stack
	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] netsock2.c: Using SIP RTP CoS mark 5
	> Nov 19 10:04:47 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:47] VERBOSE[28255][C-00081313] app_dial.c: **Called SIP/cablecom-trunk/+41793237828**
	> Nov 19 10:04:48 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:48] VERBOSE[11644][C-00081313] res_rtp_asterisk.c: 0x7f1bdc013480 -- Strict RTP learning after remote address set to: 62.2.46.4:17816
	> Nov 19 10:04:48 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:48] VERBOSE[28255][C-00081313] app_dial.c: SIP/cablecom-trunk-0009d17f is making progress passing it to Local/+41793237828@mobilisation-voip-000379f7;2
	> Nov 19 10:04:48 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:48] VERBOSE[28253] dial.c: Local/+41793237828@mobilisation-voip-000379f7;1 is making progress
	> Nov 19 10:04:48 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:48] VERBOSE[28255][C-00081313] res_rtp_asterisk.c: 0x7f1bdc013480 -- Strict RTP switching to RTP target address 62.2.46.4:17816 as source
	> Nov 19 10:04:49 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:49] VERBOSE[28255][C-00081313] app_dial.c: SIP/cablecom-trunk-0009d17f is ringing
	> Nov 19 10:04:49 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:49] VERBOSE[28253] dial.c: **Local/+41793237828@mobilisation-voip-000379f7;1 is ringing**
	> Nov 19 10:04:50 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:50] VERBOSE[28255][C-00081313] app_dial.c: SIP/cablecom-trunk-0009d17f is ringing
	> Nov 19 10:04:53 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:53] VERBOSE[28255][C-00081313] res_rtp_asterisk.c: 0x7f1bdc013480 -- Strict RTP learning complete - Locking on source address 62.2.46.4:17816
	> Nov 19 10:04:56 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:56] VERBOSE[28255][C-00081313] app_dial.c: SIP/cablecom-trunk-0009d17f answered Local/+41793237828@mobilisation-voip-000379f7;2
	> Nov 19 10:04:56 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:56] VERBOSE[28253] dial.c: **Local/+41793237828@mobilisation-voip-000379f7;1 answered**
	> Nov 19 10:04:56 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:56] VERBOSE[28253][C-00081321] pbx.c: Executing [1@mobilisation-call:1] Log("Local/+41793237828@mobilisation-voip-000379f7;1", "NOTICE, Do call for channel [Local/+41793237828@mobilisation-voip-000379f7;1]") in new stack
	> Nov 19 10:04:56 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:56] NOTICE[28253][C-00081321] Ext. 1:  Do call for channel [Local/+41793237828@mobilisation-voip-000379f7;1]
	> Nov 19 10:04:56 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:56] VERBOSE[28253][C-00081321] pbx.c: Executing [1@mobilisation-call:2] Answer("Local/+41793237828@mobilisation-voip-000379f7;1", "") in new stack
	> Nov 19 10:04:56 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:56] VERBOSE[28253][C-00081321] pbx.c: Executing [1@mobilisation-call:3] Wait("Local/+41793237828@mobilisation-voip-000379f7;1", "1") in new stack
	> Nov 19 10:04:56 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:56] VERBOSE[28465][C-00081313] bridge_channel.c: Channel SIP/cablecom-trunk-0009d17f joined 'simple_bridge' basic-bridge <54cf5063-f897-4e2d-ac7e-e557edc787cd>
	> Nov 19 10:04:56 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:56] VERBOSE[28255][C-00081313] bridge_channel.c: Channel Local/+41793237828@mobilisation-voip-000379f7;2 joined 'simple_bridge' basic-bridge <54cf5063-f897-4e2d-ac7e-e557edc787cd>
	> Nov 19 10:04:57 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:57] VERBOSE[28253][C-00081321] pbx.c: Executing [1@mobilisation-call:4] SendDTMF("Local/+41793237828@mobilisation-voip-000379f7;1", "#") in new stack
	> Nov 19 10:04:58 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:58] VERBOSE[28253][C-00081321] pbx.c: Executing [1@mobilisation-call:5] GotoIf("Local/+41793237828@mobilisation-voip-000379f7;1", "1?production") in new stack
	> Nov 19 10:04:58 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:58] VERBOSE[28253][C-00081321] pbx.c: Executing [1@mobilisation-call:9] AGI("Local/+41793237828@mobilisation-voip-000379f7;1", "agi://10.118.121.70:4573/MobilisationAgiScript?mobilisation_id=cda90215-3bdd-454b-8610-7ac85345687f&valid_pin_code=504351&invalid_pin_code=504350&notification_id=d48d2ddb-9bbb-493c-9e30-22b2dda58eeb") in new stack
	> Nov 19 10:04:58 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:04:58] VERBOSE[28253][C-00081321] file.c: **<Local/+41793237828@mobilisation-voip-000379f7;1> Playing './mobilisation/cda90215-3bdd-454b-8610-7ac85345687f.ulaw' (language 'fr')**
	> Nov 19 10:05:02 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:05:02] VERBOSE[28253][C-00081321] res_agi.c: <Local/+41793237828@mobilisation-voip-000379f7;1> Playing './mobilisation-ack.slin' (escape_digits=) (sample_offset 0) (language 'fr')
	> Nov 19 10:05:04 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:05:04] VERBOSE[28253][C-00081321] res_agi.c: <Local/+41793237828@mobilisation-voip-000379f7;1>AGI Script agi://10.118.121.70:4573/MobilisationAgiScript?mobilisation_id=cda90215-3bdd-454b-8610-7ac85345687f&valid_pin_code=504351&invalid_pin_code=504350&notification_id=d48d2ddb-9bbb-493c-9e30-22b2dda58eeb completed, returning 0
	> Nov 19 10:05:04 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:05:04] VERBOSE[28253][C-00081321] pbx.c: **Executing [1@mobilisation-call:10] Hangup("Local/+41793237828@mobilisation-voip-000379f7;1", "") in new stack**
	> Nov 19 10:05:04 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:05:04] VERBOSE[28253][C-00081321] pbx.c: Spawn extension (mobilisation-call, 1, 10) exited non-zero on 'Local/+41793237828@mobilisation-voip-000379f7;1'
	> Nov 19 10:05:04 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:05:04] VERBOSE[28255][C-00081313] bridge_channel.c: Channel Local/+41793237828@mobilisation-voip-000379f7;2 left 'simple_bridge' basic-bridge <54cf5063-f897-4e2d-ac7e-e557edc787cd>
	> Nov 19 10:05:04 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:05:04] VERBOSE[28465][C-00081313] bridge_channel.c: Channel SIP/cablecom-trunk-0009d17f left 'simple_bridge' basic-bridge <54cf5063-f897-4e2d-ac7e-e557edc787cd>
	> Nov 19 10:05:04 dc1-ip-pbx-01.cta.local asterisk: [Nov 19 10:05:04] VERBOSE[28255][C-00081313] pbx.c: Spawn extension (mobilisation-voip, +41793237828, 6) exited non-zero on 'Local/+41793237828@mobilisation-voip-000379f7;2'

