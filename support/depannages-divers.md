---
title: Dépannages divers
description: Problèmes récurrents et procédures de rétablissement
published: true
date: 2021-08-06T07:23:01.243Z
tags: 
editor: markdown
dateCreated: 2021-08-05T13:58:34.331Z
---

# Support CTA
## Document de maintenance Start
[2020 03 04 Sae Ii Proc Document De Maintenance Rev 1 7](/uploads/systel/2020-03-04-sae-ii-proc-document-de-maintenance-rev-1-7.pdf "2020 03 04 Sae Ii Proc Document De Maintenance Rev 1 7")
## Support mur d'image
### Relancer le mur d'image
__Problème__
Le mur d'image ne s'affiche plus, la session est fermée (page de login Windows)

__Solution__
1) Se connecter sur srv-agcwall-01 (10.96.128.141) et rouvrir la session windows. Mot de passe dans keepass ProSDIS-infra -> domaine telem.local -> srv-agcwall -> <u>compte local</u>
2) Se connecter sur srv-agcwall-02 (10.96.128.142) et rouvrir la session windows. Mot de passe dans keepass ProSDIS-infra -> domaine telem.local -> srv-agcwall -> <u>compte local</u>
Le mot de passe pour se connecter via VNC figure dans keepass ProSDIS-infra -> domaine telem.local -> srv-agcwall -> <u>Compte VNC</u>

Se connecter sur AGCWEB, http://10.96.128.141/AGCWeb/Default.aspx (le lien se trouve également sur srv-affi, section *Seraphine - Mur d'image*, <u>AGC WALL</u>).
Mot de passe dans keepass ProSDIS-infra -> domaine telem.local -> srv-agcwall -> <u>AGCWeb</u>

Dans le menu en haut, onglet *Connexion*, vérifier que les deux murs sont connectés, presser *connexion* le cas échéant.
a) Revenir dans l'onglet *AGCWALL*, dans la combo box *connexion* sélectionner *Mur gauche*, choisir l'entrée *Macros* dans le sous-menu à gauche, choisir *Mur gauche* dans la liste déroulante puis choisir la macro appropriée (*jour gauche* ou *nuit gauche* en fonction du mode souhaité). Valider pour appliquer la macro.
b) Répéter l'opération a) pour le mur droit

Vérifier que les deux murs affichent bien des information différentes (l'apperçu est visible directement dans AGCWEB). Si tel n'est pas le cas, "jouer" avec les macros (inverser gauche et droite, où sélectionner deux fois gauche/doite pour les deux écrans, demander à l'opérateur de basculer de mode, ...).
En se connectant aux serveurs via VNC, il est normal que les deux écrans affichent uniquement un fond rose.

### Source fault
__Problème__
Message source fault clignotte sur l'écran
Message "no signal" sur l'écran (écran noir)
Image brouillée

__Solution__
Se connecter sur AGCWall http://10.96.128.141/AGCWeb/login/default.aspx, sélectionner dans le menu AGCWALL -> SOURCES MATERIELLES, clic sur MATERIELLES, clic sur la source défaillante (en rouge), clic sur le bouton réinitlaliser.

### Recenter un affichage sur le mur d'image
__Problème__
La carte météo (ou un autre affichage) n'est pas correctement centrée sur le mur d'image.

__Solution__
Se connecter sur AGCWall http://10.96.128.141/AGCWeb/login/default.aspx sélectionner le bon mur dans la drop box *connexion* en haut à gauche puis sélectionné la section affichée : un cadre bleu entour l'élément sélectionné.
Cliquer ensuite le bouton *FONCTIONS* puis cliquer le bouton + *(zoom / coupe)*. Jouer ensuite avec les paramètres x, y, l et h pour rencenter correctement l'affichage.

### Affichage Météo et inforoute n'est pas à jour
__Problème__
La météo ne s'affiche pas ou n'est pas à jour.

__Solution__
Se connectrer via VNC sur PC-WIN-METEO-01 - 10.96.128.31 (mot de passe dans keepass)  et relancer chrome (ou simplement redémarrer windows).

__Note:__ Des manipulations aditionelles sont requises pour établir le bon affichage - à faire par les opérateurs CTA.

### Affichage ALR (RCT) n'est pas à jour
__Problème__
L'affichage des ALR (RCT) n'est pas à jour.
Anciennement écrans intranet

__Solution__
Redémarrer le Rasperry PI.
- rasp-affialr-01 (10.96.128.32) Coté Pully
- rasp-affialr-02 (10.96.128.33) Coté Lausanne

__Attention__ Attendre au minimum 30 secondes pour l'affichages des états (les pastilles sont grisées après un reboot)

Se connecter à la machine : `ssh pi@10.96.128.3[2|3]` mot de passe situé dans le Keepass ProSDIS.
Redémarrer la machine en exécutant `sudo reboot`. 

### Nouveau raspberry n'affiche pas des pages en .telem.local
__Problème__
Un nouveau raspberry n'arrive pas à afficher des pages dont l'url est dans le domaine .telem.local

__Solution__
vi /etc/nsswitch.conf

modifier la ligne suivante:
#hosts:          files mdns4_minimal [NOTFOUND=return] dns
hosts:          files dns

### Affichage agenda véhicule (vhc) n'est pas à jour
__Problème__
L'affichage agenda véhicule (vhc) n'est pas à jour.

__Solution__
Redémarrer le Rasperry PI.
- rasp-agenda-vhc (10.118.122.216)

Se connecter à la machine : `ssh pi@10.118.122.216` mot de passe situé dans le Keepass ProSDIS.
Redémarrer la machine en exécutant `sudo reboot`. 

**NOTE:** Si l’affichage n’est pas juste, demander aux opérateurs de switcher le mode du mur d’image pour corrigé l’affichage

### Affichage agenda CTA n'est pas à jour
__Problème__
L'affichage agenda CTA n'est pas à jour.

__Solution__
Redémarrer le Rasperry PI.
- rasp-agenda-cta (10.96.128.35)

Se connecter à la machine : `ssh pi@10.96.128.35` mot de passe situé dans le Keepass ProSDIS.
Redémarrer la machine en exécutant `sudo reboot`. 

### Incohérence dans l'affichage des appels entrants ou des Postes Opérateur loggés
__Problème__
Les écrans indiquant les appels entrants n'affichent pas le bon nombre d'appels.
Les écrans n'affichent pas correctement les Postes Opérateurs loggés.

__Solution__
Redémarrer les deux Rasperry PI 
 - rasp-affiappel-01 (10.118.122.211)
 - rasp-affiappel-02 (10.118.122.212)

Les redémarrer l'un après l'autre en exécutant `sudo reboot`. Attendre que le premier PI aie redémarré avant de rebooter le second.
Pour se connecter à la machine : `ssh pi@10.118.122.21[1|2]`

Mot de passe présent dans keepassX sous _affichage CTA -> rasp-affiappel_.

### Ecran des statisques d'appels non fonctionnel (mur d'image)
__Problème__
Au milieu du mur d'image un petit écran affiche les statistiques des appel traité par le CTA.
Cet écran n'est plus fonctionnel

__Solution__
Redémarrer le rasp-totalappel-01

ssh pi@10.118.122.213 Même MdP que pour les rasp-affiappel

### Affichage agenda SSB (Jean Favre) en erreur
__Problème__
L'affichage de l'agenda SSB (Jean Favre) est en erreur.

__Solution__
Redémarrer la machine Windows
- Se connecter à la machine `10.96.128.34` puis la redémarrer (via le task manager bouton pas disponible)
- Si l'agenda véhicule se lance tout seul, fermer Internet Explorer (via le task manager -> kill process)
- Sur le bureau, double cliquer sur l'icône *Agenda (192.168.0.1) (S) - Racourci*, la fenêtre avec le titre *AGENDA - Affichage* s'affiche
- Déplacer la fenêtre de le coin inférieur droite de l'écran, se coordoner avec le CTA pour la placer correctement afin qu'elle soit affichée complètement sur le mur d'image
- Faire un basculement du mur pour vérifier le positionnement des fenêtres si sortie vidéo en veille 

<img src="/uploads/images/agenda-sbb.png" alt="Agenda Sbb" style="margin-left: 20px; width:70%;">

## Basculer PP1 de SLAVE vers MASTER
<a name="pp1_slave_master"></a>
__Problème__
Le PP1 est en mode SLAVE.

__Solution__
Se connecter en VNC vers le PP1 (10.144.1.10) - mot de passe situé dans le Keepass ProSDIS.
Sur l'**application DAG4000**, selectionner **'Statistic'** sur le menu à gauche et cliquer sur le button **'Switch to Master'**

<img src="/uploads/images/ppswitchtomaster.png" alt="Ppswitchtomaster" style="margin-left: 20px; width:70%;">

### Problème d'envoi de pager avec le laptop de secours ultime
__Problème__
L'envoi de pager via le laptop de secours ultime au CTA ne fonctionne pas (notamment lors des essais régulier du CTA)

__Procédure__
Vérifier que le PP1 est bien MASTER (voir [bascule PP1 slave -> master](#pp1_slave_master)) puisque le PC de secours ultime est connecté en direct sur le PP1.

## Procédure en mode de repli
__Problème__
Un restore des données de la Prod sur Repli se fait tous les jours à 15:30, ce qui risque de corrompre les données en utilisation si on est passé sur le mode de repli.

__Procédure__
- Le CTA appele la personne de piquet pour informer du passage en mode de repli
- Piquet STEL se connecte sur le serveur DSI-SRV-BD-03 (10.80.128.10) et ouvre le planificateur de tâches pour désactiver la tâche « Activation repli SPSL» qui aura pour effet de ne pas restaurer la base de données

Dans l’idéal, si on doit passer en repli, il faudra :
- Dans un premier temps lancer le job SQL de copie de base pour récupérer les données les plus à jour possibles à un instant T (à condition que la partie BDD en PROD soit toujours disponible, puisque si vous passez en repli c’est que quelque part la PROD à un problème);
- Lancer la tâche d’activation du script en manuel pour restaurer la base et la désactiver juste après.

### Schéma bascule primaire

<img src="/uploads/annotation-2019-08-15-102901.png" alt="Annotation 2019 08 15 102901" style="margin-left: 20px; width:70%;">

## Warnings sur Start concernant StartPhone, voyant TTS rouge
__Problème__
Suite à une bascule des StartPhone, plusieurs warnings sont affiché sur Start et le voyant TTS clignote en rouge

__Procédure__
- Se connecté sur les supervisors StartPhone (poluxeye). Liens sur la page srv-affi-01/telem.
- Contrôler que le *StartPhone 1* est en mode **STARTED** et le *StartPhone 2* est en mode **IDLE**

<img src="/uploads/images/startphone-1-started.png" alt="Startphone 1 Started" style="margin-left: 20px; width:50%;">

__StartPhone 1 STARTED__

<img src="/uploads/images/startphone-1-visualisationdispositifsexternes.png" alt="Startphone 1 Visualisationdispositifsexternes" style="margin-left: 20px; width:50%;">

__Selectionner Visualisation des dispositifs externes__

<img src="/uploads/images/startphone-1-dispositifsexternes.png" alt="Startphone 1 Dispositifsexternes" style="margin-left: 20px; width:50%;">

__Tous les dispositifs externes sont connectés sur StartPhone 1__

**NOTE:** Sur SartPhone 2 plusieurs dispositifs sont déconnectés

<img src="/uploads/images/startphone-2-dispositifsexternes.png" alt="Startphone 2 Dispositifsexternes" style="margin-left: 20px; width:50%;">

Redémarrer les deux Rasperry PI pour remettre le voyant TTS à vert
 - rasp-affiappel-01 (10.118.122.211)
 - rasp-affiappel-02 (10.118.122.212)

Les redémarrer l'un après l'autre en exécutant `sudo reboot`. Attendre que le premier PI aie redémarré avant de rebooter le second.
Pour se connecter à la machine : `ssh pi@10.118.122.21[1|2]`

Mot de passe présent dans keepassX sous _affichage CTA -> rasp-affiappel_.

## Redémarrage ESX
__Problème__
Alarme ESX sur Nagios

- CPU Usage for VMHost en critical
- Memory for VMHost en critical
- etc

> Le reboot de l'ESX ne doit être réalisée **que et uniquement que** si le serveur hôte (ESX) en question est planté ! Pour vérifier que le serveur est fonctionnel, ou pas, voici la marche à suivre : 

### Alarmes hyperviseur sur ESX
__Problème__
Suite à une mise à jour des ESX, il est possible qu'en de rares cas une fausse alarme soit remontée sur l'hyperviseur ex. `S: (Service check timed out after XX seconds) sur dc2-srv-esx-sup-02`.

__Procédure de vérification de l'état de l'ESX__
S'assurer qu'il s'agit bien d'une fausse alerte en se connectant sur vSphere, sélectionner l'ESX puis vérifier qu'aucune alarmes n'est présente dans l'onglet _Alarms_ en bas de l'écran.

Vous pouvez aussi aller vérifier l'état des VM d'un hôte 
	- Aller dans l'onglet "VM" et vérifier que des VM's sont en cours d'execution
	- Vérifier que la VM tourne correctement (par exemple en vérifiant son état sur Nagios / Cellnet, ou en vous connectant dessus)
	- Si les VM fonctionnent correctement et que les différents statuts sont cohérents, l'ESX n'est pas en panne.


> Si les différents checks ci-dessus n'ont pas donnés satisfactions, alors procéder au redémarrage du serveur :

__Procédure__
- Avant toute chose, se connecter sur vSphere (10.118.110.110), chercher l'host concerné et le mettre en mode maintenance sous "Action / Mode de Maintenance / Passer en mode de maintenance" (s'il n'est pas totalement freeze, il va déplacer les VMs et ne pas faire d'alarmes) 
- Trouver l'adresse IP de l'ILO de l'ESX en question sur IPAdmin
- Se connecter sur l'ILO avec un browser 
- Credentials dans le KeyPass (Compte Administrateur ILO)
- Aller dans Remote Console
- Cliquer sur Web Start
- Enregistrer et lancer le programme téléchargé
- En haut du programme, cliquer sur Power Switch -> Cold Reboot
- Regarder sur l'écran console que l'ESX redémarre bien (on voit la procédure de boot jusqu'au login)
- Enlever le mode de maintenance sous vSphere
- Contrôler que les VM reviennent correctement 
- Vérifier que les alarmes Nagios sont quittancées

## SAP BI Rapports ne s'ouvrent pas ("Illegal access to the viewer, please use a valid url")
__Problème__
Les rapports BI ne s'ouvrent pas, message *"Illegal access to the viewer, please use a valid url"*

__Procédure__
- Se connecter sur la VM srv-bo-01 (windows)
- Ouvrir l'application "Central Configuration Manager"
- Arrêter toutes les services
- Démarrer d'abord le service **Apache Tomcat** et après le service **Server Intelligence Agent**
- Le satut des services **Apache Tomcat** et **Server Intelligence Agent** doivent être *Exécution en cours*

<img src="/uploads/srv-bo-01-bi-services.png" alt="Srv Bo 01 Bi Services" style="margin-left: 20px; width:70%;">

__Site pour tester__
http://10.144.2.70:8080/BOE/BI

## Caméras du CTA ne marche plus // Mur d'image
__Problème__
Plusieurs caméras sur le mur d’image au CTA ne marchent plus

__Procédure__
Voici la vue du mur gauche dans son état normal

<img src="/uploads/agc-normal.png" alt="Agc Normal" style="margin-left: 20px; width:90%;">

Pendant le étapes suivantes, se connecter sur l'AGC Wall (http://10.96.128.141/AGCWeb/login/default.aspx), les credentials sont sur le Keepass ProSDIS, pour avoir la visualisation en temps réel des caméras au mur d’image.

- Redémarrer le serveur de streaming via SSH. 
- Se connecter sur la page **Streaming Server Admin Software** http://10.96.33.15:8080
- Nom d'utilisateur (*telematique*) et mot de passe situé dans le **Keepass Telematique** (Sites - Pully - Serveurs & switches - Streaming server)
- Sur la page d’administration exécuter les étapes suivantes :
1. Cliquer sur **Stream monitors**
1. Sélectionner un des **Media Client**
1. Cliquer sur **Edit**
1. Sur la fenêtre « Stream monitor édition », cliquer sur l’onglet **Monitors**
1. Cliquer sur la **caméra** que pose problème
1. Sur la fenêtre d’édition du monitor sélectionné, cliquer sur **Apply**, pour mettre à jour la configuration

Si la caméra n'est pas affiché sur le mur d'image, contrôler la présence du flux vidéo sur le vidéo recoder de pully (synology 10.96.33.10).
Essayer aussi de redémarrer la caméra directement en vous connectant sur son IP (visible depuis la liste du matériel sur le vidéo recorder).

Si l'espace disque est plein:

Commande utile (à exécuter en root) permettant d'afficher les plus gros dossier sur le filesystem
 root@srv-video-01:~# du -a / | sort -n -r | head -n 20

<img src="/uploads/images/camerasmediaclientapplyconfig.png" alt="Camerasmediaclientapplyconfig" style="margin-left: 20px; width:70%;">

## Alarme réseau transport – Mux 1 / 2
__Problème__
Par temps orageux, plusieurs alarmes MUX et FH peuvent apparaître  

<img src="/uploads/images/hyperviseur.png" alt="Hyperviseur" style="margin-left: 20px; width:70%;">

__Procédure__
Vous connecter sur cellnet radio - http://172.27.1.10:9080/Itanium-war/
Visualisations – Vue générale
Events – Events history list

Si il n'y a pas de croix rouge sur les sites c'est à dire que les sites sont tous connecté (comme sur cette capture d'écran)

Si un site est déconnecté (croix rouge sur le rond du site), appeler le Groupe Transmission.

<img src="/uploads/images/cellnetradio.png" alt="Cellnetradio" style="margin-left: 20px; width:70%;">

Plan de transmission des sites.

<img src="/uploads/images/plansitesradio.png" alt="Plansitesradio" style="margin-left: 20px; width:70%;">

## Appel bloqué sur la cartographie GeoWeb (poste opérateur)
__Problème__
Le pictogramme du téléphone reste affiché dans GeoWeb bien que l'appel soit terminé.

Attention, il ne faut pas avoir des appels en cours pendant l'opération !

__Procédure__
* Se connecter au PO TELEM avec la fonction ADMIN-SYSTEL
* Ouvrir GeoWeb [Menu Utilitaires -> Raccourcis -> GeoWeb]
* Dans GeoWeb, activer les appels. Il faut sélectionner le téléphone

<img src="/uploads/images/geoweb-op-appels.png" alt="Geoweb Op Appels" style="margin-left: 20px; width:30%;">

* Dans Start, Menu Utilitaires -> Désélectionner -> Appels en cours de saisie
* Retourner dans GeoWeb et rafraichir la page par un F5. L'appel ne doit plus s'afficher.

## Console trip : FVD1 ou F8 PTT anormalement long !
__Problème__
La console affiche le message : FVD1 ou F8 PTT anormalement long

__Procédure__
* Sélectionner l'onglet FVD1 / F8
* Sélectionner la ligne d'erreur "FVD1/F8 PTT anormalement long"
* Quittancer "par le vu" et appuyer sur le bouton physique du PTT (en bas à droit)

## Les appels ne remontent pas dans START
__Problème__
Les appels téléphonique ou radio ne remontent pas dans START. L'opérateur doit les prendre depuis la console trip.

__Procédure__
Il faut redémarrer la console et START afin que les 2 systèmes soient appairés correctement.

## Le compteur d'ALR est faux / incohérent sur le mur d'image

•	Contrôler les états de supervision des IP-RCTs
	o	Permets de valider en partie l’infrastructure de l’ECA et des services ALR fournis par SDN
	http://srv-affi-01.telem.local/rct/
	http://10.64.1.70:8080/ (mot de passe dans KeePass sous RCT Telematique)
	http://10.64.1.71:8080/
	http://10.118.128.40/ (mot de passe dans KeePass sous ALR Telematique)
•	Effectuer des transmissions d’alarme au moyen des transmetteurs présents dans la salle du CTA (DC-09 et TUSNet)
	o	Permet de tester le protocole cisnommé de bout à bout
•	Noter les raccordements (A0 de préférence pour faciliter l’analyse), l’alarme de supervision ou toutes autres informations pertinentes à une résolution de problème avant d’effectuer l’appel
	o	Ces références sont indispensables pour effectuer un diagnostic sur une alarme
	Mobiliser Systel / SwissDotNet afin d'aller plus loin dans le diabgnostique 

## La cartographie au SPSL ne fonctionne plus
__Problème__
La cartographie sur l'écran tactile du SPSL (en caserne) est figée ou ne s'affiche plus correctement.
	
__Procédure__
* 	Se connecter sur la machine via VNC `10.80.128.150`, username vide, mot de passe dans keepass ProSDIS -> SPSL -> affichage carto caserne.
* 	Redémarrer la machine.

## Problème avec sos directories - NotBd
__Problème__
Les ellipses ne s'affichent plus sur la carto lors des appels avec des numéros de mobile [079 078 077 076]
	
__Procédure__
* 	Faire un appel 118 avec un mobile
* 	Vérifier si l'url suivante retourne un xml avec les coordonnées GPS. Mettre le numéro de mobile dans l'url.
https://sos.directoriesdata.ch:8443/sdi-sos/NotDbIn?XMLRequest=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22%3F%3E%3Csvc_init+ver%3D%223.0.0%22%3E%3Chdr+ver%3D%223.0.0%22%3E%3Cclient%3E%3Cid%3EECAVD_IDA%3C%2Fid%3E%3Cpwd%3EBgSM7sJCQj%3C%2Fpwd%3E%3Crequestmode+type%3D%22PASSIVE%22%2F%3E%3C%2Fclient%3E%3C%2Fhdr%3E%3Ceme_lir+ver%3D%223.0.0%22%3E%3Cmsids%3E%3Cmsid+type%3D%22MSISDN%22%3E41795932169%3C%2Fmsid%3E%3C%2Fmsids%3E%3C%2Feme_lir%3E%3C%2Fsvc_init%3E

* Il faut se connecter sur un serveur stat : srv-start-01
* Aller dans c:\ et ouvrir notdb.txt en y mettant le bon numéro de portable
* Dans firefox, y coller l’url modifiée et vérifier que la réponse ressemble à ceci si elle est positive :

Attention directories garde en cache la demande donc il faut faire des tests avec plusieurs numéros !

<img src="/uploads/sos-directories-notdb.png" alt="sos.directories-notdb" style="margin-left: 20px; width:50%;">

## Problème personnel bloqué en intervention sur START
__Problème__
Une personne est bloqué dans une intervention sur START et ne peut pas être à nouveau mobilisé. Typiquement pour les chef d'intervention.

__Procédure__

* Se connecter sur le PO START TELEM en PROD (en remote si vous êtes à la maison) - 10.118.122.10 (en MSTSC - RDP)
* Se connecter à Start après avoir fait un "Copy Start"
* Utilisation de l'utilitaire de déselection des personnels au sein de l'application :
* Menu "Utilitaires" > "Déselectionner" > "Matériels/Personnels/Equipes retenus"

<img src="/uploads/annotation-2020-09-07-080911.png" alt="" style="margin-left: 20px; width:30%;">

## Alarme critical sur Audiocom connectivity (dsi-srv-mobilisation-01)
__Problème__
Le statut du module Audiocom connectivity reste CRITICAL après mobilisation au SPSL.

__Procédure__
Demander au CTA de effectuer une mobilisation de test au SPSL pour forcer la mise à jour du statut Audiocom connectivity.

### Plus de sonnerie d'alarmes au SPSL
__Problème__
Les alarmes ne sonnent plus au SPSL

__Procédure__
Redémarer le Cellnet SPSL
Contrôler l'état de connexion des automates Ixa (dans la device list)
Le SPSL doit contacter  "Ixa Systems" pour redémarrage de l'automate audiocom.

/!\ Attention si vous redémarrez aussi le serveur mobilisation (dsi-srv-mobilisation) si des interventions sont en cours, elles vont être rejouées dans la caserne 

Voici le lien contenant d'informations sur la mobilisation au SPSL
http://wiki.telem.local/en/spsl#serveur-intervention-spsl

## Problème de connexion depuis l'exterieur (PortailWeb, myStart+ et LEGO)
__Problème__
En cas de problème de connexion depuis l'exterieur (PortailWeb, myStart+ et LEGO), contrôler l’état des HA Proxy (credentials sur keepass ProSDIS-Infra)
    • http://10.64.6.181:9000/ (DMZ HA Proxy Web 01) 
    • http://10.64.6.182:9000/ (DMZ HA Proxy Web 02) 
		
Contrôler l'état des VerneMQ : 
   • LEGO : http://10.64.2.114:8888/status
   • MyStart : http://10.64.2.126:8888/status
	 
Contrôler l'état des StartMob (StartServer DMZ)
LEGO
   • http://10.64.2.133:8989/systel-rest/poluxeye/#/home
	 • http://10.64.2.134:8989/systel-rest/poluxeye/#/home
MyStart
   • http://10.64.2.135:8989/systel-rest/poluxeye/#/home
   • http://10.64.2.136:8989/systel-rest/poluxeye/#/home
	  
Contrôler l'état des cartes réseaux dans vSphere 
   • https://10.118.110.110/
		
__Procédure__
En cas de carte réseau déconnectée, reconnecter puis relancer la VM dans vSphere directement

<img src="/uploads/vsphere.png" alt="Vsphere" style="margin-left: 20px; width:50%;">

Après avec reconnecté puis rebooter la machine, vérifier l'état du service VerneMQ sur le broker (credential dans le KeePass ProSDIS-Infra) :

> sudo service vernemq status

Si le service est DOWN, essayer de relancer le service :

> sudo service vernemq start

Si le service ne redémarre pas, il faut vider les messages bloqueés dans la queue VerneMQ sur les deux serveurs :

> sudo rm -rf /var/lib/vernemq/msgstore/*

Puis, relancer le services sur les deux serveurs et contrôler le cluster VerneMQ sur la page de contrôle. Si le cluster est UP, contrôler que la connexion MQTT est bien réalisée sur les serveurs StartMob 

> sudo vmq-admin session show

Les serveurs "MobilityServer" 3 et 4 doivent être connectés

<img src="/uploads/startmob.png" alt="Startmob" style="margin-left: 20px; width:50%;">

Si les MobilityServer ne sont pas connectés, un reboot 1 à 1 des serveurs devraient les reconnecter.

Si tout échoue, appeler Systel
## Services externes
__problème__
Sur les services externes, plusieurs points sont à contrôler avant de mobiliser Systel 

__Procédure__
Check des DMZ-HA-PROXY-WEB :
* http://10.64.6.181:9000/
* http://10.64.6.182:9000/

Si tout est au vert, c’est que la partie HA de Systel fonctionne « normalement ». Il faut désormais vérifier les services / serveurs. 
**Si une partie est en rouge, appeler Systel**

Il faut vérifier l’applicatifs au niveau utilisateur
* MyStart
* LEGO
* Portail Web
* Affichage caserne (voir [ici](#controler-le-nombre-decrans-caserne-que-sont-connectes) comment controler l'affichage caserne)



Si 1 seul service est touché, c’est probablement un problème au niveau du service. L’idéal et de tester si ce service fonctionne en TEST / FORM. Suite de quoi => appeler Systel
Si 2 ou plus services sont touchés, le WAF est possiblement impacté, Afin de s’assurer que c’est le WAF ou la partie Systel, 
il est possible de contrôler que la partie TEST ou FORM fonctionne, pour se faire, vérifier la connectivité MyStart en TEST 
* Logout de votre compte MyStart
* Aller dans option, mdp => « admin + jj » (jj = jour)
* Entrer dans label : ECA_Test
* Selectionner le SDIS ECA_TEst au lieu de ECA
* Loguer vous avec votre NIP - MDP : 118
* Suite de quoi, si la connexion fonctionne => le WAF n’est pas impacté 
* Si la connexion ne fonctionne pas => se diriger vers le WAF

Srv-waf-01.118-vaud.ch (https://10.118.110.10)
Srv-waf-02.118-vaud.ch (https://10.118.110.11)
Login : keepass (srv-waf-01)

> Avant de reboot le serveur, merci d'effectuer les opérations suivantes : 
### Récupération des logs des WAF
Se rendre sous System / Maintenance / Debug
Puis cliquer sous "Download"
![Etape 1](/uploads/etape-1.png "Etape 1")

Suite de quoi, se rendre sous Log&Report / Log Access / Download
Selectionner chaque Log Type (Event Log, Attack Log, Traffic Log) et faire "Download"
![Etape 2](/uploads/etape-2.png "Etape 2")

Enfin, se rendre sous System / Network / Packet Capture
Lancer la capture #5 avec la petite flèche, attendre que la capture soit terminée, et downloader le fichier
![Etape 3](/uploads/etape-3.png "Etape 3")

Vérifier l’état des servies et du serveur au niveau des différents graphiques. Si les "Concurrent Connections" sont à 0, alors redémarrer le serveur. 

![Forti Shut](/uploads/uploads/forti-shut.png "Forti Shut")

Cliquer sur le bouton « Reboot » et attendre 2-3 minutes. 

> Ne pas oublier d'avertir le CTA et de mettre en "Schedule Downtime" l'hôte et les services sous Nagios afin que les remontées d'erreurs n'arrivent pas sur Cellnet. 
> Merci de transmettre les LOGS à Sébastien et / ou Dan afin qu'ils les envoient à Fortinet

En vue très schématique, voici comment se déplace le flux entre internet et les différents services.

![Schema](/uploads/uploads/schema.png "Schema")

## Redémarrage des serveurs GEO

![Sans Titre](/uploads/sans-titre.png "Sans Titre")

## Contrôler le nombre d’écrans caserne que sont déconnectés

Se connecter sur https://afficas-admin.118-vaud.ch en tant que administrator (credentials sur Keepass ProSDIS)
1.	Cliquer sur le menu « Recherche des écrans »
2.	Filtrer le résultat de recherche
a.	Emplacement = DPS
b.	Autorisé = true
c.	Connecté = false
3.	Contrôler que nous avons environs 17 écrans

<img src="/uploads/affichagecasernerechercheecrans.png" alt="Affichage Caserne Recherche Ecrans" style="margin-left: 20px; width:40%;">

Si le nombre d’écrans déconnectés est largement supérieur, le serveur doit être redémarré.

__Procédure__
Se connecter à la machine : `ssh administrator@10.64.2.10` mot de passe situé dans le Keepass ProSDIS.
Redémarrer la machine en exécutant `sudo reboot`.

## Reboot VerneMQ sur SRV-BROK
__Problème__
Le service VerneMQ est éteint ou crashé.

__Procédure__
1. Depuis vSphere, se connecter sur la machine srv-dmz-broker-0(1/2).
2. Vérifier l'état du service `sudo service vernemq status `
3. Démarrer le service si éteint  `sudo service vernemq start`
4. Re-vérifiier l'état du service tel qu'en (2.)
5. Si toujours pas redémarré ou en failed `sudo service vernemq stop` puis `sudo service vernemq start` jusqu'à ce qu'il accept.

## Check du MdGate Systel
__Problème__
La liste des SDIS est vide dans MyStart

__Procédure__
Check via l'URL https://mystartsec.systel-dc.com:9090/mqtt/sdis que des SDIS sont affichés, si la page est blanche, il y a un problème avec le MdGate de Systel => Les mobiliser. 

## Véhicules dupliqués sur le synoptique
__Problème__
Plusieurs mêmes véhicules apparaissent de façon dupliquée sur le synoptique.

__Procédure__
1. Vérifier quel est l'instant de Start active à l'aide des liens : 
		a. Start 1 : http://10.118.140.33:8989/systel-rest/poluxeye/#/home/
		b. Start 2 : http://10.118.140.34:8989/systel-rest/poluxeye/#/home/
2. Passer le server STARTED en IDLE à l'aide du bouton de la page web.
3. Vérifier que le server qui était précédement en IDLE passe bien automatiquement au statut STARTED
3. Se connecter à vSphere https://10.118.110.110/ et redémarrer le serveur qui est à l'état IDLE : actions -> power -> restart guest os
4. Attendre que le serveur redémarrer (une dizaine de minutes)
5. Se loguer sur le serveur (aucune action n'est requise).

## Redémarrer des containers Docker
__Problème__
Des applications avec Docker sont en erreur

__Procédure__
1. Se connecter par ssh sur le serveur (srv-affi-01 - srv-crss-web-01 - ....)
2. Pour voir tous les containers -> sudo docker ps -a
3. Regarder la colonne STATUS afin de connaitre son état UP ou autres
4. Optionnel : pour voir l'état en live des containers qui sont up -> sudo docker stats
5. Aller dans le répertoire docker ou docker_app
		a. ls -al
		b. cd docker ou cd docker_app
		c. Si plusieurs applications comme sur srv-affi-01 -> cd réperoire_de_mon_app
6. Important : il faut toujours être dans le répertoire de l'application qui doit être redémarrée !
7. Vérifier le nombre de fichier docker-compose.yml -> ls -al
8. Stopper le ou les containers 
		a. Si un seul fichier docker-compose : sudo docker-compose stop
		b. Si plusieurs fichiers docker-compose.yml comme pour agenda-cta : sudo docker-compose -f docker-compose.yml -f docker-compose-prod.yml stop
10. Démarrer  le ou les containers
		a. Si un seul fichier docker-compose : sudo docker-compose up -d ou voir s’il y a un fichier readme ou start-docker-cmd
		b. Si plusieurs fichiers docker-compose.yml comme pour agenda-cta : sudo docker-compose -f docker-compose.yml -f docker-compose-prod.yml up -d
11. Vérifier l'état (STATUS UP) -> sudo docker ps -a
12. Voir les logs en live du démarrage -> sudo docker-compose logs --tail=10 -f

Information supplémentaire voir <a href="http://wiki.telem.local/docker-container">la page Docker du wiki</a> :)

## PortailWeb Adobe Air - Practeo
__Informations__
Le portail web est désormais en PRODUCTION et FORMATION tournant dans un package Adobe Air.
Afin de pouvoir le faire tourner de façon « transparente » pour les utilisateurs, nous avons loué un serveur dédié dans le Datacenter de « Ganesh-Hosting » à Vevey (https://www.ganesh-hosting.ch/) via la société Practeo à Renens (https://www.practeo.ch/?r=0_0)

Nous avons un accès à ces serveurs (Ganesh), IP publique des serveurs dans KeePass, afin de pouvoir le reboot en cas de problème sur le portail web, le login et mot de passe se trouve dans le KeePass.
L’accès en RDP ne peut se faire que depuis le réseau ProSDIS interne avec notre IP publique. De ce fait, en cas de dépannage depuis la maison, vous devez vous connecter sur la machine de maintenance :
* 10.96.201.10
	* Login et password : AD Telem

__Clés Adobe Air__
**FORMATION** : eyJzZGlzIjoiRk9STUFUSU9OIiwiZGVybmllckVudiI6IkZPUk1BVElPTiIsInBvcnRhaWxXZWJVcmxzIjp7IkZPUk1BVElPTiI6Imh0dHBzOi8vZm9ybS1wb3J0YWlsLjExOC52YXVkLmNoIn19
**PRODUCTION** : eyJwb3J0YWlsV2ViVXJscyI6eyJQUk9EIjoiaHR0cHM6Ly9wb3J0YWlsLjExOC12YXVkLmNoIn0sInNkaXMiOiJFQ0EiLCJkZXJuaWVyRW52IjoiUFJPRCJ9

__Problème__
Le PortailWeb affiche plusieurs options à la place l'option par défaut "PROD".

__Procédure__
Ajouter la clé d'activation du PortailWeb Adobe Air

En cas de problème,
1. cliquer sur "Changer de SDIS"
![1](/uploads/1.png "1")

2. Copy paste la clef d'activation **eyJwb3J0YWlsV2ViVXJscyI6eyJQUk9EIjoiaHR0cHM6Ly9wb3J0YWlsLjExOC12YXVkLmNoIn0sInNkaXMiOiJFQ0EiLCJkZXJuaWVyRW52IjoiUFJPRCJ9**
3. Cliquer activer 
![1](/uploads/2.png "2")

4. Choisir l'environnement de PROD
![3](/uploads/3.png "3")

## StartAffi.rdp
__Informations__
Les postes opérateurs n'ayant pas accès à internet, nous avons mise en place une Remote App avec un Google Chrome afin de pouvoir passer par une machine rebond.

Pour ce faire, 2 serveurs sont utilisés : 
- srv-tse-manager-01 : 10.96.128.42
		- Ce serveur s'occupe de gérer les connexions
- srv-tse-host-01 : 10.96.200.11
		- Ce serveur héberge l'application Google Chrome 

Les opérateurs ont un raccourci sur leur bureau appelé "StartAffi.rdp" qui est un lien RDP vers la machine "SRV-TSE-MANAGER-01.TELEM.LOCAL" 

__Procédure__
En cas de problème avec la connexion au RDP (et non pas avec la non-affichage de la page), se connecter sur les 2 serveurs et les rebooter

## ECABOX - Syno-Storage - srv-storage-01
__Informations__
Le service "ECABOX" est basé sur une machine Linux Ubuntu avec point de montage iSCSI sur la paire de Synology "syno-sto" (Storage). En cas de problème avec la machine / les syno, et après un reboot, il faut faire attention à contrôler l'état du service NGINX
__Procédure__
> Check du service avec sudo service nginx status

Si Ko, alors relance du service après avoir fait un Stop
> sudo service nginx stop
> sudo service nginx start

<img src="/uploads/ngix.png" alt="Ngix" style="margin-left: 20px; width:60%;">

### Action à faire si la vm a les disques en readonly après un crash. Attention disques en LVM

Il faut booter la vm avec l’image ISO : [Datastore_iSCSI4] ISO/gparted-live-1.1.0-1-amd64.iso

Dans un terminal :

sudo lsblk [List block devices]

sudo lvscan [List all logical volumes in all volume groups]

sudo fsck /dev/… il faut mettre le nom donné par la cmd lvscan

## Récupération des logs d'une tablette LEGO
__Informations__
En cas de problème sur une tablette en particulier, il peut être intéressant de récupérer les logs avant d'ouvrir un ticket chez Systel. Pour ce faire, vous aurez besoin du n° de la tablette ou de son numéro de licence ou encore de son indicatif

__Procédure__
1. Se loguer sur un PO
2. Après avoir ouvert Start, dans la page de login du SSO cliquer sur "Paramétrage des outils mobiles"
3. En haut à droite, rechercher votre tablette
4. Cliquer sur la bonne tablette
5. Sur la droite, sélectionner "Récupérer les logs" dans le menu déroulant, puis "Envoyer"
6. Récupérer le .zip et envoyer le à Systel via Sysnet


## Problème de réécoute depuis START
__Informations__
La réécoute des appels depuis START n'est plus possible, une alarme Gestion RC est remontée dans START. 

__Procédure__
1. Se connecter en cta\Administrateur sur le srv-multi-01.cta.local (10.118.140.14). 
2. Si Gestion RC est ouvert, le fermer puis le réouvrir à l'aide du raccourcis situé sur le bureau. 
3. Se déloguer et reloguer (bouton en haut à droite sur l'interface graphique du GestionRC). 
4. Tester de recharger la liste des appels (dans les logs, on voit le nombre d'appels récupérés, cette valeur doit être différente de zéro). 

![Gestionrc](/uploads/gestionrc.jpg "Gestionrc")

## Alarme CTL Romanel
Vous trouverez ici la documentation pour pouvoir désactiver un critère de sécurité au CTL sur OnSphere. 

[Suppression Element Verrouillage](/uploads/suppression-element-verrouillage.pdf "Suppression Element Verrouillage")

Cela est certes quelque peu indigeste mais peut nous permettre de nous en sortir dans un cas de figure comme celui rencontré hier où la barrière IB7 déclenchait des alarmes effractions intempestives. 


## Serveur Ubuntu bloqué en lecture seule

**SNAPSHOT** de la VM sur vsphere
Se connecter en SSH sur le serveur.
Identifié le volume impacté en listant les volumes:
Command:

`$ sudo fdisk -l`

Exemple d'output:

> Device     Boot    Start      End  Sectors  Size Id Type
> /dev/sda1  *        2048 31457279 31455232   15G 83 Linux
> /dev/sda2       31459326 33552383  2093058 1022M  5 Extended
> /dev/sda5       31459328 33552383  2093056 1022M 82 Linux swap / Solaris


Identifier le volume Extended ou LVM, habituellement sda2, pas systématique. ATTENTION: NE JAMAIS LANCER LA COMMANDE SUIVANTE SUR LA PARTITION BOOT, autrement la machine ne démarrera plus.
Dans l'exemple précédent le volume est sda2

Commande suivante:
`sudo fsck -fy /dev/sda2`

Exemple d'output:

> fsck from util-linux 2.27.1
> e2fsck 1.42.13 (17-May-2015)
> DOROOT: clean, 289048/3932160 files, 11328157/15728640 blocks


## État des transmetteurs / critères ALR n'est pas cohérant entre la Gestion des ALR Start et SDN
Le critère ou le transmetteur a été supprimé de la Gestion des ALR SDN mais est toujours présent sur la Gestion des ALR Start.

| Valeur ID_ETAT_OP | STATUT Gestion ALR |
| ------ | --- |
|0| Actif|
|1| Inactif|
|2| Test|
|3| Supprimé|
|4| Inconnu|

Valider que la zone n'as pas le statut correct et exécuter la requête update pour le transmetteur et critère sur la DB Start

![Sql Server Start](/uploads/sql-server-start.png "Sql Server Start")
IP: 10.118.128.25
login: sa
password: Keepass (chercher "startdb")

```sql
DECLARE @NumDossier VARCHAR(6);
SET @NumDossier = 'A04635';
DECLARE @Critere VARCHAR(20);
SET @Critere = 14;

--METTRE A JOUR LE CRITERE AVEC L’ETAT OP À 3
--UPDATE [StartDB].[dbo].[ALR_ZONE] SET ID_ETAT_OPE = 3 WHERE NUMERO_DOSSIER = @NumDossier AND NUMERO_ZONE = @Critere;

--VOIR LA VALEUR ID_ETAT_OPE
SELECT TOP 1000 [NUMERO_ZONE]
      ,[NUMERO_DOSSIER]
      ,[CLASSE_RISQUE]
      ,[NUMERO_LIEU]
      ,[ID_ETAT_OPE]
	  ,ID_ANNUAIRE_OPE
  FROM [StartDB].[dbo].[ALR_ZONE] WHERE NUMERO_DOSSIER = @NumDossier AND NUMERO_ZONE = @Critere;
```

Autres requêtes outils
```sql
-- Lister les critéres qu'ont le technicien "SUPPRIMEE DANS GESTION ALR" et n'ont pas le statut supprimé
SELECT NUMERO_DOSSIER, NUMERO_ZONE, NOM_ER, ID_ETAT_OPE, ID_ANNUAIRE_OPE FROM [StartDB].[dbo].ALR_ZONE
WHERE ID_ANNUAIRE_OPE = '73049' AND ID_ETAT_OPE != 3 ORDER BY NUMERO_ZONE

-- Lister les critéres qu'ont le technicien "ATTENTE DE SUPPRESSION" et n'ont pas le statut supprimé
SELECT NUMERO_DOSSIER, NUMERO_ZONE, NOM_ER, ID_ETAT_OPE, ID_ANNUAIRE_OPE FROM [StartDB].[dbo].ALR_ZONE
WHERE ID_ANNUAIRE_OPE = '71950' AND ID_ETAT_OPE != 3 ORDER BY NUMERO_ZONE

-- Lister les techniciens pour la suppression 
SELECT TOP (1000) [ID_ANNUAIRE_OPE],[NOM_ANNUAIRE],[PRENOM_ANNUAIRE] FROM [StartDB].[dbo].[ANNUAIRE_OPE] WHERE [NOM_ANNUAIRE] like '%SUPPR%';
```
