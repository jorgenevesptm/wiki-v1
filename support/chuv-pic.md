<!-- TITLE: CHUV PIC -->
<!-- SUBTITLE: A quick summary of Chuv Pic -->

# Connexion au CHUV 
https://vpn.chuv.ch/partner
A priori il faut une machine windows
Pour le user/pwd voir le keepass

# Configuration de Putty
<img src="/uploads/chuv-pic/chuv-pic-putty.png" alt="Putty Config" style="margin-left: 20px; width:60%;">

# Configuration de SqlDeveloper
<img src="/uploads/chuv-pic/chuv-pic-sqldeveloper.png" alt="SqlDeveloper Config" style="margin-left: 20px; width:40%;">


Pour le pwd voir le keepass

# SQL pour les insertions / mises à jour-- Insert pic user -- default pwd = pic

-- CET
insert into tbl_users value( null, 'xxx@cet', '660b8d986fff7adea1ba00bca4522a74', 'Prénom Nom', 'PN', 0, 1, 1);

-- CAE
insert into tbl_users value( null, 'xxx@cae', '660b8d986fff7adea1ba00bca4522a74', 'Prénom Nom', 'PN', 0, 2, 1);

-- CTA
insert into tbl_users value( null, 'xxx@cta', '660b8d986fff7adea1ba00bca4522a74', 'Prénom Nom', 'PN', 0, 3, 1);

-- CASU
insert into tbl_users value( null, 'xxx@casu', '660b8d986fff7adea1ba00bca4522a74', 'Prénom Nom', 'PN', 0, 4, 1);


update tbl_users set password = '660b8d986fff7adea1ba00bca4522a74' where username = 'xxx@cta';

update tbl_users set password = '660b8d986fff7adea1ba00bca4522a74' where id = 45;

# SQL aide
 
select * from tbl_users where username in ('bs@cet', 'sgraber@casu', 'fbraillard@casu');

select * from tbl_users where displayname like '%Beau%';

delete from tbl_users where id = 191;



