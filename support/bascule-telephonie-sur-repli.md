<!-- TITLE: Bascule Telephonie Sur Repli -->
<!-- SUBTITLE: A quick summary of Bascule Telephonie Sur Repli -->

# Bascule de la téléphonie sur le repli
## But
Lorsqu'il n'est plus possible d'utiliser les consoles TRIP ou lorsque les opérateurs du CTA doivent quitter la salle d'engagement, il est nécessaire de faire une bascule en mode REPLI. 

## Opérations
Si la télématique doit procéder à la bascule des numéros sur le repli (118 mais également les numéros administratifs), cela peut être fait via le Business Center de Swisscom (https://extranet.swisscom.ch). 

Pour les opértaeurs, il existe deux moyens d'accéder au site Internet de Swisscom pour la bascule. 
* Le premier par Start-Affi directement depuis le PO Start (option qu'ils doivent privilégier dans la mesure du possible). La connexion internet est (en temps normal) celle d'UPC (selon principe BGP). 
* Le second par les PCs de secours. Ceux-ci sont disponibles si les stations SAE sont HS et utilise une connexion internet 4G de Swisscom via la carte SIM dans le laptop. Attention à bien déconnecter le portable de la docking station. 

Tout d'abord, se loguer via les informations contenues dans le Keepass télématique. 

![Bascule Login](/uploads/bascule-login.png "Bascule Login")

Sélectionner Enterprise SIP (dans le menu en haut à gauche).

![Bascule Esip Rdm](/uploads/bascule-esip-rdm.png "Bascule Esip Rdm")

Ensuite cliquer sur "Scénarios de déviation", également situé dans le menu en haut à gauche de la page nouvellement chargée. 

![Bascule Devi](/uploads/bascule-devi.png "Bascule Devi")

Choisir le scénario "118 - PROD - SPSL" puis cliquer sur "Activer". 

![Bascule Scenario](/uploads/bascule-scenario.png "Bascule Scenario")

La déviation est active lorsque la pastille verte apparaît. 

![Bascule Scenario Act](/uploads/bascule-scenario-act.png "Bascule Scenario Act")

Les appels arrivent désormais sur les 5 téléphones portables. 

Pour reprendre les appels au CTA sans la déviation, reprendre les poitns ci-dessus mais cliquer susr "Désactiver" à côté du scénario "118 - PROD - SPSL" actif. La déviation est insactive lorsque la pastille verte disparaît. 