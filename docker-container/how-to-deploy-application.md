<!-- TITLE: How To Deploy Application -->
<!-- SUBTITLE: Les quelques étapes à suivre pour déployer une application dans un environnement de container sous Docker -->

# How to deploy from a dev environment

Prerequisite : A server with docker, docker-compose, docker images and application containers running
Open a console in root project and copy everything into it.
## Run Maven command to build, test Java code and build, copy Angular code

-------------------------------------------------------------------- copy start --------------------------------------------------------------------------

```text
cd docker && \
docker-compose stop && \
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d && \
cd .. && \
mvn clean install -P PROD
```

-------------------------------------------------------------------- copy end --------------------------------------------------------------------------

Check url `https://dev.118-vaud.ch/agenda-cta/agenda`
Check url `https://dev.118-vaud.ch/agenda-cta/login`

**Change host srv-affi-qa-01 or srv-affi-01**

-------------------------------------------------------------------- copy start --------------------------------------------------------------------------

```text
export HOST_TO_DEPLOY=srv-affi-qa-01 && \
export DATE_TAR=$(date +"%Y%m%d-%H%M%S") && \
cp docker/payara/deployments/agenda-cta.war $DATE_TAR-agenda-cta.war && \
tar cfz $DATE_TAR-angular.tar.gz -C docker/apache agenda-cta && \
scp $DATE_TAR-agenda-cta.war $DATE_TAR-angular.tar.gz administrator@$HOST_TO_DEPLOY:~/war-deployment/
```

-------------------------------------------------------------------- copy end --------------------------------------------------------------------------

## Run script in war-deployment directory to deploy application in your server

WAR -> `./deployAgendaCTA.sh  YYYYMMDD-HHMMSS-agenda-cta.war`

ANGULAR -> `./deployAngularAgendaCTA.sh  YYYYMMDD-HHMMSS-angular.tar.gz`

Or quickly with ssh

```text
export HOST_TO_DEPLOY=srv-affi-qa-01

ssh -tq administrator@$HOST_TO_DEPLOY "sudo bash -c \
'cd war-deployment && ./deployAgendaCTA.sh $DATE_TAR-agenda-cta.war && ./deployAngularAgendaCTA.sh $DATE_TAR-angular.tar.gz'"
```

## Check if the application is running properly

Check url QA https://agenda-cta-qa.118-vaud.ch:4439/agenda-cta/agenda according to your open ports

Health check QA https://agenda-cta-qa.118-vaud.ch:4439/agenda-cta/rs/agenda/healthCheck

Check url PROD https://agenda-cta.118-vaud.ch:4439/agenda-cta/agenda according to your open ports

Health check PROD https://agenda-cta.118-vaud.ch:4439/agenda-cta/rs/agenda/healthCheck

## Connect by ssh to the server and check application logs

Go to `docker/agenda-cta/logs` and `tail -f app.log`

Check containers logs `sudo docker-compose logs -f --tail=20`

Workaround if logs are not refreshed -> restart war app container

Check which containers are running `sudo docker ps`
Restart specific container `sudo docker restart app-srv-agenda-cta`

