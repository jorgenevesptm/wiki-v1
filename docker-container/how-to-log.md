<!-- TITLE: How To Log Docker -->

### How to check logs

Run `sudo docker ps or sudo docker stats`
to see container name.

Run `sudo docker logs container_name 2>&1 | less`
to check log with less.

Replace container_name by the real name of your container.

