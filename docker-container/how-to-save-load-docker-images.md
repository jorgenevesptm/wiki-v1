<!-- TITLE: How To Save / Load Docker Images -->

### How to save docker images  from a dev environment

Build images and save its with docker save cmd. Adapt version with the last used. --19.04.10--
-------------------------------------------------------------------- copy start --------------------------------------------------------------------------

```text
export DOCKER_IMG_VERSION=19.04.10 && \
cd docker && \
docker-compose down && \
docker-compose -f docker-compose.yml build --force-rm --compress --no-cache && \
cd .. && \
export DATE_IMG=$(date +"%Y%m%d-%H%M%S") && \
echo "Save images ..." && \
docker save mariadb:10.3.10 payara/agenda-cta:$DOCKER_IMG_VERSION apache/agenda-cta:$DOCKER_IMG_VERSION > $DATE_IMG-img-agenda-cta.tar && \
echo "Clean images ..." && \
docker rmi $(docker images -f dangling=true -q)
```

-------------------------------------------------------------------- copy end --------------------------------------------------------------------------

### How to load docker images

Copy image archive into the server
`scp  $DATE_IMG-img-agenda-cta.tar administrator@srv-affi-qa-01:~/docker`

Load image
`ssh administrator@srv-affi-qa-01 "sudo -S docker load --input docker/$DATE_IMG-img-agenda-cta.tar" `

### How to setting up docker agenda-cta application

**Change host srv-affi-qa-01 or srv-affi-01**


```text
ssh administrator@srv-affi-qa-01 "mkdir -p docker/agenda-cta/apache && mkdir -p docker/agenda-cta/payara" && \
scp docker/*.yml docker/.env  administrator@srv-affi-qa-01:~/docker/agenda-cta/ && \
ssh administrator@srv-affi-qa-01 " \
sudo -S mkdir -p /opt/apache/agenda-cta/   && \
sudo -S mkdir -p /opt/payara/agenda-cta/logs/  && \
sudo -S chmod ugo+rw /opt/payara/agenda-cta/logs/. "
```


check ports used
`sudo netstat -lntu`

### Modify used ports in docker-compose.yml if necessary.

In your case, change MariaDB 3306:3306 -> 3396:3306,  Payara 4848:4848 -> 4849:4848, Apache 80:80 -> 8099:80  -  443:443 -> 4439:443

### Run your containers -> go to docker-compose files directory -> /docker/agenda-cta

QA env : `sudo docker-compose -f docker-compose.yml -f docker-compose-qa.yml up -d`

PROD env : `sudo docker-compose -f docker-compose.yml -f docker-compose-prod.yml up -d`

create database connection to mariaDB with ssh tunnel according to your mariaDB port and execute 00_create_tables_MariaDB.sql


 After read <a href="http://wiki.telem.local/docker-container/how-to-deploy-application">How to deploy docker application</a>

