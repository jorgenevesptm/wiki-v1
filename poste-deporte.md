<!-- TITLE: Poste Deporte -->
<!-- SUBTITLE: A quick summary of Poste Deporte -->

# Infos

Le concept du post déporté se compose de 4 éléments principaux

- Firewall - 10.180.128.1
- PO - 10.180.128.20
- TRIP - 10.180.128.11
- Media Client (Carto) - 10.180.148.30

La documentation se trouve sur le NAS ici => \\ha-syno-01.telem.local\Data\Prosdis\Documentation\Infrastructure\2018-10-12 - Concept PO déporté_V0.4.pdf

Le matériel se trouve dans INFO3.

![Img 20191017 135951](/uploads/img-20191017-135951.jpg "Img 20191017 135951")
![Img 20191017 140008](/uploads/img-20191017-140008.jpg "Img 20191017 140008")


# Configurations
Avant de partir, il faut penser à modifier deux choses au niveau des Fortigate 

1. Changer IP interface Fortigate 100E Deported => Network / Interface WAN1 => 10.176.231.254 
2. Changer IP VPN Distant Fortigate 500D Partenaire => VPN / IPSec Tunnels / Network / remote Gateway => 10.176.231.254

