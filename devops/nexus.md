<!-- TITLE: Nexus -->
<!-- SUBTITLE: A quick summary of Nexus -->

# Nexus Repositoy Manager
- Nexus est installé sur la même machine que GitLab : `10.96.128.150`
- Page d'administration Nexus : `http://10.96.128.150:8081/` login `admin` pwd `voir keepass`

## Ubuntu install
source  https://stackoverflow.com/questions/57028412/how-to-install-nexus-on-ubuntu-18-04

1. Install Java 8
`$ sudo apt-get update`
`$ sudo apt install openjdk-8-jre-headless -y`

2. Download Nexus
`$cd /home/administrator`
`$ sudo wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz`
`$ sudo tar -zxvf latest-unix.tar.gz`
`$ sudo mv nexus-3.19.1-01/ /opt/nexus`
`$ sudo mv sonatype-work/ /opt/`

3. As a good security practice, it is not advised to run nexus service as root. so create a new user called nexus and grant sudo access to manage nexus services.
`$ sudo adduser --disabled-password nexus`

4. Set no password for nexus user and enter below command to edit sudo file
`$sudo visudo`

5. Add the below line and Save: 
`nexus ALL=(ALL) NOPASSWD: ALL`

6. Change file and owner permission for nexus files
`$ sudo chown -R nexus:nexus /opt/nexus`
`$ sudo chown -R nexus:nexus /opt/sonatype-work`

7. Open /opt/nexus/bin/nexus.rc file, uncomment run_as_user parameter and set it as following.
`$ sudo vim /opt/nexus/bin/nexus.rc`
`run_as_user="nexus" (file shold have only this line)`

8. Add nexus as a service at boot time
`$ sudo ln -s /opt/nexus/bin/nexus /etc/init.d/nexus`

9. Log in as a nexus user and start service
`$ su - nexus`
`$ /etc/init.d/nexus start`

10. check the port is running or not using netstat command
`$ sudo netstat -plnt`

11. Make sure port 8081 is accessible from another computer.

12. Open a web browser and go to `http://server-address:8081`. Login as `admin`, password is stored on server in file `/opt/sonatype-work/nexus3/admin.password`
