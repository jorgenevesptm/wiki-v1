<!-- TITLE: Migrer repository remote vers un nouveau serveur -->

# Faire un push depuis repository local vers un nouveau repository remote
1 - Le nouveau serveur doit avoir git installé et un utilisateur git sans mot de passe doit exister.

2 - Ajouter la clé id_rsa.pub de la machine local (normalement dans ~/.ssh) aux clés autorisées de l'user git (dans /home/git/.ssh/authorized_keys).




3 - Création du nouveau repository remote:


```text
administrator@srv-scm-01:~$ cd /home/git
administrator@srv-scm-01:/home/git$ sudo mkdir fire-station-display.git
administrator@srv-scm-01:/home/git$ cd fire-station-display.git/
administrator@srv-scm-01:/home/git/fire-station-display.git$ sudo git init --bare
Initialized empty Git repository in /home/git/fire-station-display.git/
administrator@srv-scm-01:/home/git/fire-station-display.git$ cd ..
administrator@srv-scm-01:/home/git$ sudo chown git:git fire-station-display.git -R

```




4 - (conseillé) - En local, créer un repertoire sandbox et copier le projet à migrer dedans. Ex: 

```text
davide@laptop-drs:/media/data/development/git/sandbox$ cp ../fire-station-display -R .
```


5 - Copier le repository local en dehors le working copy:

```text
davide@laptop-drs:/media/data/development/git/sandbox$ cd fire-station-display
davide@laptop-drs:/media/data/development/git/sandbox/fire-station-display$ mv .git ../fire-station-display.git
```


6 - Transformer le repository local en "bare".
```text
davide@laptop-drs:/media/data/development/git/sandbox/fire-station-display$ cd ../fire-station-display.git
davide@laptop-drs:/media/data/development/git/sandbox/fire-station-display.git$ git config --bool core.bare true
```

7 - Faire le push en mode miroir:

```text
git push --mirror git@10.96.128.150:fire-station-display.git
```

8 - (optionnel) Faire pointer le projet vers le nouveau remote:

```text
davide@laptop-drs:/media/data/development/git/fire-station-display/.git$ sudo vi config
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
[remote "origin"]
        url = git@10.96.128.150:fire-station-display.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
        remote = origin
        merge = refs/heads/master
[branch "develop"]
        remote = origin
        merge = refs/heads/develop

```
On peut maintenant commiter / push les fichiers modifiés en local vers le nouveau repository!










