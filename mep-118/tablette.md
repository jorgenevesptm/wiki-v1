<!-- TITLE: Tablette -->
<!-- SUBTITLE: Guide déploiement MEP 118 sur tablette -->

# Instructions
## Accès à la page de déploiement:

1. Connexion sur le serveur Airwatch https://srv-airwatch-acc.telem.local/
2. Naviguez dans la barre de gauche "Device" -> "Provisionning" -> "Product list view"

Chaque ligne comprenant la dénomination du SDIS suivi de 118 concerne le déploiement de la MEP 118 dans SDIS approprié.
* par exemple pour Lausanne - Épalinges : **"LOSA - 118"**

Seules ces lignes sont à considérer, NE pas intervenir sur toutes lignes ne correspondant PAS aux critères mentionnés précédemment.

## Activer un déploiement

1. Repérer le SDIS sur lequel le déploiement est souhaité
2. Cliquer sur le Bouton rouge dans la première colonne de la ligne avec le titre "Active"
3. Message pop-up s'affiche demandant une confirmation, cliquer "OK"
4. La puce "Active" passe au vert

Ne pas désactiver puis activer un déploiement déjà lancé.
Cela reset les données du déploiement déjà effectué.
Pour les tablettes qui auraient déjà fait le déploiement, leurs statuts seront en échec, car elles avaient déjà les modifications déployées.