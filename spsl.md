<!-- TITLE: Spsl -->
<!-- SUBTITLE: © Ville de Lausanne - Service de protection et sauvetage Lausanne -->

# Emplacement des affichages caserne au SPSL
<table style="width:90%">
  <tr>
    <th>Écran</th>
    <th>Emplacement</th>
  </tr>
  <tr>
		<td>Lausanne 01</td>
    <td>Couloir cafeteria côté nord</td> 
  </tr>
	<tr>
		<td>Lausanne 02</td>
    <td>Garage WC</td> 
  </tr>
	<tr>
		<td>Lausanne 03</td>
    <td>Garage</td> 
  </tr>
	<tr>
		<td>Lausanne 04</td>
    <td>Perche de feu (Pole Dance)</td> 
  </tr>
	<tr>
		<td>Lausanne 05</td>
    <td>Salle chef section</td> 
  </tr>
	<tr>
		<td>Lausanne 06</td>
    <td>Couloir cafeteria côté sud</td> 
  </tr>
	<tr>
		<td>Lausanne 07</td>
    <td>Vestiaire gym</td> 
  </tr>
	<tr>
		<td>Lausanne 08</td>
    <td>Atelier</td> 
  </tr>
	<tr>
		<td>Lausanne 09</td>
    <td>Bureau du commandant</td> 
  </tr>
	<tr>
		<td>Lausanne 10</td>
    <td>Couloir salle chef section</td> 
  </tr>
</table>

# Script pour activer/désactiver la mise à jour des données au SPSL
## Documentation
* Schtasks.exe - https://docs.microsoft.com/en-us/windows/win32/taskschd/schtasks
* Simple Obfuscation (cacher le password) - https://mikefrobbins.com/2017/06/15/simple-obfuscation-with-powershell-using-base64-encoding/
	* Pour generer le obfuscation lancer la ligne suivant avec PowerShell
> 	`[Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes("'PASSWORD_ADMIN_SYSTEL'"))`
## EnableRemoteScheduledTask.bat

```batchfile
@echo off
:: "Possible options /DISABLE or /ENABLE"
powershell -encodedCommand JwBFAEMAQQBAAFMAeQBzAHQAZQBsACEAQQBkAFcAaQBuADIAMAAxADYAJwA= -noprofile >tmp.txt
set /p password=<tmp.txt
del tmp.txt
schtasks /Change /S 10.80.128.10 /U administrateur@cta /P %password% /TN "\ActivationSPSL\Activation Auto" /ENABLE
schtasks /Query /S 10.80.128.10 /U administrateur@cta /P %password% /FO TABLE /TN "\ActivationSPSL\Activation Auto"
```


## DisableRemoteScheduledTask.bat
```batchfile
@echo off
:: "Possible options /DISABLE or /ENABLE"
powershell -encodedCommand JwBFAEMAQQBAAFMAeQBzAHQAZQBsACEAQQBkAFcAaQBuADIAMAAxADYAJwA= -noprofile >tmp.txt
set /p password=<tmp.txt
del tmp.txt
schtasks /Change /S 10.80.128.10 /U administrateur@cta /P %password% /TN "\ActivationSPSL\Activation Auto" /DISABLE
schtasks /Query /S 10.80.128.10 /U administrateur@cta /P %password% /FO TABLE /TN "\ActivationSPSL\Activation Auto"
```

## Ecrans portail web
Script de démarrage automatique du portail web dans `~/.config/autostart`

## Analyse des logs de mobilisation au SPSL

Se connecter sur le serveur de mobilisation SPSL : http://dsi-srv-mobilisation-01.telem.local:8080 (utilisateur et mdp sur le keepass ProSDIS-Infra)



1. Sélectionner l’onglet « Application Server » (en bas à gauche)
2. Sélectionner le menu « History » (à droite)
3. Ouvrir la combo box « Export »
4. Cliquer pour ouvrir le fichier de log (choisir votre  éditeur de texte préféré)

![Srv Mob Spsl](/uploads/srv-mob-spsl.png "Srv Mob Spsl")

Chercher le texte « Starting intervention »
Dans l’exemple le système voyait l’intervention 409 mais le fichier WAV n’était pas encore présent, une fois le fichier présent le serveur commence la mobilisation au SPSL.
Si après 30s le fichier WAV n'est pas présent sur la GED, la sonnerie par défaut va être utilisée.

Example de log (l’heure affichée en UTC):

> **2020-01-27 08:25:57,498 INFO ch.swissdotnet.spsl.intervention.start.StartInterventionTransformer [START intervention polling scheduler-1] Intervention [00000409] provided with sound URL [null]**.
> 2020-01-27 08:25:57,499 ERROR ch.swissdotnet.spsl.intervention.start.StartInterventionTransformer [START intervention polling scheduler-1] Intervention[00000409] provided with invalid sound URL [null].
> 2020-01-27 08:26:02,193 DEBUG ch.swissdotnet.spsl.intervention.inventory.api.CellNetApi [Quuppa API session scheduler-1] CellNet API, performing PUT request on: [/api/plugin/v1/init].
> 2020-01-27 08:26:02,204 INFO ch.swissdotnet.spsl.intervention.start.StartInterventionGetter [START intervention polling scheduler-1] Requesting interventions from START on [http://10.64.2.180:8989].
> 2020-01-27 08:26:02,457 INFO ch.swissdotnet.spsl.intervention.start.StartInterventionTransformer [START intervention polling scheduler-1] Intervention [00000409] provided with sound URL [null].
> 2020-01-27 08:26:02,457 ERROR ch.swissdotnet.spsl.intervention.start.StartInterventionTransformer [START intervention polling scheduler-1] Intervention[00000409] provided with invalid sound URL [null].
> 2020-01-27 08:26:04,695 DEBUG org.quartz.core.QuartzSchedulerThread [DefaultQuartzScheduler_QuartzSchedulerThread] batch acquisition of 0 triggers
> 2020-01-27 08:26:07,204 INFO ch.swissdotnet.spsl.intervention.start.StartInterventionGetter [START intervention polling scheduler-1] Requesting interventions from START on [http://10.64.2.180:8989].
> 2020-01-27 08:26:07,442 INFO ch.swissdotnet.spsl.intervention.start.StartInterventionTransformer [START intervention polling scheduler-1] Intervention [00000409] provided with sound URL [null].
> 2020-01-27 08:26:07,442 ERROR ch.swissdotnet.spsl.intervention.start.StartInterventionTransformer [START intervention polling scheduler-1] Intervention[00000409] provided with invalid sound URL [null].
> 2020-01-27 08:26:12,195 DEBUG ch.swissdotnet.spsl.intervention.inventory.api.CellNetApi [Quuppa API session scheduler-1] CellNet API, performing PUT request on: [/api/plugin/v1/init].
> 2020-01-27 08:26:12,204 INFO ch.swissdotnet.spsl.intervention.start.StartInterventionGetter [START intervention polling scheduler-1] Requesting interventions from START on [http://10.64.2.180:8989].
> 2020-01-27 08:26:12,422 INFO ch.swissdotnet.spsl.intervention.start.StartInterventionTransformer [START intervention polling scheduler-1] Intervention [00000409] provided with sound URL [null].
> 2020-01-27 08:26:12,438 ERROR ch.swissdotnet.spsl.intervention.start.StartInterventionTransformer [START intervention polling scheduler-1] Intervention[00000409] provided with invalid sound URL [null].
> 2020-01-27 08:26:17,204 INFO ch.swissdotnet.spsl.intervention.start.StartInterventionGetter [START intervention polling scheduler-1] Requesting interventions from START on [http://10.64.2.180:8989].
> **2020-01-27 08:26:17,467 INFO ch.swissdotnet.spsl.intervention.start.StartInterventionTransformer [START intervention polling scheduler-1] Intervention [00000409] provided with sound URL [https://ged.118-vaud.ch:443/alfresco/service/cmis/s/workspace:SpacesStore/i/4850c735-bc6f-44f5-8e03-56f925a0118b/content.wav].**
> **2020-01-27 08:26:17,475 INFO ch.swissdotnet.spsl.intervention.session.InterventionSession [START intervention polling scheduler-1] Starting intervention [00000409]**
> 
>  with vehicles
>   [Feumotech AG AS 1000 (R_SEMOIR VD 2364) (id : 9517)] at place []
> 
>  and persons
>   [Sap Nicolas Mayer (2037) (id : 2037)] in room [57]
>   [App Eric Chatelan (737) (id : 737)] in room [57]
> 
> 2020-01-27 08:26:17,476 DEBUG ch.swissdotnet.spsl.intervention.inventory.api.CellNetApi [START intervention polling scheduler-1] CellNet API, performing PUT request on: [/api/plugin/v1/states].
> 2020-01-27 08:26:17,493 DEBUG ch.swissdotnet.spsl.intervention.inventory.api.CellNetApi [START intervention polling scheduler-1] CellNet API, performing PUT request on: [/api/plugin/v1/states].
> 2020-01-27 08:26:17,518 INFO ch.swissdotnet.spsl.intervention.session.InterventionSession [START intervention polling scheduler-1] Sound [https://ged.118-vaud.ch:443/alfresco/service/cmis/s/workspace:SpacesStore/i/4850c735-bc6f-44f5-8e03-56f925a0118b/content.wav] transmission started for intervention [00000409].
> **2020-01-27 08:26:17,609 INFO ch.swissdotnet.spsl.intervention.session.InterventionSession [Resource session scheduler-1] Sound transmission succeeded.**
