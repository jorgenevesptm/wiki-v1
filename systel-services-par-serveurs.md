<!-- TITLE: Systel Services Par Serveurs -->
<!-- SUBTITLE: Information par Systel der services par serveurs -->

# INTERNE - **VLAN ALERTE**
|Nom processus | Fonctionnalités | Serveur | Détetction erreur | Relance & contrôle |
| ----------- | --------------- | ------- | ----------------- | ------------------ |
|STARTSERVER (Polux) | •	Calcul proposition de départ <br>•	Calcul synoptique Web <br>•	Calcul pour GeoWeb |**Start 1**<br>10.118.140.33<br>**Start 2**<br>10.118.140.34 <br>Actif / Passif|Icône module N <br> tiers sur Start NT| Appli sur serveur <br> http://ip:8989/systel-rest/poluxeye |
|ACTIVEMQ	|• Synchronisation des postes<br>•	Rafraîchissement des postes<br>•	Tous les applicatifs|	**Multi 1**<br>10.118.140.14<br>**Multi 2**<br>10.118.140.24|Actif / Actif<br>Utilisateurs START/WEB|	ActiveMQ1 puis ActiveMQ2<br>Service Windows|
|DLGINTRAC|	•	Calcul pour le portail web<br>•	Calculs compétences personnel|**Multi 1**<br>10.118.140.14<br>**Multi 2**<br>10.118.140.24<br>Actif / En attente (60 sec)|Icône jaune sur Start NT|Appli sur serveur|
|DlgGVR	|•	Gestion des radios matériels et spécialistes|	**Multi 1**<br>10.118.140.14|	|	Appli sur serveur|
|GeoFlotteSvr|	•	Gestion du passage des statuts et de la géolocalisation des engins et des spécialistes|	**Multi 1**<br>10.118.140.14<br>**Multi 2**<br>10.118.140.24<br>Actif / En attente (60 sec)||Appli sur serveur|
|PrnServer|•	Impression ticket vers Imprimante dans caserne |	**Multi 1**<br>10.118.140.14<br>**Multi 2**<br>10.118.140.24<br>Actif / En attente (60 sec)||Appli sur serveur|
|STARTPHONE SERVER (syscallmgr)|•	Téléphonie Asterisk<br>•	Radio Swissdotnet<br>•	Création messages vocaux<br>•	Réception messages SMTP<br>•	Déclenchement SMS/Synthèse vocale Mail/Fax<br>•	Intégration des ALR|**Startphone 1**<br>10.118.118.31<br>**Startphone 2**<br>10.118.118.32<br>Actif / Passif<br>Nuance<br>|Icône téléphone bleu sur START NT Asterisk controller|http://ip:8989/systel-rest/poluxeye/<br>Appli sur serveur service startphone status |
|DWH|•	Statistiques|intelli|Les stats ne sont plus à jour|Raccourcis (tâches planifiées)|
|Service Intelligence Agent|•	Business Objects Intelligence Web Service|BO-01|Impossible d’accéder à Business Objects||
|StartWeb / GeoWeb|•	Alarmes auto (interface web appelée depuis START)<br>•	Geoweb (cartographie START)<br>•	Synoptique web<br>•	Seraphine|**Geoweb-01**<br>10.118.140.12<br>**Geoweb-02**<br>10.118.140.22<br>Actif / Actif|Appli figée / Erreur|Round robin DSN <br>Équilibrage de charge<br>Service nGinx|
|ETL RH/ADMIN||Inter 02<br>10.144.2.31|Planning pas à jour<br>Plus de remontée de rapport|Raccourcis (tâches planifiées)|
|Aide à la décision START|•	Proposition événements prise d'appel||Icône vert / Rouge sur Start NT|Menu Utilitaire dans StartNT|
# EXTERNE - DMZ-EXTERNE
|Nom processus | Fonctionnalités | Serveur | Détetction erreur | Relance & contrôle |
| ----------- | --------------- | ------- | ----------------- | ------------------ |
|STARTSERVEUR<br>(Pollux / Module N Tiers)|•	Plugin LEGO Mobilité (dialogue avec LEGO)<br>•	API ECA	|**dmz-start-01**<br>10.64.2.133<br>**dmz-start-02**<br>10.64.2.134<br>Actif / Passif|Plus de connexion sur les LEGO|Appli serveur|
|START WEB<br>(GeoWeb)|•	Affichage caserne<br> Lausanne (carto)|**dmz-geoweb-01**<br>10.64.2.112<br>**dmz-geoweb-02**<br>10.64.2.122||Service nGinx|
|VerneMQ|•	VerneMQ+ MQTT<br>(Rafraîchissement LEGO)|**dmz-broker-01**<br>10.64.2.114<br>**dmz-broker-02**<br>10.64.2.124||sudo service vernemq restart|
|PORTAIL WEB<br>(WebCIS)|•	Portail Web + iCome<br>(depuis mobile, lien web)<br>•	Cartographie portail web GCIS<br>(pour les centres)|**web-01**<br>10.64.2.31<br>**web-02**<br>10.64.2.32|Appli figée<br>Erreur http 404|Appli Glassfish<br>Appli GCIS (watchdog)|
|STARTSERVEUR<br>
(Pollux / Module N Tiers)|•	Plugin Mobilité (dialogue avec mySTART+)|**dmz-start-03**<br>10.64.2.135<br>Actif|StartServer dédié mySTART+|Appli serveur|
|MyStart|•	Vernemq MQTT broker|**dmz-broker-03**<br>10.64.2.126|Error conn. broker|sudo service vernemq restart|

# DMZ-PARTENAIRE
|Nom processus | Fonctionnalités | Serveur | Détetction erreur | Relance & contrôle |
| ----------- | --------------- | ------- | ----------------- | ------------------ |
|GeoServer|•	Cartographie<br>•	LEGO<br>•	START<br>|**geoserver-1-01**<br>10.144.2.120||```sudo systemctl status jetty ps aux  grep jetty 10.144.2.120:8080/geoserver```|
|GeoServer SIG-TELEM|•	Cartographie<br>•	PostGIS<br>|**srv-sig-telem-01**<br>10.144.2.80||```sudo docker stats```<br>```ps aux | grep java```<br>```ps aux | grep```<br>```srv-sig-telem-01:8080/geoserver```<br>|

|**Attention processus de démarrage long > 2 minutes**<br>|
|---------------------------------------------------------|
|```su -			[pour passer en root ne pas faire sudo ]```<br>```service jetty stop	[ne pas faire un restart]```<br>```ps aux | grep jetty	[pour vérifier que jetty est bien arrêté.```<br>```			une seule ligne doit être affichée : root ... 0:00	grep jetty]```<br>```service jetty start```<br>```ps aux | grep jetty	[2 lignes doivent être affichées]```<br>```Attendre ... ~2 minutes```<br>```Ctrl url http://10.144.2.120:8080/geoserver```<br>|

|**Attention VM avec Docker**|
| ---------------------- |
| sudo reboot<br>Ctrl url http://srv-sig-telem-01:8080/geoserver|